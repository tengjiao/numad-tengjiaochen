package edu.neu.madcourse.tengjiaochen;

import edu.neu.madcourse.tengjiaochen.communicate.Communicate_CommunicateActivity;
import edu.neu.madcourse.tengjiaochen.communicate.Communicate_CommunicationInitialActivity;
import edu.neu.madcourse.tengjiaochen.communicate.bean.Communicate_ServiceState;
import edu.neu.madcourse.tengjiaochen.communicate.service.Communicate_SnoopNewMessageService;
import edu.neu.madcourse.tengjiaochen.communicate.service.Communicate_WatchDogService;
import edu.neu.madcourse.tengjiaochen.dictionary.Dictionary_SplashActivity;
import edu.neu.madcourse.tengjiaochen.finalproject.FinalProject_DescriptionActivity;
import edu.neu.madcourse.tengjiaochen.finalproject.FinalProject_TutorialActivity;
import edu.neu.madcourse.tengjiaochen.newgame.NewGame_MainActivity;
import edu.neu.madcourse.tengjiaochen.newgame.NewGame_SplashActivity;
import edu.neu.madcourse.tengjiaochen.sudokuv4.Sudoku;
import edu.neu.madcourse.tengjiaochen.trickestpart.TrickestPart_TutorialActivity;
import edu.neu.madcourse.tengjiaochen.twoplayer.TwoPlayer_CommunicationInitialActivity;
import edu.neu.madcourse.tengjiaochen.twoplayer.TwoPlayer_SplashActivity;
import android.R.layout;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Layout;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		/*
		 * this code can change the title without
		 * changing the launcher.
		 */
		setTitle("Tengjiao Chen");
		
		Button aboutButton = (Button) findViewById(R.id.AboutButton);
		Button generateErrorButton = (Button) findViewById(R.id.GenerateErrorButton);
		Button sudoku = (Button) findViewById(R.id.SudokuButton);
		Button dictionaryButton = (Button) findViewById(R.id.DictionaryButton);
		Button newGameButton = (Button) findViewById(R.id.NewGameButton);
		Button communicationButton = (Button) findViewById(R.id.CommunicationButton);
		Button twoPlayerButton = (Button) findViewById(R.id.TwoPlayerButton);
		Button trickestPartButton = (Button) findViewById(R.id.TrickestPartButton);
		Button finalProjectButton = (Button) findViewById(R.id.FinalProjectButton);
		Button quitErrorButton = (Button) findViewById(R.id.QuitButton);

		aboutButton.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(MainActivity.this,AboutActivity.class));
			}
		});

		generateErrorButton.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				throw new RuntimeException();
			}
		});
		
		sudoku.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(MainActivity.this,Sudoku.class));
			}
		});

		dictionaryButton.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(MainActivity.this,Dictionary_SplashActivity.class));
			}
		});

		newGameButton.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(MainActivity.this,NewGame_SplashActivity.class));
			}
		});
		
		communicationButton.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(MainActivity.this,Communicate_CommunicationInitialActivity.class));
			}
		});
		
		twoPlayerButton.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(MainActivity.this,TwoPlayer_SplashActivity.class));
			}
		});
		
		trickestPartButton.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(MainActivity.this,TrickestPart_TutorialActivity.class));
			}
		});

		finalProjectButton.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(MainActivity.this,FinalProject_DescriptionActivity.class));
			}
		});
		
		quitErrorButton.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		
		// if snoopNewMessageSerive is alive
		// I have to kill it
		boolean isSnoopNewMessageServiceAlive = Communicate_ServiceState.getServiceState().getIsSnoopNewMessageServiceAlive();
		if(isSnoopNewMessageServiceAlive){
			// set the tag the disallows this service restart
			Communicate_ServiceState.getServiceState().setSnoopNewMessageServiceAllowedRestart(false);
			// stop the SnoopNewMessageService
			Intent intent = new Intent(MainActivity.this, Communicate_SnoopNewMessageService.class);
			stopService(intent);
			
		}

		// if snoopNewMessageSerive is alive
		// I have to kill it
		boolean isWatchDogServiceAlive = Communicate_ServiceState.getServiceState().getIsWatchDogServiceAlive();
		if(isWatchDogServiceAlive){
			// set the tag the disallows this service restart
			Communicate_ServiceState.getServiceState().setWatchDogServiceAllowedRestart(false);
			// stop the WatchDogService
			Intent intent = new Intent(MainActivity.this, Communicate_WatchDogService.class);
			stopService(intent);

		}
		
	}

}

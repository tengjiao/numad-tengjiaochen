package edu.neu.madcourse.tengjiaochen.communicate;

import edu.neu.madcourse.tengjiaochen.MainActivity;
import edu.neu.madcourse.tengjiaochen.R;
import edu.neu.madcourse.tengjiaochen.communicate.bean.Communicate_MessageContainer;
import edu.neu.madcourse.tengjiaochen.communicate.bean.Communicate_ServiceState;
import edu.neu.madcourse.tengjiaochen.communicate.service.Communicate_SnoopNewMessageService;
import edu.neu.madcourse.tengjiaochen.communicate.service.Communicate_WatchDogService;
import edu.neu.madcourse.tengjiaochen.communicate.util.Communicate_NetUtils;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class Communicate_CommunicateActivity extends Activity {
	
	public final static int RECEIVED_NEW_MESSAGE = 0;
	public final static int SEND_NEW_MESSAGE = 1;
	public final static int INTERNET_FAILS = 2;
	public final static int INTERNET_OK = 3;
	
	private EditText editTextMessageSend;
	private Button buttonSend;
	private Button buttonClear;
	private boolean isSnoopNewMessageServiceRunning = false;
	
	private static TextView textViewReceived;

	public static String getTextReceived(){
		if(textViewReceived != null){
			return textViewReceived.getText()+"";
		} else {
			return "";
		}
	}
	
	public static Handler handler;
	private Intent snoopNewMessage;
	private TextView textViewHintMessage;
	private MyReceiverNetFail receiverFail;
	private MyReceiverNetOK receiverOK;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.communicate_activity_communicate);
		
		// register two receivers
		receiverFail = new MyReceiverNetFail();   
        IntentFilter filterFail = new IntentFilter("edu.neu.mascourse.tengjiaochen.COMMUNICATION_FAILS");   
        registerReceiver(receiverFail, filterFail);
		
        receiverOK = new MyReceiverNetOK();   
        IntentFilter filterOK = new IntentFilter("edu.neu.mascourse.tengjiaochen.COMMUNICATION_OK");   
        registerReceiver(receiverOK, filterOK);

        editTextMessageSend = (EditText) findViewById(R.id.communicate_editText_MessageSend);
		buttonSend = (Button)findViewById(R.id.communicate_buttonSend);
		buttonClear = (Button)findViewById(R.id.communicate_buttonClear);
		textViewReceived = (TextView)findViewById(R.id.communicate_textViewReceived);
		textViewHintMessage = (TextView)findViewById(R.id.communicate_textViewHintMessage);
		
		snoopNewMessage = new Intent(Communicate_CommunicateActivity.this, Communicate_SnoopNewMessageService.class);
		startService( snoopNewMessage );
		Communicate_SnoopNewMessageService.setIsAllowedRunning(true);
		isSnoopNewMessageServiceRunning = true;
		
		Log.i("Communicate_CommunicateActivity","snoopNewMessage intent code executed");
		
		buttonSend.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Editable text = editTextMessageSend.getText();
				String imei = Communicate_MessageContainer.getMessageContainer().getIMEI();
				Communicate_NetUtils.setKeyValue( imei + "message", text+"");
				Log.i("CommunicateActivity","text: "+text);
				Log.i("CommunicateActivity","imei: "+imei);
			}
		});
		
		buttonClear.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				editTextMessageSend.setText("");
			}
		});

		handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub
				super.handleMessage(msg);
				
				if(msg.what == Communicate_CommunicateActivity.RECEIVED_NEW_MESSAGE){
					String messageReceived = (String) msg.obj;
					changeContentReceived(Communicate_CommunicateActivity.RECEIVED_NEW_MESSAGE,messageReceived);
				} else if(msg.what == Communicate_CommunicateActivity.INTERNET_FAILS){
					textViewHintMessage.setText("Communication Fails");
					textViewHintMessage.setTextColor(Color.RED);
				} else if(msg.what == Communicate_CommunicateActivity.INTERNET_OK){
					textViewHintMessage.setText("Communication OK");
					textViewHintMessage.setTextColor(Color.GREEN);
				}
				
			}

		};
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		Communicate_SnoopNewMessageService.setAppOnFront(false);

	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Communicate_SnoopNewMessageService.setAppOnFront(true);
		
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if(isSnoopNewMessageServiceRunning){
			// in the real game when the game is end
			// these services should be killed
			// but in a communication app
			// they should not be arbitrarily killed
			
			//stopService(snoopNewMessage);
			//isSnoopNewMessageServiceRunning = false;
		}
		
		unregisterReceiver(receiverFail);
		unregisterReceiver(receiverOK);
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
			dialog();
			return true;
		}
		return true;
	}
	
	protected void dialog() {
		AlertDialog.Builder builder = new Builder(Communicate_CommunicateActivity.this);
		builder.setMessage("Are you sure to leave?\nIf you leave, the communication will finish.");
		builder.setTitle("Hint");
		builder.setPositiveButton("YES",
		new android.content.DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				//AccoutList.this.finish();
				//System.exit(1);
//				android.os.Process.killProcess(android.os.Process.myPid());
				Communicate_CommunicateActivity.this.finish();
				
				// if snoopNewMessageSerive is alive
				// I have to kill it
				boolean isSnoopNewMessageServiceAlive = Communicate_ServiceState.getServiceState().getIsSnoopNewMessageServiceAlive();
				if(isSnoopNewMessageServiceAlive){
					// set the tag the disallows this service restart
					Communicate_ServiceState.getServiceState().setSnoopNewMessageServiceAllowedRestart(false);
					// stop the SnoopNewMessageService
					Intent intent = new Intent(Communicate_CommunicateActivity.this, Communicate_SnoopNewMessageService.class);
					stopService(intent);
					
				}

				// if snoopNewMessageSerive is alive
				// I have to kill it
				boolean isWatchDogServiceAlive = Communicate_ServiceState.getServiceState().getIsWatchDogServiceAlive();
				if(isWatchDogServiceAlive){
					// set the tag the disallows this service restart
					Communicate_ServiceState.getServiceState().setWatchDogServiceAllowedRestart(false);
					// stop the WatchDogService
					Intent intent = new Intent(Communicate_CommunicateActivity.this, Communicate_WatchDogService.class);
					stopService(intent);

				}
			}
		});
		builder.setNegativeButton("NO",
		new android.content.DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		builder.create().show();
	}
	
	private void changeContentReceived(final int receivedNewMessage,
			final String messageReceived) {
		// TODO Auto-generated method stub
		runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				if(receivedNewMessage == Communicate_CommunicateActivity.RECEIVED_NEW_MESSAGE){
					textViewReceived.setText(messageReceived);
				}
			}
		});
	}
	
	private class MyReceiverNetFail extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
//			Toast.makeText(Communicate_CommunicateActivity.this, "Connection Fails", Toast.LENGTH_SHORT).show();
			Message msgSent = new Message();
			msgSent.what = Communicate_CommunicateActivity.INTERNET_FAILS;
			Communicate_CommunicateActivity.handler.sendMessage(msgSent);
		}
	}
	private class MyReceiverNetOK extends BroadcastReceiver{
		
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			Message msgSent = new Message();
			msgSent.what = Communicate_CommunicateActivity.INTERNET_OK;
			Communicate_CommunicateActivity.handler.sendMessage(msgSent);
		}
	}
}

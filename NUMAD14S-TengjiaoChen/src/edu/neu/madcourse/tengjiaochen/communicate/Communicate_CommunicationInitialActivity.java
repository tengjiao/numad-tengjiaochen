package edu.neu.madcourse.tengjiaochen.communicate;

import edu.neu.madcourse.tengjiaochen.AboutActivity;
import edu.neu.madcourse.tengjiaochen.MainActivity;
import edu.neu.madcourse.tengjiaochen.R;
import edu.neu.madcourse.tengjiaochen.communicate.bean.Communicate_MessageContainer;
import edu.neu.madcourse.tengjiaochen.communicate.service.Communicate_JoiningGameService;
import edu.neu.madcourse.tengjiaochen.communicate.service.Communicate_WaitingForJoinService;
import edu.neu.madcourse.tengjiaochen.communicate.service.Communicate_WatchDogService;
import edu.neu.madcourse.tengjiaochen.communicate.util.Communicate_NetUtils;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;


public class Communicate_CommunicationInitialActivity extends Activity {

	public static Handler handler;
	
	protected final int CREAT_GAME = 0;
	protected final int CANCEL_CREATED_GAME = 1;
	protected final int FIND_EXISTED_GAME = 2;
	public static final int JOINING_SUCCESS = 3;
	public static final int OPPONENT_FOUND_SUCESS = 4;
	public final int INTERNET_FAIL = 5;
	public final int INTERNET_OK = 6;
	
	private boolean isJoiningGameServiceRunning = false;
	private boolean isWaitingForJoinServiceRunning = false;
	private boolean isCheckingThreadAllowedRunning = false;

	private TextView phoneID;
	private Button createButton;
	private Button joinButton;
	private Button refreshButton;
	private TextView gameCreatingState;
	private String imei;
	private RadioButton opponent1;
	private RadioButton opponent2;
	private RadioButton opponent3;
	private Intent waitingForJoinServiceintent;
	private Intent joiningGameServiceintent;

	private Button acknowledgementButton;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.communicate_activity_communicationinitial);

		phoneID = (TextView) findViewById(R.id.communicate_textViewPhoneID);

		TelephonyManager telephonyManager = (TelephonyManager) this
				.getSystemService(Context.TELEPHONY_SERVICE);
		imei = telephonyManager.getDeviceId();

		phoneID.setText(imei);

		gameCreatingState = (TextView) findViewById(R.id.communicate_textViewGameCreatingState);

		createButton = (Button) findViewById(R.id.communicate_buttonCreate);
		joinButton = (Button) findViewById(R.id.communicate_buttonJoin);
		refreshButton = (Button) findViewById(R.id.communicate_buttonRefresh);
		acknowledgementButton = (Button) findViewById(R.id.communicate_buttonAcknowledgements);
		
		opponent1 = (RadioButton)findViewById(R.id.communicate_RadioButtonOpponent1);
		opponent2 = (RadioButton)findViewById(R.id.communicate_RadioButtonOpponent2);
		opponent3 = (RadioButton)findViewById(R.id.communicate_RadioButtonOpponent3);

		createButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Button button = (Button) v;

				if (button.getText().equals("Create")) {
					joinButton.setTextColor(Color.parseColor("#33999999"));
					joinButton.setClickable(false);
					
					refreshButton.setTextColor(Color.parseColor("#33999999"));
					refreshButton.setClickable(false);
					
					Communicate_NetUtils.setKeyValue("JoinGameOponent", "");
					Communicate_NetUtils.setKeyValue("InitialEnsure", "");
					createButton.setText("Uncreate");
					gameCreatingState.setText("Creating Game... \nPlease wait...");
					Communicate_NetUtils.setKeyValue("GameCreated", imei);
					checkState(CREAT_GAME);
				} else if (button.getText().equals("Uncreate")) {

					createButton.setText("Create");
					gameCreatingState.setText("Canceling Game... \nPlease wait...");
					Communicate_NetUtils.setKeyValue("GameCreated","Canceled");
					checkState(CANCEL_CREATED_GAME);
				}
				
			}

		});
		
		refreshButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				checkState(FIND_EXISTED_GAME);
			}
			
		});

		joinButton.setOnClickListener(new OnClickListener() {
			
			@Override
			// TODO Auto-generated method stub
			public void onClick(View v) {
				
				
				
				if( opponent1.getVisibility() == View.INVISIBLE 
						&& opponent2.getVisibility() == View.INVISIBLE
						&& opponent3.getVisibility() == View.INVISIBLE
						){
					Toast.makeText(Communicate_CommunicationInitialActivity.this.getApplicationContext(), "Please press refresh to find games", Toast.LENGTH_SHORT).show();
					return;
				} else if(opponent1.getVisibility() == View.VISIBLE
						&& ( !opponent1.isChecked() )
						){
					Toast.makeText(Communicate_CommunicationInitialActivity.this.getApplicationContext(), "Please select one game to join", Toast.LENGTH_SHORT).show();
					return;
				}
				
//				NetUtils.setKeyValue("InitialEnsure", "");
				Communicate_MessageContainer.getMessageContainer().setOpponent(Communicate_MessageContainer.getMessageContainer().getIMEI());
				Communicate_MessageContainer.getMessageContainer().setIMEI(imei);
				
				
				gameCreatingState.setText("Joining the Game... \nPlease wait...");
				
				isJoiningGameServiceRunning = true;
				
				joiningGameServiceintent = new Intent(Communicate_CommunicationInitialActivity.this, Communicate_JoiningGameService.class);
				startService( joiningGameServiceintent );
			}
		});
		
		acknowledgementButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(Communicate_CommunicationInitialActivity.this, Communicate_AcknowledgemntActivity.class));
			}
		});
	
		checkInternetSituation();
		
		handler = new Handler(){
			public void handleMessage(android.os.Message msg) {
				Log.i("Handler","getMessage");
				if(msg.what == JOINING_SUCCESS){
					Log.i("CommunicationInitialActivity","into handler");
					Intent intent = new Intent(Communicate_CommunicationInitialActivity.this, Communicate_GameInitialSplashActivity.class);
					startActivity(intent);
					stopService(joiningGameServiceintent);
					Communicate_MessageContainer.getMessageContainer().setInitialEnsure("");
					finish();
				} else if(msg.what == OPPONENT_FOUND_SUCESS){
					Intent intent = new Intent(Communicate_CommunicationInitialActivity.this, Communicate_GameInitialSplashActivity.class);
					startActivity(intent);
					stopService(waitingForJoinServiceintent);
					finish();
				} else if(msg.what == INTERNET_FAIL){
					
					changeGameCreatingState(INTERNET_FAIL);
				} else if(msg.what == INTERNET_OK){
					
					changeGameCreatingState(INTERNET_OK);
				}		
			}

		};
	}
	
	

	private void checkInternetSituation() {
		// TODO Auto-generated method stub
		isCheckingThreadAllowedRunning = true;
		
		new Thread(){
			@Override
			public void run() {
				// TODO Auto-generated method stub
				super.run();
				int netFailTime = 0;
				while(true){
					
					if(!isCheckingThreadAllowedRunning){
						return;
					}
					
					Log.i("Communicate_CommunicationInitialActivity","I'm checking the Internet Situation");
					
					boolean isInternetOK = Communicate_NetUtils.checkInternetSituation();
					try {
						sleep(500);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					if(isInternetOK){
						if(netFailTime >=2){
							Message msg = new Message();
							msg.what = INTERNET_OK;
							handler.sendMessage(msg);
						}
						netFailTime = 0;
						continue;
					} else {
						netFailTime++;
					}
					
					if(netFailTime == 2 ){
						Message msg = new Message();
						msg.what = INTERNET_FAIL;
						handler.sendMessage(msg);
					}
					
				}
				
			}
		}.start();
		
		
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		isCheckingThreadAllowedRunning = false;
		
	}
	
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		
		Log.i("CommunicationInitialActivity","onPause");
		
		if(isJoiningGameServiceRunning){
			stopService(joiningGameServiceintent);
			isJoiningGameServiceRunning = false ;
			Communicate_GameInitialSplashActivity.setCreating(false);
		} else if(isWaitingForJoinServiceRunning  ){
			stopService(waitingForJoinServiceintent);
			isWaitingForJoinServiceRunning = false ;
			Communicate_GameInitialSplashActivity.setCreating(true);
		}
		
		Communicate_MessageContainer.getMessageContainer().setIMEI(imei);
	}
	
	private void checkState(final int GAME_STATE) {
		// TODO Auto-generated method stub
		new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				if (GAME_STATE == Communicate_CommunicationInitialActivity.this.CREAT_GAME) {
					int attempt =0;
					Communicate_NetUtils.getValue("GameCreated");
					while (true) {
						String imei = Communicate_MessageContainer.getMessageContainer().getIMEI();
						if (imei.equals(Communicate_CommunicationInitialActivity.this.imei)) {
							changeGameCreatingState(CREAT_GAME);
							break;
						}
						
						try {
							Thread.sleep(300);
							attempt++;
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						
						if( attempt % 3 ==0 ){
							Communicate_NetUtils.getValue("GameCreated");
						}
					}
				} else if(GAME_STATE == Communicate_CommunicationInitialActivity.this.CANCEL_CREATED_GAME){
					int attempt =0;
					Communicate_NetUtils.getValue("GameCreated");
					while (true) {
						String imei = Communicate_MessageContainer.getMessageContainer().getIMEI();
						if (imei.equals("Canceled")) {
							changeGameCreatingState(CANCEL_CREATED_GAME);
							break;
						}
						
						try {
							Thread.sleep(300);
							attempt++;
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						
						if( attempt % 3 ==0 ){
							Communicate_NetUtils.getValue("GameCreated");
						}
					}
				} else if(GAME_STATE == Communicate_CommunicationInitialActivity.this.FIND_EXISTED_GAME){
					Log.i("CommunicationInitialActivity","I am in FIND_EXISTED_GAME");
					int attempt =0;
					Communicate_NetUtils.getValue("GameCreated");
					while (true) {
						String opponent = Communicate_MessageContainer.getMessageContainer().getIMEI();
						if (!opponent.equals("none")) {
							changeGameCreatingState(FIND_EXISTED_GAME);
							break;
						}
						
						try {
							Thread.sleep(300);
							attempt++;
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						
						if( attempt % 3 ==0 ){
							Communicate_NetUtils.getValue("GameCreated");
						}
					}
				}
			}

		}).start();
	}

	private void changeGameCreatingState(final int GAME_STATE) {
		// TODO Auto-generated method stub
		runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				if (GAME_STATE == Communicate_CommunicationInitialActivity.this.CREAT_GAME) {
					gameCreatingState.setText("Game Created! Waiting for joining...");
					waitingForJoinServiceintent = new Intent(Communicate_CommunicationInitialActivity.this, Communicate_WaitingForJoinService.class);
					startService( waitingForJoinServiceintent );
					isWaitingForJoinServiceRunning = true;
				} else if(GAME_STATE == Communicate_CommunicationInitialActivity.this.CANCEL_CREATED_GAME){
					Log.i("CommunicationInitialActivity", "I am running the Game Canceled");
					gameCreatingState.setText("Game Canceled!");
					joinButton.setTextColor(Color.parseColor("#FF000000"));
					joinButton.setClickable(true);
					
					refreshButton.setTextColor(Color.parseColor("#FF000000"));
					refreshButton.setClickable(true);
					
					stopService(waitingForJoinServiceintent);
					isWaitingForJoinServiceRunning = false;
				} else if(GAME_STATE == Communicate_CommunicationInitialActivity.this.FIND_EXISTED_GAME){
					String opponent = Communicate_MessageContainer.getMessageContainer().getIMEI();
					if(opponent.equals("") || opponent.equals("Canceled") || opponent.equals(imei)){
						opponent1.setVisibility(View.INVISIBLE);
						opponent2.setVisibility(View.INVISIBLE);
						opponent3.setVisibility(View.INVISIBLE);
					} else {
						opponent1.setText(opponent);
						opponent1.setVisibility(View.VISIBLE);
						Log.i("CommunicationInitialActivity","I am changing oppnent1");
					}
				} 
				//handleINTERNET_FAIL �� INTERNET_OK
				else if(GAME_STATE == INTERNET_FAIL){
					
					Toast.makeText(getApplicationContext(), "Internet Fails", Toast.LENGTH_SHORT).show();
				} else if(GAME_STATE == INTERNET_OK){
					
					Toast.makeText(getApplicationContext(), "Internet OK", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
	

}

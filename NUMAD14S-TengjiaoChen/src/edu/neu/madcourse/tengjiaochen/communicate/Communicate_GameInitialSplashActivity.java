package edu.neu.madcourse.tengjiaochen.communicate;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import edu.neu.madcourse.tengjiaochen.R;
import edu.neu.madcourse.tengjiaochen.communicate.bean.Communicate_MessageContainer;
import edu.neu.madcourse.tengjiaochen.communicate.service.Communicate_WatchDogService;
import edu.neu.madcourse.tengjiaochen.communicate.util.Communicate_NetUtils;

public class Communicate_GameInitialSplashActivity extends Activity {

	protected final int SEND_COUNTER = 0;
	protected final int GET_COUNTER = 1;
	protected final int CHANGE_COUNTER = 2;
	protected final int INTERNET_FAILS = 3;
	
	private TextView initialEnsure;
	private TextView iMEI;
	private TextView opponent;
	private static boolean isCreating;
	private TextView counterTextView;
	private Intent watchDogServiceintent;

	public static void setCreating(boolean isCreating) {
		Communicate_GameInitialSplashActivity.isCreating = isCreating;
	}
	
	private Handler handler = new Handler(){
		public void handleMessage(android.os.Message msg) {
			Log.i("GameInitialSplashActivity","Handler getMessage");
			if(msg.what == SEND_COUNTER ){
				Log.i("GameInitialSplashActivity"," in SEND_COUNTER ");
				int counterInt= (Integer)msg.obj;
				changeUI(CHANGE_COUNTER,counterInt);
			} else if(msg.what == GET_COUNTER ){
				int counterInt= (Integer)msg.obj;
				changeUI(CHANGE_COUNTER,counterInt);
			}	
		}

	};
	private MyReceiver receiver;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.communicate_activity_gameinitsplash);

		receiver = new MyReceiver();   
        IntentFilter filter = new IntentFilter("edu.neu.mascourse.tengjiaochen.COMMUNICATION_FAILS");   
        registerReceiver(receiver, filter);
		
		opponent = (TextView) findViewById(R.id.communicate_textViewOpponent);
		iMEI = (TextView) findViewById(R.id.communicate_textViewIMEI);

		initialEnsure = (TextView) findViewById(R.id.communicate_textViewInitialEnsure);

		counterTextView = (TextView) findViewById(R.id.communicate_textView_Counter);
		
		opponent.setText("opponent: " + Communicate_MessageContainer.getMessageContainer().getOpponent());
		
		TelephonyManager telephonyManager = (TelephonyManager) this
				.getSystemService(Context.TELEPHONY_SERVICE);
		String imei = telephonyManager.getDeviceId();
		Communicate_MessageContainer.getMessageContainer().setIMEI(imei);
		
		iMEI.setText("iMEI: " + Communicate_MessageContainer.getMessageContainer().getIMEI());
		initialEnsure.setText("initialEnsure: "
				+ Communicate_MessageContainer.getMessageContainer().getInitialEnsure());

		watchDogServiceintent = new Intent(Communicate_GameInitialSplashActivity.this, Communicate_WatchDogService.class);
		
		if (isCreating) {
			Log.i("GameInitialSplashActivity","creatGame");
			sendCounterInfo();
		} else {
			Log.i("GameInitialSplashActivity","joinTheGame");
			waitCounterInfo();
		}

	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unregisterReceiver(receiver);
	}

	private void waitCounterInfo() {
		// TODO Auto-generated method stub
		Communicate_MessageContainer.getMessageContainer().setWatchDogState(null);
		startService(watchDogServiceintent);
		
		new Thread(new Runnable() {
			public void run() {
				int counterInt = Integer.parseInt(""+counterTextView.getText());
				int attempt = 0;
				while(counterInt > 0){
					counterInt--;
					Communicate_NetUtils.getValue("counter");
					while(true){
						
						if(Communicate_MessageContainer.getMessageContainer().getCounter().equals(""+counterInt)){
							Message msgSent = new Message();
							msgSent.what = GET_COUNTER;
							msgSent.obj = counterInt;
							handler.sendMessage(msgSent); 
							break;
						}
						
						try {
							Thread.sleep(50);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						if(attempt % 4 == 0){
							Communicate_NetUtils.getValue("counter");
						}
					}
				}
			}
		}).start();
	}

	private void sendCounterInfo() {
		// TODO Auto-generated method stub
		Communicate_MessageContainer.getMessageContainer().setWatchDogState("0");
		startService(watchDogServiceintent);
		
		new Thread(new Runnable() {
			public void run() {
				int counterInt = Integer.parseInt(""+counterTextView.getText());
				while(counterInt > 0){
					counterInt--;
					Log.i("GameInitialSplashActivity","sendCounterInfo, sending : "+ counterInt);
					
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					Communicate_NetUtils.setKeyValue("counter", ""+counterInt);
					Communicate_NetUtils.getValue("counter");
					
					int attempt = 0;
					
					while(true){
						
						Log.i("GameInitialSplashActivity","in the checking block");
						String counter = Communicate_MessageContainer.getMessageContainer().getCounter();
						Log.i("GameInitialSplashActivity","counter : " + counter);
						if(counter.equals(""+counterInt)){
							Log.i("GameInitialSplashActivity","data from net, counterInt: "+counterInt);
							Message msgSent = new Message();
							msgSent.what = SEND_COUNTER;
							msgSent.obj = counterInt;
							handler.sendMessage(msgSent); 
							break;
						}
						
						try {
							Thread.sleep(100);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						
						if(attempt % 4 == 0){
							Communicate_NetUtils.setKeyValue("counter", ""+counterInt);
							Communicate_NetUtils.getValue("counter");
						}
					}
				}
			}
		}).start();
	}

	private void changeUI(final int state , final int counterInt) {
		// TODO Auto-generated method stub
		runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				if(state == CHANGE_COUNTER){
					Log.i("GameInitialSplashActivity","changing UI thread : " + counterInt);
					counterTextView.setText(""+counterInt);
					
					if(counterInt == 0){
						Intent intent = new Intent(Communicate_GameInitialSplashActivity.this, Communicate_CommunicateActivity.class);
						startActivity(intent);
						finish();
					}
				} else if(state == INTERNET_FAILS){
					Toast.makeText(Communicate_GameInitialSplashActivity.this, "Internet Fails \nPlease Quit and Try again", Toast.LENGTH_LONG).show();
				}
			}
		});
	}
	
	private class MyReceiver extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			changeUI(INTERNET_FAILS,0);
		}
	}
	
//	private void internetFailNotice(){
//		runOnUiThread(new Runnable() {
//			
//			@Override
//			public void run() {
//				Toast.makeText(Communicate_GameInitialSplashActivity.this.getApplicationContext(), "Internet Fails \nPlease Quit and Try again", Toast.LENGTH_LONG).show();
//			}
//			
//		});	
//	}
	
}

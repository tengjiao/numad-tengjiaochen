package edu.neu.madcourse.tengjiaochen.communicate.bean;

import android.util.Log;

public class Communicate_MessageContainer {
	
	private static Communicate_MessageContainer messageContainer;
	private Communicate_MessageContainer() {}
	public static Communicate_MessageContainer getMessageContainer(){
		
		if(messageContainer == null){
			messageContainer = new Communicate_MessageContainer();
		}
		
		return messageContainer;
	}
	
	private String watchDogState = null;
	private String sendGetData = null;
	private String opponent = "none";
	private String Joinopponent = "none";
	private String IMEI = "";
	private String InitialEnsure = "";
	private String counter = "";
	private String messageSent = "";
	private String messageReceived = "";
	private String opponentLiveState = "";
	private String opponentGameState = "";
	private String highScoreList = "";
	private int[] highScoretop5 = new int[5];
	
	
	public int[] getHighScoretop5() {
		return highScoretop5;
	}
	
	public void setHighScoretop5(int[] highScoretop5) {
		this.highScoretop5 = highScoretop5;
	}
	
	public String getHighScoreList() {
		return highScoreList;
	}
	public void setHighScoreList(String highScoreList) {
		this.highScoreList = highScoreList;
	}
	
	public String getOpponentGameState() {
		return opponentGameState;
	}
	
	public void setOpponentGameState(String opponentGameState) {
		this.opponentGameState = opponentGameState;
	}
	
	public String getOpponentLiveState() {
		return opponentLiveState;
	}
	public void setOpponentLiveState(String opponentLiveState) {
		this.opponentLiveState = opponentLiveState;
	}
	public String getMessageSent() {
		return messageSent;
	}
	
	public void setMessageSent(String messageSent) {
		this.messageSent = messageSent;
	}
	
	public String getMessageReceived() {
		return messageReceived;
	}
	
	public void setMessageReceived(String messageReceived) {
		this.messageReceived = messageReceived;
	}
	
	public String getInitialEnsure() {
		return InitialEnsure;
	}

	public void setInitialEnsure(String initialEnsure) {
		this.InitialEnsure = initialEnsure;
	}

	public String getIMEI() {
		return IMEI;
	}

	public void setIMEI(String IMEI) {
		this.IMEI = IMEI;
	}

	public String getOpponent() {
		return opponent;
	}

	public void setOpponent(String opponent) {
		this.opponent = opponent;
	}

	public String getWatchDogState() {
		return watchDogState;
	}



	public void setWatchDogState(String watchDogState) {
		this.watchDogState = watchDogState;
	}

	public String getSendGetData() {
		return sendGetData;
	}

	public void setSendGetData(String sendGetData) {
		this.sendGetData = sendGetData;
	}

	public String getCounter() {
		return counter;
	}
	
	public void setCounter(String counter) {
		this.counter = counter;
	}

	public String getJoinopponent() {
		return Joinopponent;
	}
	
	public void setJoinopponent(String joinopponent) {
		Joinopponent = joinopponent;
	}
	
	public void setValue(String key , String value){
		if(key.equals("WatchDogState")){
			this.watchDogState = value;
		} else if (key.equals("key")){
			this.sendGetData = value;
		} else if (key.equals("GameOponent")){
			this.opponent = value;
		} else if (key.equals("GameCreated")){
			this.IMEI = value;
		} else if(key.equals("InitialEnsure")){
			this.InitialEnsure = value;
		} else if(key.equals("counter")){
			Log.i("MessageContainer","Changing the counter to :" + value);
			this.counter = value;
		} else if( key.equals("JoinGameOponent") ){
			this.Joinopponent = value;			
		} else if( key.equals( IMEI + "message") ){
			this.messageSent = value;
		} else if( key.equals( opponent + "message") ){
			this.messageReceived = value;
		} else if( key.equals( opponent + "-live") ){
			this.opponentLiveState = value;
		} else if( key.equals( opponent + "gamestate" ) ){
			this.opponentGameState = value; 
		} else if( key.equals("twoplayerhighscore") ){
			this.highScoreList = value; 
		}
			
	}

}

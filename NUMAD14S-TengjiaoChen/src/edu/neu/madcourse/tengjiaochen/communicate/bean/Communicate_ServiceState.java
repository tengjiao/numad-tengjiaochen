package edu.neu.madcourse.tengjiaochen.communicate.bean;

public class Communicate_ServiceState {
	private static Communicate_ServiceState serviceState;
	private Communicate_ServiceState() {}
	public static Communicate_ServiceState getServiceState(){
		
		if(serviceState == null){
			serviceState = new Communicate_ServiceState();
		}
		
		return serviceState;
	}
	
	private boolean isSnoopNewMessageServiceAlive = false;
	private boolean isWatchDogServiceAlive = false;
	
	private boolean isSnoopNewMessageServiceAllowedRestart = false;
	private boolean isWatchDogServiceAllowedRestart = false;
	
	public boolean getIsSnoopNewMessageServiceAllowedRestart() {
		return isSnoopNewMessageServiceAllowedRestart;
	}
	public void setSnoopNewMessageServiceAllowedRestart(
			boolean isSnoopNewMessageServiceAllowedRestart) {
		this.isSnoopNewMessageServiceAllowedRestart = isSnoopNewMessageServiceAllowedRestart;
	}
	
	public boolean getIsWatchDogServiceAllowedRestart() {
		return isWatchDogServiceAllowedRestart;
	}
	public void setWatchDogServiceAllowedRestart(
			boolean isWatchDogServiceAllowedRestart) {
		this.isWatchDogServiceAllowedRestart = isWatchDogServiceAllowedRestart;
	}

	public boolean getIsSnoopNewMessageServiceAlive() {
		return isSnoopNewMessageServiceAlive;
	}
	public void setSnoopNewMessageServiceAlive(boolean isSnoopNewMessageServiceAlive) {
		this.isSnoopNewMessageServiceAlive = isSnoopNewMessageServiceAlive;
	}
	
	public boolean getIsWatchDogServiceAlive() {
		return isWatchDogServiceAlive;
	}
	public void setWatchDogServiceAlive(boolean isWatchDogServiceAlive) {
		this.isWatchDogServiceAlive = isWatchDogServiceAlive;
	}
	
}
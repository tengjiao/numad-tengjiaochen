package edu.neu.madcourse.tengjiaochen.communicate.service;

import java.util.Random;
import java.util.regex.Pattern;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;

import edu.neu.madcourse.tengjiaochen.communicate.Communicate_CommunicationInitialActivity;
import edu.neu.madcourse.tengjiaochen.communicate.bean.Communicate_MessageContainer;
import edu.neu.madcourse.tengjiaochen.communicate.util.Communicate_NetUtils;

public class Communicate_WaitingForJoinService extends Service {

	private Thread waitingForJoinThread;
	private boolean isThreadAllowedRunning;
	
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		Communicate_MessageContainer.getMessageContainer().setInitialEnsure("");
		isThreadAllowedRunning = true;
		waitingForJoinThread = new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub

				int attempt = 0;
				Communicate_NetUtils.getValue("JoinGameOponent");
				Random random = new Random();
				int nextInt = random.nextInt(100);
				Communicate_NetUtils.setKeyValue("InitialEnsure", ""+nextInt);

				Log.i("WaitingForJoinService", "nextInt : " + nextInt); 
				
				while (true) {
					if(!isThreadAllowedRunning){
						return;
					}
					
					boolean isCommunicationOK = false;
					
					// opponent check
					while (true) {
						if(!isThreadAllowedRunning){
							return;
						}
						String joinOpponent = Communicate_MessageContainer.getMessageContainer().getJoinopponent();
						// judge whether the opponent is composed of numbers
						Log.i("WaitingForJoinService", "opponent : " + joinOpponent); 
						
						if (Pattern.compile("[a-zA-Z0-9]+").matcher(joinOpponent)
								.matches()) {
							
							Log.i("WaitingForJoinService", "opponent : " + joinOpponent); 
							Communicate_MessageContainer.getMessageContainer().setOpponent(joinOpponent);
							break;
						}

						try {
							Thread.sleep(300);
							attempt++;
						} catch (InterruptedException e) {
							e.printStackTrace();
						}

						if (attempt % 9 == 0) {
							Communicate_NetUtils.getValue("JoinGameOponent");
						}
					}
					
					Log.i("WaitingForJoinService", "Jump out of the 1st loop"); 
					
					attempt = 0;
					
					Communicate_NetUtils.getValue("InitialEnsure");
					
					Log.i("WaitingForJoinService", "I created an integer " + nextInt); 
					
					// ensure InitialEnsure
					// shaking hands with opponent
					while(true){
						
						if(!isThreadAllowedRunning){
							return;
						}
						
						String initialEnsure = Communicate_MessageContainer.getMessageContainer().getInitialEnsure();
						
						Log.i("WaitingForJoinService", "initialEnsure I got is " + initialEnsure); 
						
						if(Pattern.compile("[a-zA-Z0-9]+").matcher(initialEnsure).matches()
								&& Integer.parseInt(initialEnsure)<=nextInt+10
								&& nextInt+1<=Integer.parseInt(initialEnsure)
								){
							
							Log.i("WaitingForJoinService", "In if, initialEnsure I got is " + initialEnsure); 
							isCommunicationOK = true;
							break;
						} 

						try {
							Thread.sleep(10);
							attempt++;
						} catch (InterruptedException e) {
							e.printStackTrace();
						}

						if (attempt % 20 == 0) {
							Communicate_NetUtils.getValue("InitialEnsure");
						}
						
						if(attempt == 80){
							break;
						}
						
					}
					
					if(isCommunicationOK){
						Message msgSent = new Message();
						msgSent.what = Communicate_CommunicationInitialActivity.OPPONENT_FOUND_SUCESS;
						Communicate_CommunicationInitialActivity.handler.sendMessage(msgSent); 
						break;
					} else{
						continue;
					}
				}
			}
		});

		waitingForJoinThread.start();

	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		Log.i("WaitingForJoinService", "Waiting for Joining has been destroyed"); 
		isThreadAllowedRunning = false;
//		NetUtils.setKeyValue("InitialEnsure", "");
		Communicate_MessageContainer.getMessageContainer().setInitialEnsure("");
		
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

}

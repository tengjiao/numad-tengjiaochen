package edu.neu.madcourse.tengjiaochen.communicate.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;

import edu.neu.madcourse.tengjiaochen.communicate.Communicate_CommunicateActivity;
import edu.neu.madcourse.tengjiaochen.communicate.bean.Communicate_MessageContainer;
import edu.neu.madcourse.tengjiaochen.communicate.bean.Communicate_ServiceState;
import edu.neu.madcourse.tengjiaochen.communicate.util.Communicate_NetUtils;
// 13-127 has been commented out
public class Communicate_WatchDogService extends Service {
	
	private String watchDogState = null;
	private boolean isConnectionOK = true;
	private boolean isThreadAllowedRunning = true;
	private SnoopMessageReceiver snoopMessageReceiver;
	
	@Override
	public void onCreate() {	// 服务被创建的时候执行
		super.onCreate();
		
		// set a tag that this service is alive
		Communicate_ServiceState.getServiceState().setWatchDogServiceAlive(true);
		
		// set a tag that allows this service restart
		Communicate_ServiceState.getServiceState().setWatchDogServiceAllowedRestart(true);

		
		snoopMessageReceiver = new SnoopMessageReceiver();   
        IntentFilter filter = new IntentFilter("edu.neu.mascourse.tengjiaochen.SNOOP_NEW_MESSAGE_SERVICE_KILLED");   
        registerReceiver(snoopMessageReceiver, filter);
		
		isThreadAllowedRunning = true;
		
		// setting my live State to Yes
		new Thread() {
			public void run() {
				// constantly set my imei - live to yes
				String imei = Communicate_MessageContainer.getMessageContainer().getIMEI();
				while(true){
					if(!isThreadAllowedRunning){
						return;
					}

					Communicate_NetUtils.setKeyValue(imei + "-live", "live");
					
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}.start();
		
		// Checking if my opponent is alive
		
		new Thread() {
			public void run() {
				String opponent = Communicate_MessageContainer.getMessageContainer().getOpponent();
				int breadJudge = 0;
				while(true){
					if(!isThreadAllowedRunning){
						return;
					}
					
					// set my opponent live state to ""
					Communicate_NetUtils.setKeyValue(opponent+"-live", "");
					
					// sleep
					
					try {
						sleep(1000);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					// get my opponent live state
					Communicate_NetUtils.getValue(opponent+"-live");
					
					// sleep
					try {
						sleep(1000);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					// check
					String opponentLiveState = Communicate_MessageContainer.getMessageContainer().getOpponentLiveState();
					
					if(opponentLiveState.equals("live")){
						Intent intent = new Intent("edu.neu.mascourse.tengjiaochen.COMMUNICATION_OK");
						sendBroadcast(intent);
						breadJudge = 0;
						continue;
					} else {
						breadJudge++;
					}
					
					if(breadJudge == 2){
						Intent intent = new Intent("edu.neu.mascourse.tengjiaochen.COMMUNICATION_FAILS");
						sendBroadcast(intent);
					}
					// double check
				}
			}
		}.start();
		
	}
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		isThreadAllowedRunning =false;
		
		boolean isWatchDogServiceAllowedRestart = Communicate_ServiceState.getServiceState().getIsWatchDogServiceAllowedRestart();
		if(isWatchDogServiceAllowedRestart){
			Intent intent = new Intent("edu.neu.mascourse.tengjiaochen.WATCHDOG_KILLED");
			sendBroadcast(intent);
		}
		
		unregisterReceiver(snoopMessageReceiver);
		
		// set a tag that this service is alive
		Communicate_ServiceState.getServiceState().setWatchDogServiceAlive(false);

	}
	
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
	private class SnoopMessageReceiver extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			startService(new Intent(Communicate_WatchDogService.this,Communicate_SnoopNewMessageService.class));
		}
	}
}

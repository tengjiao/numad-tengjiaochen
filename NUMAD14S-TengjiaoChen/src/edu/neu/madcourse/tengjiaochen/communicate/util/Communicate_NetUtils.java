package edu.neu.madcourse.tengjiaochen.communicate.util;


import edu.neu.madcourse.tengjiaochen.communicate.bean.Communicate_MessageContainer;
import edu.neu.mhealth.api.KeyValueAPI;

public class Communicate_NetUtils {

	private static String newValue;

	public static void setKeyValue(final String key, final String value) {
		new Thread() {
			@Override
			public void run() {
				KeyValueAPI.put("EverCool", "ctj.890716", key, value);
			}
		}.start();
	}

	public static void getValue(final String key) {
		new Thread() {
			@Override
			public void run() {
				String getValue = null;
				getValue = KeyValueAPI.get("EverCool", "ctj.890716", key);
				newValue = getValue;
//				NetUtils.setNewValue(getValue);
				Communicate_MessageContainer.getMessageContainer().setValue(key,newValue);
			}
		}.start();
		
//		return newValue;
	}

	public static boolean checkInternetSituation(){
		return KeyValueAPI.isServerAvailable();
	}
	
	public static String getNewValue() {
		return newValue;
	}

	public static void setNewValue(String newValue) {
		Communicate_NetUtils.newValue = newValue;
	}
}

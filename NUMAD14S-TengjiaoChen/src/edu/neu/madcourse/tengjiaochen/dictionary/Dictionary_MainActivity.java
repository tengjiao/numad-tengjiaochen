package edu.neu.madcourse.tengjiaochen.dictionary;

import java.util.HashSet;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import edu.neu.madcourse.tengjiaochen.MainActivity;
import edu.neu.madcourse.tengjiaochen.R;
import edu.neu.madcourse.tengjiaochen.dictionary.db.dao.DictionaryRecordDao;
import edu.neu.madcourse.tengjiaochen.dictionary.db.dao.DictionaryRecordDaoFactory;
import edu.neu.madcourse.tengjiaochen.dictionary.db.domain.DictionaryRecord;

public class Dictionary_MainActivity extends Activity {

	protected MediaPlayer music = null;
	protected static final String TAG = "MainActivity";
	protected EditText wordToFind;
	protected DictionaryRecordDao dicDao;
	protected DictionaryRecordDaoFactory dicDaoFactory;
	protected Button clear;
	protected Button returnToMenu;
	protected Button acknowledgments;
	protected LinearLayout wordHistory;
	protected ScrollView wordHistoryScroll;
	protected HashSet<String> wordHasFound = new HashSet<String>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dictionary_main_activity);

		dicDaoFactory = DictionaryRecordDaoFactory
				.getDictionaryRecordDaoFactory();
		wordHistory = (LinearLayout) findViewById(R.id.DictionaryWordHistory);
		wordHistoryScroll = (ScrollView) findViewById(R.id.DictionaryWordHistoryScroll);
		wordToFind = (EditText) findViewById(R.id.DictionaryWordToFind);
		clear = (Button) findViewById(R.id.DictionaryClearButton);
		returnToMenu = (Button) findViewById(R.id.DictionaryReturntoMenuButton);
		acknowledgments = (Button) findViewById(R.id.DictionaryAcknowledgmentsButton);

		// TextView tv = new TextView(this);
		wordToFind.addTextChangedListener(new TextWatcher() {

			@SuppressLint("ShowToast")
			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if (s.toString().length() >= 3) {
					Log.i(TAG, s.toString());
					if (s.toString().length() > 14) {
						dicDao = dicDaoFactory.getDictionaryRecordDao(15);
					} else {
						dicDao = dicDaoFactory.getDictionaryRecordDao(s
								.toString().length());
					}

					if (dicDao.find(new DictionaryRecord(s.toString()))) {
						PlayMusic(R.raw.dictionary_find_word);
						if (!wordHasFound.contains(s.toString())) {
							wordHasFound.add(s.toString());
							TextView tv = new TextView(
									Dictionary_MainActivity.this);
							tv.setLayoutParams(new ViewGroup.LayoutParams(
									ViewGroup.LayoutParams.WRAP_CONTENT,
									ViewGroup.LayoutParams.WRAP_CONTENT));
							tv.setText(s.toString());
							tv.setTextSize(15);
							wordHistory.addView(tv);

							if (wordHasFound.size() >= 5) {
								// scroll to the lastest added
								wordHistoryScroll.post(new Runnable() {
									public void run() {
										wordHistoryScroll
												.fullScroll(ScrollView.FOCUS_DOWN);
									}
								});
							}
						}
					}
				}
			}

			@Override
			public void afterTextChanged(Editable s) {

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {

			}
		});

		clear.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				wordToFind.setText("");
				wordHasFound.clear();
				wordHistory.removeAllViews();
			}
		});

		returnToMenu.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
		acknowledgments.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(Dictionary_MainActivity.this,Dictionary_Acknowdgments.class));
			}
		});

	}

	/**
	 * play the music with MusicID
	 * 
	 * @param MusicId
	 */
	private void PlayMusic(int MusicId) {
		music = MediaPlayer.create(Dictionary_MainActivity.this, MusicId);
		music.setAudioStreamType(AudioManager.STREAM_MUSIC);
		music.setVolume(2, 2);
//		AudioManager audiomanage = (AudioManager) getSystemService(Dictionary_MainActivity.AUDIO_SERVICE);// ��ȡ��������
//
//		int ringvolumeSR = audiomanage
//				.getStreamVolume(AudioManager.STREAM_RING);
//		music.setVolume(ringvolumeSR, ringvolumeSR);
		
		// make the volumn available to the users
		Dictionary_MainActivity.this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
		music.start();
	}
}

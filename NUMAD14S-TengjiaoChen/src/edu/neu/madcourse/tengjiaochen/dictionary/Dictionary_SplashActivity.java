package edu.neu.madcourse.tengjiaochen.dictionary;


import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import edu.neu.madcourse.tengjiaochen.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.AlphaAnimation;
import android.widget.ProgressBar;
import android.widget.Toast;

public class Dictionary_SplashActivity extends Activity {
	
	protected ProgressBar progressBar;
	private Handler handler = new Handler() {
		
		public void handleMessage(android.os.Message msg) {
			
			loadMainUI();
		};
	};
	
	@Override
	// TODO Auto-generated method stub
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.dictionary_splash_activity);
		progressBar = (ProgressBar) findViewById(R.id.dictionary_progress_bar);
		
		// copy the db here
		new Thread() {
			public void run() {
				try {
					progressBar.setMax(13);
					for(int i=3;i<=15;i++){
						String DBName = "dictionary_len";
						DBName = DBName + i +".db";
						
						if(i==15){
							DBName = "dictionary_len14_plus.db";
						}
						
						File file = new File(getFilesDir(), DBName);
						if (file.exists() && file.length() > 0) {
//							Log.i(TAG, "db has been copied");
						} else {
							InputStream is = getAssets().open(DBName);
							// data/data/package/files/address.db
							FileOutputStream fos = new FileOutputStream(file);
							byte[] buffer = new byte[1024];
							int len = 0;
							while ((len = is.read(buffer)) != -1) {
								fos.write(buffer, 0, len);
							}
							is.close();
							fos.close();
//							showToastInMainThread(DBName + "is successfully copied");
						}
						
						progressBar.setProgress(i-2);
						sleep(150);
						if(i==15){
							handler.sendEmptyMessage(13);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
//					showToastInMainThread("fail to copy the data");
				}

			};
		}.start();
	}
	
	private void loadMainUI() {
		Intent intent = new Intent(Dictionary_SplashActivity.this, Dictionary_MainActivity.class);
		startActivity(intent);
		// if not finished
		// user might see this one more time
		// that'll be weird 
		finish();
	}
	
//	/**
//	 * show text on Toast
//	 * just for testing
//	 * 
//	 * @param text
//	 */
//	public void showToastInMainThread(final String text) {
//		runOnUiThread(new Runnable() {
//			@Override
//			public void run() {
//				Toast.makeText(getApplicationContext(), text, 1).show();
//
//			}
//		});
//	}
}


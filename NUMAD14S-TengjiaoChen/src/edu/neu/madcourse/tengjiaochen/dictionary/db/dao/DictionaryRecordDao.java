package edu.neu.madcourse.tengjiaochen.dictionary.db.dao;

import java.util.HashSet;
import java.util.Random;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import edu.neu.madcourse.tengjiaochen.dictionary.db.domain.DictionaryRecord;

public class DictionaryRecordDao {
	
	/**
	 * find the word
	 * @param word DictionaryRecord
	 * @return true exist false not exist
	 */
	public boolean find(DictionaryRecord word){
		
		String TableName = "dictionary_len";
		int length = word.getWord().length();
		TableName = TableName +length;
		if(length >= 15){
			TableName = "dictionary_len14_plus";
		}
		
		// �ҵ�TableName = DBName
		SQLiteDatabase db = SQLiteDatabase.openDatabase(
				"/data/data/edu.neu.madcourse.tengjiaochen/files/"+TableName+".db", null,
				SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		Cursor cursor = db.rawQuery("select * from " + TableName + " where word=?", new String[]{word.getWord()});
		boolean result = cursor.moveToNext();
		cursor.close();
		db.close();
		return result;
	}

	/**
	 * find word with Specified Letters
	 * @param lettersNeeded
	 * @param length
	 * @return
	 */
	public String getWordWithSpecifiedLetters(String[] lettersNeeded, int length) {

		String TableName = "dictionary_len";
		// to avoid the 'a' 'a' 'c' situation
		HashSet<String> lettersNeed = new HashSet<String>();
		TableName = TableName +length;
		if(length >= 15){
			TableName = "dictionary_len14_plus";
		}
		
		lettersNeed.clear();

		// �ҵ�TableName = DBName
		SQLiteDatabase db = SQLiteDatabase.openDatabase(
				"/data/data/edu.neu.madcourse.tengjiaochen/files/"+TableName+".db", null,
				SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		String queryCondition=" where ";
		for(int i=0; i<lettersNeeded.length;i++){
			queryCondition = queryCondition + "word like "+"'%"+lettersNeeded[i]+"%";

			if(lettersNeed.contains(lettersNeeded[i])){
				queryCondition = queryCondition +lettersNeeded[i]+"%";
			}
			
			queryCondition = queryCondition +"'";
			
			if(i!=lettersNeeded.length-1){
				queryCondition = queryCondition +" and ";
			}
			
			lettersNeed.add(lettersNeeded[i]);
		}
		
		Log.i("lettersNeeded.length", ""+lettersNeeded.length);
		Log.i("Query Condition", queryCondition);
		Log.i("Query sentence", "select * from " + TableName +queryCondition);
		
		Cursor cursor =db.rawQuery("select * from " + TableName +queryCondition,null);
		
		// get the number of the results
		int count = cursor.getCount();
		Log.i("DictionaryRecordDao", "number of results: " + count);
		
		// if there is no results,we just return null
		if(count == 0){
			return null;
		}
		
		String result = null;

		// get a random result
		Random rand = new Random();
		int randomIndex = rand.nextInt( ( count % 100  )/2 +1 )+1;
		Log.i("DictionaryRecordDao", "now the index is: " + randomIndex);
		for( int i=0;i<randomIndex;i++){
			cursor.moveToNext();
			Log.i("DictionaryRecordDao", "now the word is: " + cursor.getString(0));
		}
		
		result = cursor.getString(0);
		
		cursor.close();
		db.close();
		
		return result;
	}	
}

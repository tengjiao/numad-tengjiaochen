package edu.neu.madcourse.tengjiaochen.dictionary.db.dao;



public class DictionaryRecordDaoFactory {
	// private Context context;

	private DictionaryRecordDaoFactory() {
		super();
	}

	private static DictionaryRecordDaoFactory dictionaryRecordDaoFactory;

	static {
		dictionaryRecordDaoFactory = new DictionaryRecordDaoFactory();
	}

	public static DictionaryRecordDaoFactory getDictionaryRecordDaoFactory() {
		// dictionaryRecordDaoFactory.context = context;
		return dictionaryRecordDaoFactory;
	}

	private DictionaryRecordDao dicDaoLenth3;
	private DictionaryRecordDao dicDaoLenth4;
	private DictionaryRecordDao dicDaoLenth5;
	private DictionaryRecordDao dicDaoLenth6;
	private DictionaryRecordDao dicDaoLenth7;
	private DictionaryRecordDao dicDaoLenth8;
	private DictionaryRecordDao dicDaoLenth9;
	private DictionaryRecordDao dicDaoLenth10;
	private DictionaryRecordDao dicDaoLenth11;
	private DictionaryRecordDao dicDaoLenth12;
	private DictionaryRecordDao dicDaoLenth13;
	private DictionaryRecordDao dicDaoLenth14;
	private DictionaryRecordDao dicDaoLenth14_plus;

	public DictionaryRecordDao getDictionaryRecordDao(int length) {
		switch (length) {
		case 3:
			if (dicDaoLenth3 == null) {
				dicDaoLenth3 = new DictionaryRecordDao();
			}
			return dicDaoLenth3;
		case 4:
			if (dicDaoLenth4 == null) {
				dicDaoLenth4 = new DictionaryRecordDao();
			}
			return dicDaoLenth4;
		case 5:
			if (dicDaoLenth5 == null) {
				dicDaoLenth5 = new DictionaryRecordDao();
			}
			return dicDaoLenth5;
		case 6:
			if (dicDaoLenth6 == null) {
				dicDaoLenth6 = new DictionaryRecordDao();
			}
			return dicDaoLenth6;
		case 7:
			if (dicDaoLenth7 == null) {
				dicDaoLenth7 = new DictionaryRecordDao();
			}
			return dicDaoLenth7;
		case 8:
			if (dicDaoLenth8 == null) {
				dicDaoLenth8 = new DictionaryRecordDao();
			}
			return dicDaoLenth8;
		case 9:
			if (dicDaoLenth9 == null) {
				dicDaoLenth9 = new DictionaryRecordDao();
			}
			return dicDaoLenth9;
		case 10:
			if (dicDaoLenth10 == null) {
				dicDaoLenth10 = new DictionaryRecordDao();
			}
			return dicDaoLenth10;
		case 11:
			if (dicDaoLenth11 == null) {
				dicDaoLenth11 = new DictionaryRecordDao();
			}
			return dicDaoLenth11;
		case 12:
			if (dicDaoLenth12 == null) {
				dicDaoLenth12 = new DictionaryRecordDao();
			}
			return dicDaoLenth12;
		case 13:
			if (dicDaoLenth13 == null) {
				dicDaoLenth13 = new DictionaryRecordDao();
			}
			return dicDaoLenth13;
		case 14:
			if (dicDaoLenth14 == null) {
				dicDaoLenth14 = new DictionaryRecordDao();
			}
			return dicDaoLenth14;
		default:
			if (dicDaoLenth14_plus == null) {
				dicDaoLenth14_plus = new DictionaryRecordDao();
			}
			return dicDaoLenth14_plus;
		}
	}
}

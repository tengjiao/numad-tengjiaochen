package edu.neu.madcourse.tengjiaochen.dictionary.db.domain;


public class DictionaryRecord {
	private String word;
	
	public DictionaryRecord() {}

	public DictionaryRecord(String word){
		this.word = word;
	}

	public void setWord(String word) {
		this.word = word;
	}
	public String getWord() {
		return word;
	}
}

package edu.neu.madcourse.tengjiaochen.finalproject;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import edu.neu.madcourse.tengjiaochen.R;
import edu.neu.madcourse.tengjiaochen.finalproject.data.FinalProject_JumpData;
import edu.neu.madcourse.tengjiaochen.finalproject.data.FinalProject_JumpDataImpl;
import edu.neu.madcourse.tengjiaochen.finalproject.model.FinalProject_Direction;
import edu.neu.madcourse.tengjiaochen.finalproject.util.FinalProject_CreateArrows;
import edu.neu.madcourse.tengjiaochen.finalproject.util.FinalProject_ImageFiller;
import edu.neu.madcourse.tengjiaochen.twoplayer.TwoPlayer_GameActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.ColorDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.graphics.Typeface;
public class FinalProject_Activity extends Activity implements
		SensorEventListener {
	private final String TAG = "TrickestPart_Activity";

	public final static int LEFT_DETECTED = 0;
	public final static int RIGHT_DETECTED = 1;
	public final static int MAKE_IT_THROUGH = 2;
	public final static int SHAKE = 3;

	private Toast toast;

	// these 2 variables are for the sensing part
	private SensorManager mSensorManager;
	private Sensor mSensor_acc;
	private Sensor mSensor_gravity;
	private Sensor mSensor_angle;

	// this layout is for arrows
	private LinearLayout arrowLayout;
	// this is to save the new series of arrows
	private List<FinalProject_Direction> newArrows;
	// left -90; right +90;around 180
	private Float direction_angle = null;

	// these are arrows to detect
	private List<ImageView> arrowImageToDetect = new ArrayList<ImageView>();

	// this is to save the jump data
	private FinalProject_JumpData jumpdata;

	// handler to send message
	private static Handler handler;

	// util
	private FinalProject_ImageFiller imageFiller;

	private TextView scoreTV;

	private final int PROGRESS_MAX = 10000;

	private boolean isSetProgressAllowed = true;

	// data for 4 stages
	private long[] stageWholeTimeArray = { 0, 20000, 20000, 35000, 35000,35000, 30000 };
	private int[] arrowNumArray = { 0, 1, 2, 3, 4, 4, 4 };
	private int[] stagePeriodTimeArray = { 0, 5000, 4000, 3000, 2500, 2300,2000 };

	private int currentStage = 1;
	private int arrowNum = 2;
	private int stagePeriodTime = 2;
	private long stageWholeTime = 2;
	boolean isStageThreadAllowed = true;

	private static final int SET_ICE1 = 1;
	private static final int SET_ICE2 = 2;
	private static final int SET_ICE3 = 3;
	private static final int SET_ICE4 = 4;
	
	private static final int ICE_BREAK = 0;
	private static final int JUMP_DETECTED = 1;
	
	private ImageView imageShake;
	private MediaPlayer iceBreak;
	private MediaPlayer jumpDetected;
	protected SharedPreferences sharedPreferences;

	//left help number
	private int leftNumber=3;
	private int rightNumber=3;
	
	// firmly stop the progress
	private boolean isProgressFirmlyStoped;
	
	// if the stage is gonna restart
	private boolean isStageGonnaRestart;
	
	private boolean isBackGroundMusicReleased;
	
	private Typeface typeface_bold;
	private Typeface typeface_normal;
	
	private boolean isGameOver = false;
	
	private boolean isHighestScoreMade = false;
	
	@SuppressLint("InlinedApi")
	@Override
	// TODO Auto-generated method stub
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.finalproject_game_activity);

		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		
		isHighestScoreMade = false;
		isGameOver = false;
		
		typeface_bold = Typeface.createFromAsset(getAssets(), "font/JosefinSans-Bold.ttf");
		typeface_normal = Typeface.createFromAsset(getAssets(), "font/JosefinSans-Regular.ttf");
		
		isStageGonnaRestart = false;
		isBackGroundMusicReleased = false;
		
		currentStage = 1;
		iceBreak = MediaPlayer.create(FinalProject_Activity.this, R.raw.trickestpart_icesound);
		jumpDetected = MediaPlayer.create(FinalProject_Activity.this, R.raw.newgame_right_word);
		
		TextView game_title_jump = (TextView) findViewById(R.id.finalproject_game_title_jump);
		game_title_jump.setTypeface(typeface_bold);
		TextView game_score_text = (TextView) findViewById(R.id.finalproject_game_score_text);
		game_score_text.setTypeface(typeface_bold);
		TextView game_timeleft_text = (TextView) findViewById(R.id.finalproject_game_timeleft_text);
		game_timeleft_text.setTypeface(typeface_bold);
		
		// stage title
		tvStage = (TextView) findViewById(R.id.finalproject_stage);
		tvStage.setTypeface(typeface_bold);
		
		// find the progress bar and initialize it
		progressBar = (ProgressBar) findViewById(R.id.finalproject_timeleft);
		ClipDrawable greenDraw = new ClipDrawable(new ColorDrawable(
				Color.parseColor("#ff44bb44")), Gravity.LEFT,
				ClipDrawable.HORIZONTAL);
		progressBar.setProgressDrawable(greenDraw);
		progressBar.setBackgroundColor(Color.parseColor("#cccccccc"));
		progressBar.setMax(PROGRESS_MAX);
		progressBar.setProgress(PROGRESS_MAX);

		// initiate the sensor
		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		mSensor_acc = mSensorManager
				.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
		mSensor_gravity = mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
		mSensor_angle = mSensorManager
				.getDefaultSensor(Sensor.TYPE_ORIENTATION);

		arrowLayout = (LinearLayout) findViewById(R.id.finalproject_arrow_layout);
		scoreTV = (TextView) findViewById(R.id.finalproject_score);
		scoreTV.setTypeface(typeface_bold);
		
		toast = Toast.makeText(this, "", Toast.LENGTH_SHORT);

		// next job is to get these states back
		sharedPreferences = getApplicationContext().getSharedPreferences(
				"Jump", MODE_PRIVATE);
		highestScore = sharedPreferences.getInt("highest", 000);
		
		handler = new Handler() {
			
			public void handleMessage(android.os.Message msg) {
				if (msg.what == LEFT_DETECTED || msg.what == RIGHT_DETECTED
						|| msg.what == MAKE_IT_THROUGH) {
					FinalProject_Direction direction;

					if (!newArrows.isEmpty()) {
						direction = newArrows.get(0);
					} else {
						return;
					}

					if ((direction == FinalProject_Direction.LEFT && msg.what == LEFT_DETECTED)
							|| (direction == FinalProject_Direction.RIGHT && msg.what == RIGHT_DETECTED)
							|| (msg.what == MAKE_IT_THROUGH)) {

						playMusicEffect(JUMP_DETECTED);
						newArrows.remove(0);
						ImageView image = arrowImageToDetect.remove(0);
						changeDetectedImage(image);
						image.postInvalidate();

						if (arrowImageToDetect.isEmpty()) {
							
							isSetProgressAllowed = false;
							Log.i(TAG, "Handler isSetProgressAllowed: "+isSetProgressAllowed);
							isProgressFirmlyStoped = true;
							new Thread(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									while(isProgressFirmlyStoped){
										Log.i(TAG, "while isSetProgressAllowed: "+isSetProgressAllowed);
										isSetProgressAllowed = false;
									}
									
								}
							}).start();
							
							new Thread(new Runnable() {
								public void run() {
									try {
										Thread.sleep(200);
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									changeNewArrows(arrowNum);
									
									isProgressFirmlyStoped = false;
									isSetProgressAllowed = true;
								}
							}).start();
						}
					}

					if (msg.what == SHAKE) {
						Log.i(TAG, "SHAKE DETECTED");
						int tagIceState = (Integer) imageShake.getTag();
						if (tagIceState >= SET_ICE1 && tagIceState <= SET_ICE3) {
							tagIceState++;
							imageShake.setTag(tagIceState);
							changeIce(tagIceState);
						}

					}

				}
			};
		};

		// for studing detection algorithm
		// jumpdata = new TrickestPart_JumpDataImpl2(handler);

		jumpdata = new FinalProject_JumpDataImpl(handler);
		// jumpdata.setToast(toast);//testing purpose

		imageFiller = new FinalProject_ImageFiller(getApplicationContext());
		// this is the initial stage period time
		stagePeriodTime =5000;
		changeNewArrows(1);

		// // buttons for testing
		
		buttonLEFT = (Button) findViewById(R.id.buttonLEFT);
		buttonLEFT.setTypeface(typeface_bold);
		buttonRIGHT = (Button) findViewById(R.id.buttonRIGHT);
		buttonRIGHT.setTypeface(typeface_bold);
		
		buttonLEFT.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Button hintButton = (Button)v;
				FinalProject_Direction direction = null;
				if(newArrows.size()>=1){
					direction = newArrows.get(0);
				}
				if(direction == null || direction!=FinalProject_Direction.LEFT){
					return;
				}
				if(leftNumber > 0  ){
//					leftNumber--;
					hintButton.setText("LEFT ("+leftNumber+")");
					jumpdata.JumpLeftDetected();
					if(leftNumber == 0){
						hintButton.setTextColor(Color.parseColor("#ffAAAAAA"));
					}
				} 
				else if(leftNumber<=0){
				}
			}
		});

		buttonRIGHT.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Button hintButton = (Button)v;
				FinalProject_Direction direction = null;
				if(!newArrows.isEmpty()){
					direction = newArrows.get(0);
				}
				if(direction == null || direction!=FinalProject_Direction.RIGHT){
					return;
				}
				if(rightNumber > 0  ){
//					rightNumber--;
					hintButton.setText("RIGHT ("+rightNumber+")");
					jumpdata.JumpRightDetected();
					if(rightNumber == 0){
						hintButton.setTextColor(Color.parseColor("#ffAAAAAA"));
					}
				} 
				else if(rightNumber<=0){
				}
			}
		});

		changeStage(currentStage);
		generateRamdonIce();
		refreshLEFT_RIGHTButon();
		
		Button buttonQuit = (Button)findViewById(R.id.finalproject_quitButton);
		buttonQuit.setTypeface(typeface_bold);
		buttonQuit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(isGameOver){
					finish();
				} else {
					onPause();
					dialog();
				}
				
			}
		});
		
		buttonPause = (ImageView)findViewById(R.id.finalproject_game_pause_button);
		boolean isgamePaused = false;
		buttonPause.setTag(new Boolean(isgamePaused));
		imageCoverGameContent = (ImageView)findViewById(R.id.finalproject_cover_game_content);
		imageCoverGameContentBackground = (RelativeLayout)findViewById(R.id.finalproject_cover_game_content_background);
		buttonPause.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Boolean isgamePaused = (Boolean)v.getTag();
				ImageView buttonPause = (ImageView)v;
				if(!isgamePaused){ // if not paused, press to pause
					isgamePaused = true;
					buttonPause.setImageResource(getResources().getIdentifier("finalproject_game_play", "drawable" , FinalProject_Activity.this.getPackageName()));
					buttonPause.setVisibility(View.INVISIBLE);
					imageCoverGameContent.setVisibility(View.VISIBLE);
					imageCoverGameContent.bringToFront();
					imageCoverGameContentBackground.setVisibility(View.VISIBLE);
					imageCoverGameContentBackground.bringToFront();
					buttonPause.setTag(isgamePaused);
					onPause();
				} 
			}
			
		});
		
		imageCoverGameContent.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Boolean isgamePaused = (Boolean)buttonPause.getTag();
				if(isgamePaused) {
					isgamePaused = false;
					buttonPause.setImageResource(getResources().getIdentifier("finalproject_game_pause", "drawable" , FinalProject_Activity.this.getPackageName()));
					imageCoverGameContent.setVisibility(View.INVISIBLE);
					imageCoverGameContentBackground.setVisibility(View.INVISIBLE);
					buttonPause.setVisibility(View.VISIBLE);
					buttonPause.setTag(isgamePaused);
					onResume();
				}	
			}
		});
		
		
	}

	private void refreshLEFT_RIGHTButon() {
		// TODO Auto-generated method stub
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				while(true){
					if(!isStageThreadAllowed){
						return;
					}
					
					try {
						Thread.sleep(19500);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					while(isThreadPaused){
						try {
							Thread.sleep(19500);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					
					changeLEFTRIGHTTEXT();
					
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					while(isThreadPaused){
						try {
							Thread.sleep(19500);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					
					changeLEFTRIGHTTEXT();
				}
			}

		}).start();
	}
	
	private void changeLEFTRIGHTTEXT() {
		// TODO Auto-generated method stub
		runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				buttonLEFT.setText("LEFT (3)");
				buttonLEFT.setTextColor(Color.parseColor("#ff000000"));
				buttonRIGHT.setText("RIGHT (3)");
				buttonRIGHT.setTextColor(Color.parseColor("#ff000000"));
				leftNumber=3;
				rightNumber=3;
			}
		});
	}
	
	// TODO Auto-generated method stub
	private void generateRamdonIce() {
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				while(true){
					if(!isStageThreadAllowed){
						return;
					}
					
					Random random = new Random();
					int randomInt = random.nextInt(1000);
					randomInt = randomInt % 9;
					
					randomInt = randomInt + 12;
					
					try {
						Thread.sleep(randomInt * 1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}).start();
	}

	private void changeIce(final int setIceState) {
		// TODO Auto-generated method stub
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				switch (setIceState) {

				case SET_ICE2:
					imageShake.setImageDrawable(getResources().getDrawable(
							R.drawable.trickestpart_ice2));
					playMusicEffect(ICE_BREAK);
					break;
				
				case SET_ICE3:
					imageShake.setImageDrawable(getResources().getDrawable(
							R.drawable.trickestpart_ice3));
					playMusicEffect(ICE_BREAK);
					break;

				case SET_ICE4:
					imageShake.setVisibility(ImageView.INVISIBLE);
					playMusicEffect(ICE_BREAK);
//					textShake.setVisibility(View.VISIBLE);
					break;
				}
			}
		});	
	}
	
	private void playMusicEffect(int SITUATION){
		
		switch (SITUATION) {
 
		case ICE_BREAK:
			FinalProject_Activity.this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
			iceBreak.setAudioStreamType(AudioManager.STREAM_MUSIC);
			iceBreak.setVolume(4, 4);
			iceBreak.start();
			break;
		case JUMP_DETECTED:
			jumpDetected.stop();
			jumpDetected.release();
			jumpDetected = MediaPlayer.create(FinalProject_Activity.this, R.raw.newgame_right_word);
			FinalProject_Activity.this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
			jumpDetected.setAudioStreamType(AudioManager.STREAM_MUSIC);
			jumpDetected.setVolume(1, 1);
			jumpDetected.start();
			break;
		}
	}
	
	private void changeStage(final int currentStageNow) {
		// TODO Auto-generated method stub

		Log.i(TAG, "current stage: " + currentStageNow);

		arrowNum = arrowNumArray[currentStageNow];
		stagePeriodTime = stagePeriodTimeArray[currentStageNow];
		stageWholeTime = stageWholeTimeArray[currentStageNow];

		if(currentStageNow == 2){
			runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					buttonPause.setVisibility(View.INVISIBLE);
				}
			});
		}
		
		new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub

				playStageMusic(currentStageNow);
				Log.i(TAG, "playing current stage music");
				try {
					Thread.sleep(stageWholeTimeArray[currentStageNow]);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				while (isThreadPaused) {
					try {
						Thread.sleep(stageWholeTimeArray[currentStageNow]);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				if (!isStageThreadAllowed || currentStageNow == 4) {
					return;
				} else {
					changeStage(currentStageNow + 1);
					// stage switch animation
					changeStageTitle(currentStageNow + 1);
				}
			}

		}).start();

	}

	private void changeStageTitle(final int currentStageNow) {
		// TODO Auto-generated method stub
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				tvStage.setText("Stage " + currentStageNow);
				// int textSize = (int)tvStage.getTextSize();
				// tvStage.setTextSize(textSize+currentStageNow);
				switch (currentStageNow) {
				case 2:
					// ClipDrawable greenDraw2 = new ClipDrawable(new
					// ColorDrawable(
					// Color.parseColor("#ff33bb44")), Gravity.LEFT,
					// ClipDrawable.HORIZONTAL);
					// progressBar.setProgressDrawable(greenDraw2);
					tvStage.setTextColor(Color.parseColor("#ff55cc55"));
					break;

				case 3:
					// ClipDrawable greenDraw3 = new ClipDrawable(new
					// ColorDrawable(
					// Color.parseColor("#ff33bb33")), Gravity.LEFT,
					// ClipDrawable.HORIZONTAL);
					// progressBar.setProgressDrawable(greenDraw3);
					tvStage.setTextColor(Color.parseColor("#ff55bb55"));

					break;

				case 4:
					// ClipDrawable greenDraw4 = new ClipDrawable(new
					// ColorDrawable(
					// Color.parseColor("#ff33aa33")), Gravity.LEFT,
					// ClipDrawable.HORIZONTAL);
					// progressBar.setProgressDrawable(greenDraw4);
					tvStage.setTextColor(Color.parseColor("#ff44aa44"));

					break;
				}
			}
		});
	}

	private void playStageMusic(final int currentStageNow) {

		switch (currentStageNow) {
		case 1:
			backgroungMusic = MediaPlayer.create(FinalProject_Activity.this,
					R.raw.trickestpart_stage_1);
			break;

		case 2:
			backgroungMusic.stop();
			backgroungMusic.release();
			Log.i(TAG, "music stop 611");
			backgroungMusic = MediaPlayer.create(FinalProject_Activity.this,
					R.raw.trickestpart_stage_2);
			break;

		case 3:
			backgroungMusic.stop();
			backgroungMusic.release();
			Log.i(TAG, "music stop 618");
			backgroungMusic = MediaPlayer.create(FinalProject_Activity.this,
					R.raw.trickestpart_stage_3);
			break;

		case 4:
			backgroungMusic.stop();
			backgroungMusic.release();
			Log.i(TAG, "music stop 625");
			backgroungMusic = MediaPlayer.create(FinalProject_Activity.this,
					R.raw.trickestpart_stage_4);
			break;
		}

		FinalProject_Activity.this
				.setVolumeControlStream(AudioManager.STREAM_MUSIC);
		backgroungMusic.setAudioStreamType(AudioManager.STREAM_MUSIC);
		backgroungMusic.setVolume(4, 4);
		backgroungMusic.start();
		
		Log.i(TAG, "music playing is:"+backgroungMusic.toString());
		Log.i(TAG, "music playing 648");
		
		backgroungMusic.setLooping(true);

		if (!isStageThreadAllowed) {
			backgroungMusic.stop();
			Log.i(TAG, "music stop 654");
		}
	}

	// TODO Auto-generated method stub
	// 5000 whole progress -- 10000
	//
	private void rollProgressBar(final int wholeProgress,
			final int FromProgress, final int time) {
		progressBar.setProgress(FromProgress);
		new Thread(new Runnable() {

			@Override
			public void run() {
				float unit = (float) 6000 / 200;
				progressBar.setProgress(FromProgress);
				try {
					Thread.sleep((long) unit);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				progressBar.setProgress(FromProgress);
				try {
					Thread.sleep((long) unit);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				progressBar.setProgress(FromProgress);
				isSetProgressAllowed = true;

				// TODO Auto-generated method stub
				int progress = FromProgress;
				unit = (float) time / 200;
				while (progress >= 0) {
					
					if (!isSetProgressAllowed) {
						return;
					}

					if (isThreadPaused) {
						continue;
					}

					try {
						Thread.sleep((long) unit);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					if (!isSetProgressAllowed) {
						return;
					}

					progress = progressBar.getProgress();

					if (progress >= 1 && isSetProgressAllowed) {
						progress = (int) (progress - (float) wholeProgress / 200);
					}

					if (!isSetProgressAllowed) {
						return;
					}
					changeProgressBar(progress);
				}
			}

		}).start();
	}

	private void changeProgressBar(final int progress) {
		// TODO Auto-generated method stub
		runOnUiThread(new Runnable() {

			

			@Override
			public void run() {

				if (progress <= 0 && isSetProgressAllowed) {
					isGameOver = true;
					isSetProgressAllowed = false;
//					Intent intent = new Intent(FinalProject_Activity.this,
//							FinalProject_GameOverActivity.class);
//					startActivity(intent);
//					finish();
					
//					change the code
					
					onPause();
					isStageThreadAllowed = false;
					isSetProgressAllowed = false;
					isThreadPaused = false;
					backgroungMusic.stop();
					backgroungMusic.release();
					isBackGroundMusicReleased = true;
					Log.i(TAG,"music stop 747");
					
					ImageView imageCover = (ImageView)findViewById(R.id.finalproject_cover_game_content_finish);
//					TextView tvHighestScore = (TextView)findViewById(R.id.finalproject_highest_score);
//					TextView tvCurrentScore = (TextView)findViewById(R.id.finalproject_current_score);
					TextView thisround_label = (TextView)findViewById(R.id.gameover_thisround_label);
					thisround_label.setTypeface(typeface_bold);
					TextView current_score = (TextView)findViewById(R.id.finalproject_current_score);
					current_score.setTypeface(typeface_bold);
					current_score.setTextColor(Color.parseColor("#FFC80000"));
					TextView total_jump_label = (TextView)findViewById(R.id.finalproject_total_jump_label);
					total_jump_label.setTypeface(typeface_bold);
					TextView best_label = (TextView)findViewById(R.id.finalproject_best_label);
					best_label.setTypeface(typeface_bold);
					TextView total_jump = (TextView)findViewById(R.id.finalproject_total_jump);
					total_jump.setTypeface(typeface_bold);
					total_jump.setTextColor(Color.parseColor("#FF00CC33"));
					TextView finalproject_best = (TextView)findViewById(R.id.finalproject_best);
					finalproject_best.setTypeface(typeface_bold);
					finalproject_best.setTextColor(Color.parseColor("#FF0033CC"));
					TextView you_made_new_highest_score = (TextView)findViewById(R.id.finalproject_you_made_new_highest_score);
					you_made_new_highest_score.setTypeface(typeface_bold);
//					TextView total_calories = (TextView)findViewById(R.id.finalproject_total_calories);
//					total_calories.setTypeface(typeface_bold);
					Button buttonPlayAgain = (Button)findViewById(R.id.finalproject_play_again);
					buttonPlayAgain.setTypeface(typeface_bold);
					LinearLayout coverLayout = (LinearLayout)findViewById(R.id.finalproject_cover_game_content_finish_layout);
					LinearLayout coverlayout_outer = (LinearLayout)findViewById(R.id.finalproject_cover_game_content_finish_layout_outer);
					LinearLayout coverlayout_total_best = (LinearLayout)findViewById(R.id.finalproject_cover_game_content_finish_layout_outer);
					LinearLayout coverlayout_jump_best = (LinearLayout)findViewById(R.id.finalproject_cover_game_content_finish_layout_outer);
					LinearLayout coverlayout_game_play_again = (LinearLayout)findViewById(R.id.finalproject_cover_game_play_again);
					
					sharedPreferences = getApplicationContext().getSharedPreferences("Jump", MODE_PRIVATE);
					int highestScore = sharedPreferences.getInt("highest", 000);
					int TotalJump = sharedPreferences.getInt("TotalJump", 000);
					String currentScore = sharedPreferences.getString("current", "unavaible");
					TotalJump = TotalJump + Integer.parseInt(currentScore);
					
					editor = sharedPreferences.edit();
					editor.putInt("TotalJump", TotalJump);
					editor.commit();
					
					String highestScoreStr = highestScore+"";
					for(int i=highestScoreStr.length();i<3;i++){
						highestScoreStr = "0"+highestScoreStr;
					}
					
//					tvHighestScore.setText("My Highest Score: "+highestScoreStr);
//					tvCurrentScore.setText("Current Score: "+currentScore);
					finalproject_best.setText(""+highestScore);
					current_score.setText(""+Integer.parseInt(currentScore));
					total_jump.setText(""+TotalJump);
					
//					int JUMP_PER_MINUTE = 70;
//					int weight = 160;
//			 		double RATIO_JUMP_70 = 0.074;
//					double totalCalories =  RATIO_JUMP_70 * weight * Integer.parseInt(currentScore) / JUMP_PER_MINUTE;
//					total_calories.setText(String.format("%.2f", totalCalories) + " cal");
					
					imageCover.setVisibility(View.VISIBLE);

					thisround_label.setVisibility(View.VISIBLE);
					current_score.setVisibility(View.VISIBLE);
					
					best_label.setVisibility(View.VISIBLE);
					total_jump_label.setVisibility(View.VISIBLE);
					
					finalproject_best.setVisibility(View.VISIBLE);
					total_jump.setVisibility(View.VISIBLE);
					
					buttonPlayAgain.setVisibility(View.VISIBLE);
					
					if(isHighestScoreMade){
						you_made_new_highest_score.setVisibility(View.VISIBLE);
					}
//					total_calories.setVisibility(View.VISIBLE);
					
					imageCover.bringToFront();
					coverLayout.bringToFront();
					coverlayout_outer.bringToFront();
					coverlayout_total_best.bringToFront();
					coverlayout_jump_best.bringToFront();
					coverlayout_game_play_again.bringToFront();
					
					thisround_label.bringToFront();
					current_score.bringToFront();
					best_label.bringToFront();
					total_jump_label.bringToFront();
					finalproject_best.bringToFront();
					total_jump.bringToFront();
					if(isHighestScoreMade){
						you_made_new_highest_score.bringToFront();
					}
//					total_calories.bringToFront();
					
					buttonPlayAgain.bringToFront();

					buttonPause.setClickable(false);
					buttonPause.setVisibility(View.INVISIBLE);
					
					buttonPlayAgain.setOnClickListener(new OnClickListener() {
						

						@Override
						public void onClick(View v) {
							isStageGonnaRestart = true;
							finish();
							Intent intent = new Intent(FinalProject_Activity.this, FinalProject_Activity.class);
							startActivity(intent);
						}
					});
					
				}

				// TODO Auto-generated method stub
				if (isSetProgressAllowed) {
					progressBar.setProgress(progress);
				}
//				Log.i(TAG, "progress: " + progress);
//				Log.i(TAG, "isSetProgressAllowed: " + isSetProgressAllowed);
			}
		});
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		// Activity is on the front, register the sensor
		mSensorManager.registerListener(this, mSensor_acc,
				SensorManager.SENSOR_DELAY_FASTEST);
		mSensorManager.registerListener(this, mSensor_gravity,
				SensorManager.SENSOR_DELAY_FASTEST);
		mSensorManager.registerListener(this, mSensor_angle,
				SensorManager.SENSOR_DELAY_FASTEST);
		Boolean isgamePaused = (Boolean)buttonPause.getTag();
		if(!isgamePaused){
			isThreadPaused = false;
		}
		if(backgroungMusic != null && !isBackGroundMusicReleased && !isgamePaused){
			backgroungMusic.start();
			Log.i(TAG, "music play 815");
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		// Activity is on the back, unregister the sensor
		mSensorManager.unregisterListener(this);

//		String currentHighestStr = "" + tvHighestScore.getText();
		int currentHighest = highestScore;

		String currentScoreStr = "" + scoreTV.getText();
		int currentScore = Integer.parseInt(currentScoreStr);
		if (currentScore > currentHighest) {
			isHighestScoreMade = true;
			editor = sharedPreferences.edit();
			editor.putInt("highest", currentScore);
			editor.commit();
		}

		editor = sharedPreferences.edit();
		editor.putString("current", currentScoreStr);
		editor.commit();

		if(!isStageGonnaRestart && !isBackGroundMusicReleased){
			Log.i(TAG, "backgroungMusic: "+backgroungMusic);
			backgroungMusic.pause();
			Log.i(TAG,"music pause 842");
		}
		isThreadPaused = true;
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		isStageThreadAllowed = false;
		isSetProgressAllowed = false;
		isThreadPaused = false;
		if(!isStageGonnaRestart && !isBackGroundMusicReleased){
			backgroungMusic.stop();
			backgroungMusic.release();
			isBackGroundMusicReleased = true;
			Log.i(TAG, "music stop 854");
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0 && !isGameOver) {
			onPause();
			dialog();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	private void dialog() {
		// TODO Auto-generated method stub
		AlertDialog.Builder builder = new Builder(FinalProject_Activity.this);
		builder.setMessage("Are you sure to leave?");
		builder.setTitle("Hint");
		builder.setPositiveButton("YES",
		new android.content.DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				FinalProject_Activity.this.finish();
			}
		});
		builder.setNegativeButton("NO",
		new android.content.DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Boolean isgamePaused = (Boolean)buttonPause.getTag();
				if(!isgamePaused){
					onResume();
				}
				dialog.dismiss();
			}
		});
		builder.create().show();
	}
	
	// this part will be changed for the final project
	float[] rotMat = new float[9];
	float[] vals = new float[3];

//	private TextView tvHighestScore;

	private Editor editor;

	private ProgressBar progressBar;

	private MediaPlayer backgroungMusic;

	private TextView tvStage;

	private boolean isThreadPaused = false;

	private Button buttonLEFT;

	private Button buttonRIGHT;

	private int highestScore;

	private ImageView imageCoverGameContent;

	private ImageView buttonPause;

	private RelativeLayout imageCoverGameContentBackground;

	

	@SuppressLint("NewApi")
	@Override
	public void onSensorChanged(SensorEvent event) {
		Sensor sensor = event.sensor;

		if (sensor.equals(mSensor_acc)) {
			jumpdata.saveAcc(event);
		} else if (sensor.equals(mSensor_gravity)) {
			jumpdata.saveGravity(event);
		} else if (sensor.equals(mSensor_angle)) {
			 jumpdata.saveAngle(event.values[0]);
		}

	}

	private void changeNewArrows(final int arrowNum) {
		// TODO Auto-generated method stub
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				newArrows = FinalProject_CreateArrows.getInstance().getNewArrows(arrowNum);
				imageFiller.fillTheImage(newArrows, arrowLayout,arrowImageToDetect);
				isSetProgressAllowed = false;
				progressBar.setProgress(PROGRESS_MAX);
				rollProgressBar(PROGRESS_MAX, PROGRESS_MAX, stagePeriodTime);
			}
		});
	}

	private void changeDetectedImage(final ImageView imageView) {
		// TODO Auto-generated method stub
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				// Everytime I change the background color
				// the score must add up by one.
				
				String scoreStr = "" + scoreTV.getText();
				int score = Integer.parseInt(scoreStr);
				score++;
				String newScoreStr = "" + score;
				while (newScoreStr.length() < 3) {
					newScoreStr = "0" + newScoreStr;
				}
				scoreTV.setText(newScoreStr);
				imageView.setBackgroundColor(Color.parseColor("#bb88ee88"));
				
				if(!arrowImageToDetect.isEmpty()){
					ImageView imageNext = arrowImageToDetect.get(0);
					imageNext.setBackgroundColor(Color.parseColor("#2f88ee88"));
				}
			}
		});
	}

	// Temporally, we do not have to override this part
	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
	}
}

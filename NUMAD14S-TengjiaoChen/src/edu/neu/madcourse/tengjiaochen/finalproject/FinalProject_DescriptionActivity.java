package edu.neu.madcourse.tengjiaochen.finalproject;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import edu.neu.madcourse.tengjiaochen.R;

public class FinalProject_DescriptionActivity extends Activity {
	
	private Typeface typeface_bold;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.finalproject_activity_description);
		
		typeface_bold = Typeface.createFromAsset(getAssets(), "font/JosefinSans-Bold.ttf");
		
		TextView title = (TextView)findViewById(R.id.finalproject_description_title);
		title.setTypeface(typeface_bold);
		TextView appdes1 = (TextView)findViewById(R.id.finalproject_description_App_Description1);
		appdes1.setTypeface(typeface_bold);
		TextView appcontent1 = (TextView)findViewById(R.id.finalproject_description_App_Description1_conent);
		appcontent1.setTypeface(typeface_bold);
		TextView appdes2 = (TextView)findViewById(R.id.finalproject_description_App_Description2);
		appdes2.setTypeface(typeface_bold);
		TextView appcontent2 = (TextView)findViewById(R.id.finalproject_description_App_Description2_conent);
		appcontent2.setTypeface(typeface_bold);
		
		Button startButton = (Button)findViewById(R.id.finalproject_description_startButton);
		startButton.setTypeface(typeface_bold);
		
		startButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				startActivity(new Intent(FinalProject_DescriptionActivity.this,FinalProject_TutorialActivity.class));
				finish();
			}
		});
		
	}
}

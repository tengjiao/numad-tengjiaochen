package edu.neu.madcourse.tengjiaochen.finalproject;


import edu.neu.madcourse.tengjiaochen.R;
import edu.neu.madcourse.tengjiaochen.finalproject.data.FinalProject_JumpData;
import edu.neu.madcourse.tengjiaochen.finalproject.data.FinalProject_JumpDataImpl;
import edu.neu.madcourse.tengjiaochen.finalproject.data.FinalProject_ShakeData;
import edu.neu.madcourse.tengjiaochen.finalproject.data.FinalProject_ShakeDataImpl;
import edu.neu.madcourse.tengjiaochen.finalproject.view.FinalProject_MyViewGroup;
import edu.neu.madcourse.tengjiaochen.finalproject.view.FinalProject_MyViewGroup.MyScrollListener;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;
import android.graphics.Typeface;
import android.content.DialogInterface;

public class FinalProject_TutorialActivity extends Activity implements SensorEventListener {

	protected static final String TAG = "TrickestPart_TutorialActivity";
	
	
	//  
	private FinalProject_MyViewGroup myViewGroup;
	
	private RadioGroup radioGroup;
	private LinearLayout bodyLayout;
	private Button ready;
	private Button continueButton;
	
	private int viewGroupExtend;
	private ImageView imageLeft;
	private TextView textLeft;
	private ImageView imageRight;
	private TextView textRight;
//	private ImageView imageShake;
	private TextView textShake;
	
	private SensorManager mSensorManager;
	private Sensor mSensor_acc;
	private Sensor mSensor_gravity;
	private Sensor mSensor_angle;
	
	protected SharedPreferences sharedPreferences;
	private Editor editor;
	private boolean isRightFirstPlay = true;
	
	private int currentViewNumber = 0;
	
	private final int LEFT_DETECTED = FinalProject_Activity.LEFT_DETECTED;
	private final int RIGHT_DETECTED = FinalProject_Activity.RIGHT_DETECTED;
	private final int MAKE_IT_THROUGH = FinalProject_Activity.MAKE_IT_THROUGH;
	private final int SHAKE = FinalProject_Activity.SHAKE;

	private Handler handler;

	private FinalProject_JumpData jumpdata;
	private static final int SET_ICE1 = 1;
	private static final int SET_ICE2 = 2;
	private static final int SET_ICE3 = 3;
	private static final int SET_ICE4 = 4;

	private static final int ICE_BREAK = 0;
	private static final int JUMP_DECTECTED = 1;

	private int iceState = SET_ICE1;

	private MediaPlayer iceBreak;
	private MediaPlayer jumpDetected;

	private FinalProject_ShakeData shakeData;
	private VideoView videoLeftTutorial;
	private VideoView videoRightTutorial;

	private Typeface typeface_bold;
	private Typeface typeface_normal;
	private LinearLayout toturial_jump_left_hold_the_phone;
	private LinearLayout finalproject_toturial_jump_left_video_jump;
	private TextView hold_the_phone;
	private ImageView hold_the_phone_image;
	private int timesPlayed;
	
	private boolean isRightTutorialPlaying =false;
	private boolean isLeftTutorialPlaying =false;
	
	private int dialogNumber = 0;
	private boolean isActivityPaused = false;
	private boolean isActivityDestroyed = false;
	
	private AlertDialog.Builder builder;
	private android.content.DialogInterface.OnClickListener onClickListenerForDialog;
	private AlertDialog create;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE); 
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.finalproject_activity_tutorial);
//		setTitle("Tutorial");
		isActivityPaused = false;
		isActivityDestroyed = false;
		dialogNumber = 0;
		
		isRightTutorialPlaying =false;
		isLeftTutorialPlaying =false;
		
		isRightFirstPlay = true;
		myViewGroup = new FinalProject_MyViewGroup(this);
		
        radioGroup =(RadioGroup)findViewById(R.id.finalproject_radioGroup);
        
        typeface_bold = Typeface.createFromAsset(getAssets(), "font/JosefinSans-Bold.ttf");
        typeface_normal = Typeface.createFromAsset(getAssets(), "font/JosefinSans-Regular.ttf");
        
//        radioGroup.setVisibility(View.INVISIBLE);
        
		sharedPreferences = getApplicationContext().getSharedPreferences(
				"Jump", MODE_PRIVATE);
		timesPlayed = sharedPreferences.getInt("timesPlayed", 1);
//		int timesPlayed = 0;
		radioGroup.setVisibility(View.INVISIBLE);
//		if(timesPlayed >= 2){
////			View childAt = radioGroup.getChildAt(3);
//			radioGroup.setVisibility(View.INVISIBLE);
////			RadioButton radioButton = (RadioButton)childAt;
////			radioButton.setChecked(true);
////			myViewGroup.moveToDest(3);
////			currentViewNumber = 3;
//		} else {
//			editor = sharedPreferences.edit();
//			editor.putInt("timesPlayed", timesPlayed+1);
//			editor.commit();
//		}
        
		// initiate the sensor
		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		mSensor_acc = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
		mSensor_gravity = mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
		mSensor_angle = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
		
        // add the radio group
        for (int i = 0; i < 8; i++) {
        	RadioButton radioButton = new RadioButton(this);
        	radioGroup.addView(radioButton);
        	
        	if(i==0){
        		radioButton.setChecked(true);
        	}
        	
        	// store the number
        	radioButton.setTag(i);
        	
        	radioButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					// TODO Auto-generated method stub
					if(isChecked){
						myViewGroup.moveToDest((Integer)buttonView.getTag());
						currentViewNumber = (Integer)buttonView.getTag();
//						showToastInMainThread("������" + (Integer)buttonView.getTag() +"��");
					}
				}
			});
		}
        
        
        
        handler = new Handler(){
			public void handleMessage(android.os.Message msg) {
				// block the handler 
				
				if(msg.what == LEFT_DETECTED || msg.what == RIGHT_DETECTED
						|| msg.what == MAKE_IT_THROUGH){
					
					if( (currentViewNumber == 2 && msg.what == LEFT_DETECTED)||	(msg.what == MAKE_IT_THROUGH) ){
						if(imageLeft != null){
							playMusicEffect(JUMP_DECTECTED);
							imageLeft.setBackgroundColor(Color.parseColor("#bb88ee88"));
							new Thread(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									try {
										Thread.sleep(1000);
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									if(create != null && create.isShowing()){
										create.dismiss();
									}
									moveTo(currentViewNumber+1);
								}


							}).start();
						}
						if(textLeft != null){
							textLeft.setVisibility(View.VISIBLE);
						}
					}
					
					if( (currentViewNumber == 4 && msg.what == RIGHT_DETECTED) || (msg.what == MAKE_IT_THROUGH)){
						if(imageRight != null){
							playMusicEffect(JUMP_DECTECTED);
							imageRight.setBackgroundColor(Color.parseColor("#bb88ee88"));
							new Thread(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									try {
										Thread.sleep(1000);
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									
									moveTo(currentViewNumber+1);
								}


							}).start();
						}
						if(textRight != null){
							textRight.setVisibility(View.VISIBLE);
						}
					}
					
				} 

			};
		};
        
		iceBreak = MediaPlayer.create(FinalProject_TutorialActivity.this, R.raw.trickestpart_icesound);
		jumpDetected = MediaPlayer.create(FinalProject_TutorialActivity.this, R.raw.newgame_right_word);
		
		jumpdata = new FinalProject_JumpDataImpl(handler);
		shakeData = new FinalProject_ShakeDataImpl(handler);
		
		// put the "try nexus" layout
		View try_nexus = getLayoutInflater().inflate(R.layout.finalproject_view_try_nexus,null);
		if(timesPlayed < 2){
			myViewGroup.addView(try_nexus);
		}
		

		
        // put the "hold the phone" layout
        View hold_the_phone_layout = getLayoutInflater().inflate(R.layout.finalproject_view_hold_the_phone,null);
        if(timesPlayed < 2){
        	myViewGroup.addView(hold_the_phone_layout);
        }
        
        // put the "left tutorial" layout
        View viewLeft = getLayoutInflater().inflate(R.layout.finalproject_view_left_tutorial,null);
        if(timesPlayed < 2){
        	myViewGroup.addView(viewLeft);
        }
        
        // put the "left done tutorial" layout
        View viewLeftDone = getLayoutInflater().inflate(R.layout.finalproject_view_left_done,null);
        if(timesPlayed < 2){
        	myViewGroup.addView(viewLeftDone);
        }
        
        // put the "right tutorial" layout
        View viewJumpRight = getLayoutInflater().inflate(R.layout.finalproject_view_right_tutorial,null);
        if(timesPlayed < 2){
        	myViewGroup.addView(viewJumpRight);
        }

        // put the "right done tutorial" layout
        View viewRightDone = getLayoutInflater().inflate(R.layout.finalproject_view_right_done,null);
        if(timesPlayed < 2){
        	myViewGroup.addView(viewRightDone);
        }
        
        // put in the "Are you Ready" layout
        View viewReady = getLayoutInflater().inflate(R.layout.finalproject_view_ready,null);
        myViewGroup.addView(viewReady);
        
        // put in the "acknowledgments" layout
        View viewAcknowledgments = getLayoutInflater().inflate(R.layout.finalproject_view_acknowledgements,null);
        myViewGroup.addView(viewAcknowledgments);
        
        bodyLayout = (LinearLayout) findViewById(R.id.finalproject_bodyLayout);
        bodyLayout.addView(myViewGroup);

        // try nexus part
		TextView try_nexus_text1 = (TextView)findViewById(R.id.finalproject_tutorial_try_nexus_text1);
		if(try_nexus_text1!=null){
			try_nexus_text1.setTypeface(typeface_bold);
		}
		
		Button OKButton = (Button)findViewById(R.id.finalproject_tutorial_try_nexus_OKButton);
		
		if(OKButton!=null){
			OKButton.setTypeface(typeface_bold);
			OKButton.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					moveTo(currentViewNumber+1);
				}
			});
		}
        
        int width = this.getResources().getDisplayMetrics().widthPixels / 3;
        int imageWidth = width;
        int imageHeight = width;

        // left tutorial part
        
        toturial_jump_left_hold_the_phone = (LinearLayout)findViewById(R.id.finalproject_toturial_jump_left_hold_the_phone);
        
        finalproject_toturial_jump_left_video_jump = (LinearLayout)findViewById(R.id.finalproject_toturial_jump_left_video_jump);
//        if(toturial_jump_left_hold_the_phone != null){
//        	new Thread(new Runnable() {
//				
//				@Override
//				public void run() {
//					// TODO Auto-generated method stub
//					try {
//						Thread.sleep(3000);
//					} catch (InterruptedException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
////					showTheVideo();
//					moveTo(currentViewNumber+1);
//					
//				}
//
//			}).start();
//        }
        
        hold_the_phone = (TextView)findViewById(R.id.finalproject_toturial_hold_the_phone);
        if(hold_the_phone != null){
        	hold_the_phone.setTypeface(typeface_bold);
        }
        
        Button hold_the_phone_next = (Button)findViewById(R.id.finalproject_tutorial_hold_the_phone_next);
        if(hold_the_phone_next != null){
        	hold_the_phone_next.setTypeface(typeface_bold);
        	hold_the_phone_next.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					moveTo(currentViewNumber+1);
				}
			});
        }
        
        
        // left done part
        TextView great_left_jump = (TextView)findViewById(R.id.finalproject_tutorial_leftdone_great_left_jump);
        TextView try_next = (TextView)findViewById(R.id.finalproject_tutorial_leftdone_try_next);
        
        if(great_left_jump != null && try_next!= null){
        	great_left_jump.setTypeface(typeface_bold);
        	try_next.setTypeface(typeface_bold);
        }
        
        Button leftdone_GoButton = (Button)findViewById(R.id.finalproject_tutorial_leftdone_GoButton);
        if(leftdone_GoButton != null){
        	leftdone_GoButton.setTypeface(typeface_bold);
        	leftdone_GoButton.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					moveTo(currentViewNumber+1);
					
				}
			});
        }
        

        
        // right done part
        TextView great_right_jump = (TextView)findViewById(R.id.finalproject_tutorial_rightdone_great_left_jump);
        TextView rightdone_try_next = (TextView)findViewById(R.id.finalproject_tutorial_rightdone_try_next);
        
        if(great_left_jump != null && rightdone_try_next!= null){
        	great_right_jump.setTypeface(typeface_bold);
        	rightdone_try_next.setTypeface(typeface_bold);
        }
        
        Button rightdone_GoButton = (Button)findViewById(R.id.finalproject_tutorial_rightdone_GoButton);
        if(leftdone_GoButton != null){
        	rightdone_GoButton.setTypeface(typeface_bold);
        	rightdone_GoButton.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					editor = sharedPreferences.edit();
					editor.putInt("timesPlayed", timesPlayed+1);
					editor.commit();
					moveTo(currentViewNumber+1);
				}
			});
        }
        
        // left part
        imageLeft = (ImageView)findViewById(R.id.finalproject_image_left);
        textLeft = (TextView)findViewById(R.id.finalproject_toturial_jump_left);
        
        if(imageLeft != null){
        	textLeft.setTypeface(typeface_bold);
        	int heightPixels = this.getResources().getDisplayMetrics().heightPixels;
        	if(heightPixels <=480){
        		TextView title = (TextView)findViewById(R.id.finalproject_tutorial_step1);
        		title.setText("Jump Left");
        		title.setTypeface(typeface_bold);
        		textLeft.bringToFront();
        	}
			imageLeft.setLayoutParams(new LayoutParams(imageWidth, imageHeight));
			imageLeft.setVisibility(View.VISIBLE);
        }
		// right part
        imageRight = (ImageView)findViewById(R.id.finalproject_image_right);
        textRight = (TextView)findViewById(R.id.finalproject_toturial_jump_right);
        if(imageRight != null){
        	textRight.setTypeface(typeface_bold);
	        imageRight.setLayoutParams(new LayoutParams(imageWidth, imageHeight));
	        imageRight.setVisibility(View.VISIBLE);
        }

        TextView are_you_ready= (TextView)findViewById(R.id.finalproject_tutorial_are_you_ready);
        
        if(are_you_ready != null){
        	are_you_ready.setTypeface(typeface_bold);
        }
        
        TextView acknowledgements = (TextView)findViewById(R.id.finalproject_tutorial_are_you_ready_acknowledgements);
        acknowledgements.setTypeface(typeface_bold);
        acknowledgements.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				moveTo(currentViewNumber+1);
			}
		});

        TextView backButton = (TextView)findViewById(R.id.finalproject_tutorial_backButton);
        backButton.setTypeface(typeface_bold);
        backButton.setOnClickListener(new OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		// TODO Auto-generated method stub
        		moveTo(currentViewNumber-1);
        	}
        });
        
        TextView acknowledgments_Acknowledgments = (TextView)findViewById(R.id.finalproject_tutorial_acknowledgments_Acknowledgments);
        acknowledgments_Acknowledgments.setTypeface(typeface_bold);
        TextView acknowledgments_1 = (TextView)findViewById(R.id.finalproject_tutorial_acknowledgments_1);
        acknowledgments_1.setTypeface(typeface_bold);
        TextView acknowledgments_1_conent = (TextView)findViewById(R.id.finalproject_tutorial_acknowledgments_1_conent);
        acknowledgments_1_conent.setTypeface(typeface_bold);
        TextView acknowledgments_2 = (TextView)findViewById(R.id.finalproject_tutorial_acknowledgments_2);
        acknowledgments_2.setTypeface(typeface_bold);
        TextView acknowledgments_2_conent = (TextView)findViewById(R.id.finalproject_tutorial_acknowledgments_2_conent);
        acknowledgments_2_conent.setTypeface(typeface_bold);
        TextView acknowledgments_3 = (TextView)findViewById(R.id.finalproject_tutorial_acknowledgments_3);
        acknowledgments_3.setTypeface(typeface_bold);
        TextView acknowledgments_3_conent = (TextView)findViewById(R.id.finalproject_tutorial_acknowledgments_3_conent);
        acknowledgments_3_conent.setTypeface(typeface_bold);
        TextView acknowledgments_4 = (TextView)findViewById(R.id.finalproject_tutorial_acknowledgments_4);
        acknowledgments_4.setTypeface(typeface_bold);
        TextView acknowledgments_4_conent = (TextView)findViewById(R.id.finalproject_tutorial_acknowledgments_4_conent);
        acknowledgments_4_conent.setTypeface(typeface_bold);
        TextView acknowledgments_5 = (TextView)findViewById(R.id.finalproject_tutorial_acknowledgments_5);
        acknowledgments_5.setTypeface(typeface_bold);
        TextView acknowledgments_5_conent = (TextView)findViewById(R.id.finalproject_tutorial_acknowledgments_5_conent);
        acknowledgments_5_conent.setTypeface(typeface_bold);
        TextView acknowledgments_6 = (TextView)findViewById(R.id.finalproject_tutorial_acknowledgments_6);
        acknowledgments_6.setTypeface(typeface_bold);
        TextView acknowledgments_6_conent = (TextView)findViewById(R.id.finalproject_tutorial_acknowledgments_6_conent);
        acknowledgments_6_conent.setTypeface(typeface_bold);
        
        ready = (Button) findViewById(R.id.finalproject_readyButton);
        ready.setTypeface(typeface_bold);
        ready.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				playMusicEffect(JUMP_DECTECTED);
				Intent intent = new Intent(FinalProject_TutorialActivity.this, FinalProject_Activity.class);
				startActivity(intent);
				finish();
			}
		});
 
        myViewGroup.setMyScrollListener(new MyScrollListener() {

			@Override
			public void moveToDest(int destId) {
				// TODO Auto-generated method stub
				( (RadioButton)radioGroup.getChildAt(destId) ).setChecked(true);
				if(destId == 3 ){
//					myViewGroup.setTouchAllowed(false);
					
				    int width = getResources().getDisplayMetrics().widthPixels / 3;
				    int imageWidth = width;
				    int imageHeight = width;
					
			        // right tutorial part
			        videoRightTutorial = (VideoView) findViewById(R.id.finalproject_tutorial_right);
			        Uri uriRightVideo = Uri.parse("android.resource://" + getPackageName() + "/"  
			                + R.raw.finalproject_jump_right);
			        
			        if(videoRightTutorial != null){
			        	videoRightTutorial.setVideoURI(uriRightVideo);
			        	videoRightTutorial.setMinimumHeight(imageHeight*2);
			        	videoRightTutorial.setMinimumWidth(imageWidth*2);
			        	videoRightTutorial.setLayoutParams(new LayoutParams(imageWidth*9/4, imageHeight*9/4));
			        	videoRightTutorial.setOnPreparedListener(new OnPreparedListener() {  
			        		@Override  
			        		public void onPrepared(MediaPlayer mp) {  
			        			mp.setLooping(true);  
			        		}  
			        	}); 
			        }
					
					if(videoRightTutorial != null){
						videoLeftTutorial.pause();
						videoRightTutorial.start();
						if(timesPlayed < 2){
							isRightTutorialPlaying = true;
						}
					}
				}
				
				if(destId == 1 && timesPlayed < 2){
//					myViewGroup.setTouchAllowed(false);
			        
				    int width = getResources().getDisplayMetrics().widthPixels / 3;
				    int imageWidth = width;
				    int imageHeight = width;
					
					// left tutorial part
			        videoLeftTutorial = (VideoView) findViewById(R.id.finalproject_tutorial_left);
			        Uri uriLeftVideo = Uri.parse("android.resource://" + getPackageName() + "/"  
			                + R.raw.finalproject_jump_left);
			        
			        if(videoLeftTutorial != null){
			        	videoLeftTutorial.setVideoURI(uriLeftVideo);
			        	videoLeftTutorial.setMinimumHeight(imageHeight*2);
			        	videoLeftTutorial.setMinimumWidth(imageWidth*2);
			        	videoLeftTutorial.setLayoutParams(new LayoutParams(imageWidth*9/4, imageHeight*9/4));
			        	videoLeftTutorial.setOnPreparedListener(new OnPreparedListener() {  
			        		@Override  
			        		public void onPrepared(MediaPlayer mp) {  
			        			mp.setLooping(true);  
			        		}  
			        	}); 
			        	
			        }
					
					if(videoLeftTutorial != null){
						if(videoRightTutorial != null){
							videoRightTutorial.pause();
						}
						videoLeftTutorial.start();
						if(timesPlayed < 2){
							isLeftTutorialPlaying = true;
						}
						
						if(timesPlayed < 2){
							new Thread(new Runnable() {
								final int TIME_UNIT = 500;
								int times = 0;
								@Override
								public void run() {
									while(times < 18){
										try {
											Thread.sleep(TIME_UNIT);
										} catch (InterruptedException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
										
										if(!isActivityPaused ){
											times++;
										} else {
											times = 0;
										}
										
										if(isActivityDestroyed){
											return;
										}
										
										if(currentViewNumber != 2){
											return;
										}
									}
									
									if(isActivityDestroyed){
										return;
									}
									
									if(currentViewNumber != 2){
										return;
									}else{
										dialog();
									}
									
								}
							}).start();
						}
					}
				}
			}
		});
        

	}
	

	
	private void dialog() {
		// TODO Auto-generated method stub
		
		if(dialogNumber == 0){
			dialogNumber = 1;
		} else {
			return;
		}
		runOnUiThread(new Runnable() {
			
			

			

			

			@Override
			public void run() {
				builder = new Builder(FinalProject_TutorialActivity.this);
				
				builder.setMessage("We did not sense your jumping.\n\n"+
						"This often happens when you did not jump or "+
						"the sensor on your phone is not sensitive enough.\n\n"
						+"Please try Nexus or phones with better sensors.");
				builder.setTitle("Hint");
				
				onClickListenerForDialog = new android.content.DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				};
				
				builder.setPositiveButton("OK",	onClickListenerForDialog);
				create = builder.create();
				create.show();
				
			}
		});
	}
	
	private void moveTo(final int destId) {
		// TODO Auto-generated method stub
		runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				myViewGroup.moveToDest(destId);
			}
		});
	}
	
	private void showTheVideo() {
		// TODO Auto-generated method stub
		runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
		       int width = getResources().getDisplayMetrics().widthPixels / 3;
		        int imageWidth = width;
		        int imageHeight = width;
				toturial_jump_left_hold_the_phone.setVisibility(View.INVISIBLE);
				finalproject_toturial_jump_left_video_jump.setVisibility(View.VISIBLE);

				videoLeftTutorial.start();
				
			}
		});
	}
	
	/**
	 * ����������������
	 * @param text
	 */
	public void showToastInMainThread(final String text) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(getApplicationContext(), text, 1).show();
			}
		});
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		// Activity is on the front, register the sensor	
		mSensorManager.registerListener(this, mSensor_acc, SensorManager.SENSOR_DELAY_FASTEST);
		mSensorManager.registerListener(this, mSensor_gravity, SensorManager.SENSOR_DELAY_FASTEST);
		mSensorManager.registerListener(this, mSensor_angle, SensorManager.SENSOR_DELAY_FASTEST);
		if(videoRightTutorial!=null && isRightTutorialPlaying){
			videoRightTutorial.start();
		}
		
		if(videoLeftTutorial!=null && isLeftTutorialPlaying){
			videoLeftTutorial.start();
		}
		isActivityPaused = false;
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		// Activity is on the back, unregister the sensor
		mSensorManager.unregisterListener(this);
		if(videoRightTutorial!=null && isRightTutorialPlaying){
			videoRightTutorial.pause();
		}
		
		if(videoLeftTutorial!=null && isLeftTutorialPlaying){
			videoLeftTutorial.pause();
		}
		
		isActivityPaused = true;
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		isActivityDestroyed = true;
	}
	
	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		// TODO Auto-generated method stub
		Sensor sensor = event.sensor;

		if (sensor.equals(mSensor_acc)) {
			shakeData.saveAcc(event);
			jumpdata.saveAcc(event);
		} else if (sensor.equals(mSensor_gravity)) {
			jumpdata.saveGravity(event);
		} else if (sensor.equals(mSensor_angle)) {
			jumpdata.saveAngle(event.values[0]);
		}
	}
	
	private void playMusicEffect(int SITUATION){
		
		switch (SITUATION) {
 
		case ICE_BREAK:
			FinalProject_TutorialActivity.this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
			iceBreak.setAudioStreamType(AudioManager.STREAM_MUSIC);
			iceBreak.setVolume(4, 4);
			iceBreak.start();
			break;
		case JUMP_DECTECTED:
			FinalProject_TutorialActivity.this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
			jumpDetected.setAudioStreamType(AudioManager.STREAM_MUSIC);
			jumpDetected.setVolume(1, 1);
			jumpDetected.start();
			break;
		}
	}
}

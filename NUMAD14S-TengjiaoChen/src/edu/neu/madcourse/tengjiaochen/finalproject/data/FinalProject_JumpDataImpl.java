package edu.neu.madcourse.tengjiaochen.finalproject.data;
import android.hardware.SensorEvent;
import android.os.Handler;
import android.util.Log;

public class FinalProject_JumpDataImpl extends FinalProject_JumpData {
	private static final String TAG = "TrickestPart_JumpDataImpl";
	private static final float ACC_SENSITIVITY = 0.5f;
	
	public static final int UP = 1;
	public static final int DOWN = 0;
	public static final int UNKNOWN = -1;
	public static final int LEFT = 2;
	public static final int RIGHT = 3;
	
	public FinalProject_JumpDataImpl(Handler handler) {
		super(handler);
	}
	
	private float prevAngle = -1;
	private float sum = 0;
	
	
	@Override
	public void saveAngle(float angle) {
		
		if(angle == 0) return;
		
		if(prevAngle == -1){
			sum = 0;
		}else{
			float tmp = prevAngle - angle;
			if(tmp > 300){
				sum += prevAngle - angle - 360;
			}else if(tmp < -300){
				sum += prevAngle - angle + 360;
			}else{
				sum += tmp;
			}
		}
		prevAngle = angle;
		if(sum > 48){
			direction = LEFT;
		}
		if(sum < -48){
			direction = RIGHT;
		}
		Log.i(TAG+"angle", ":::"+sum+":"+angle);
	}
	private int record_count = 0;
	private float accelerometer = 0, x = 0, y = 0, z = 0;
	@Override
	public void saveAcc(SensorEvent event) {
		x = event.values[0];
		y = event.values[1];
		z = event.values[2];
		
		x = (float) (x*(Math.sin(getXAngle())));
		
		//this recording for jump
		accelerometer = (float) (z*(Math.sin(getZAngle())) + y*(Math.sin(getYAngle())));
		//accelerometer = getTestJump();
		if(Math.abs(accelerometer) < ACC_SENSITIVITY){
			return;
		}
		
		//analysis jump
		record_count++;
		analysisAcc(accelerometer);
	}
	private int counter = -1;
	private float getTestJump(){
		counter++;
		if(counter >= TestJumpData.data.length){
			stopDirectionRecord();
			return 0;
		}else{
			return (float)TestJumpData.data[counter];
		}
		
	}
	private int counter1 = -1;
	private float getTestAngle(){
		counter1++;
		if(counter1 >= TestAngleData.data.length){
			stopDirectionRecord();
			return 0;
		}else{
			return (float)TestAngleData.data[counter1];
		}
		
	}
	private int UP_LEVEL_SUM = 180;//stage 1 threshold
	private int DOWN_LEVEL_SUM = -280;//stage 2 threshold default
	private int DOWN_INIT_LEVEL_SUM = -30;//stage 0 threshold
	private static final float DEFAULT_VALUE = -9999;
	private int patternStage = 0;//current stage
	private float prevSum = DEFAULT_VALUE;//previous sum of the acc
	private float prevPrevSum = DEFAULT_VALUE;
	private float lastSum = 0; //last sum of the acc (current one)
	
	//buffer direction change
	private float buffer1 = 0;
	private float buffer2 = 0;
	private int bufferCount1 = 0;
	private int bufferCount2 = 0;
	private final static int COUNT_LIMIT = 10;
	
	
	private void analysisAcc(float lastRecordAcc){
		lastSum += lastRecordAcc;
		if(prevSum == DEFAULT_VALUE){
			Log.i(TAG+"angle", "0:"+lastRecordAcc+":"+patternStage);
		}else{
			Log.i(TAG+"angle", ""+prevSum+":"+lastRecordAcc+":"+patternStage);
		}
		switch (patternStage) {
		case 0:
			//see if it is the beginning of the pattern recognition
			if(prevSum == DEFAULT_VALUE){
				if(lastSum < -0){//user's phone goes down
					if(prevPrevSum != DEFAULT_VALUE){
						prevSum = prevPrevSum + lastRecordAcc;
						lastSum = prevSum;
					}
					else{
						prevSum = lastSum;
						prevPrevSum = prevSum;
					}
					beginDirectionRecord();
				}else{
					resetPatterDetection();
					if(lastRecordAcc > 1.5){
						prevPrevSum = DEFAULT_VALUE;
					}
				}
			}else{
				if(lastSum > prevSum){
					if(bufferCount1 < COUNT_LIMIT){
						lastSum -= lastRecordAcc;
						buffer1 += lastRecordAcc;
						bufferCount1++;
						return;
					}else{
						buffer1 += lastRecordAcc;
						if(prevSum <= DOWN_INIT_LEVEL_SUM){
							goStage(1);
							prevPrevSum = DEFAULT_VALUE;
							lastSum = buffer1;
							prevSum = buffer1;
						}else{
							resetPatterDetection();
							if(lastRecordAcc > 2){
								prevPrevSum = DEFAULT_VALUE;
							}
						}
					}
				}else{
					prevSum = lastSum;
					prevPrevSum = prevSum;
				}
				buffer1 = 0;
				bufferCount1 = 0;
			}
			break;
		case 1:
			//user jump to the highest point
			if(lastSum < prevSum){
				if(bufferCount2 < COUNT_LIMIT){
					lastSum = lastSum - lastRecordAcc;
					buffer2 = buffer2 + lastRecordAcc;
					bufferCount2++;
					return;
				}else{
					if(prevSum >= UP_LEVEL_SUM){
						goStage(2);
					}else{
						resetPatterDetection();
					}
					buffer2 += lastRecordAcc;
					lastSum = buffer2;
					prevSum = buffer2;
					buffer2 = 0;
					bufferCount2 = 0;
				}
			}else{
				prevSum = lastSum;
				buffer2 = 0;
				bufferCount2 = 0;
			}
			break;
		case 2:
			if(prevSum == DEFAULT_VALUE){
				prevSum = lastSum;
			}
			//user lands
			if(lastSum > prevSum){
				if(prevSum <= DOWN_LEVEL_SUM){
					resetPatterDetection();
					//get the direction
					int direction = getTurnDirection();
					if(direction == LEFT){
						JumpLeftDetected();
						Log.i(TAG+"angle", ""+600);
						Log.i(TAG+"angle", ""+600);
						Log.i(TAG+"angle", ""+600);
						Log.i(TAG+"angle", ":::::"+record_count + " LEFT");
					}else if(direction == RIGHT){
						JumpRightDetected();
						Log.i(TAG+"angle", ""+-600);
						Log.i(TAG+"angle", ""+-600);
						Log.i(TAG+"angle", ""+-600);
						Log.i(TAG+"angle", ":::::"+record_count + " RIGHT");
					}else{
						Log.i(TAG+"angle", ""+700);
						Log.i(TAG+"angle", ""+700);
						Log.i(TAG+"angle", ""+700);
						Log.i(TAG+"angle", ":::::"+record_count + " MISS");
					}
					goStage(1);//user might continue jumping
					beginDirectionRecord();
					return;
				}else{
					if(prevSum <= DOWN_INIT_LEVEL_SUM){
						goStage(1);
						return;
					}
					resetPatterDetection();
					return;
				}
			}else{
				prevSum = lastSum;
			}
			break;
		}
	}
	private void goStage(int stage){
		patternStage = stage;
		prevSum = DEFAULT_VALUE;
		lastSum = 0;
	}
	private void resetPatterDetection(){
		patternStage = 0;
		prevSum = DEFAULT_VALUE;
		lastSum = 0;
		stopDirectionRecord();
	}
	//acce x
	private int POTENTIAL_LEFT = 10;//maybe jump to left
	private int POTENTIAL_RIGHT = 11;//maybe jump to right
	private int direction = UNKNOWN;//dont know the direction
	private boolean canRecordingDirection = false;
	private boolean canRecordDirection(){
		return canRecordingDirection;
	}
	private void beginDirectionRecord(){
		direction = UNKNOWN;
		resetDirectionRecord();
		canRecordingDirection = true;
		Log.i(TAG+"angle",":::"+200);
	}
	private void stopDirectionRecord(){
		canRecordingDirection = false;
	}
	private void resetDirectionRecord(){
		prevAngle = -1;
		prevSum = 0;
		
	}
	private int getTurnDirection(){
		Log.i(TAG+"angle",":::"+-200);
		if(direction == POTENTIAL_LEFT){
			direction = LEFT;
		}
		if(direction == POTENTIAL_RIGHT){
			direction = RIGHT;
		}
		return direction;
	}
	
	private float[] gravity = {0,0,0};
	private float GRAVITY = 9.81f;
	@Override
	public void saveGravity(SensorEvent event){
		final float alpha = 0.8f;
		// Isolate the force of gravity with the low-pass filter.
		gravity[0] = alpha * gravity[0] + (1 - alpha) * event.values[0];
		gravity[1] = alpha * gravity[1] + (1 - alpha) * event.values[1];
		gravity[2] = alpha * gravity[2] + (1 - alpha) * event.values[2];
	}
	private float getZAngle(){
		return (float) Math.asin(gravity[2] / GRAVITY);
	}
	private float getYAngle(){
		return (float) Math.asin(gravity[1] / GRAVITY);
	}
	private float getXAngle(){
		return (float) Math.asin(gravity[0] / GRAVITY);
	} 
	
}

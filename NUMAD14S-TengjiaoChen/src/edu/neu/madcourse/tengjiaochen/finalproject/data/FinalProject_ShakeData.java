package edu.neu.madcourse.tengjiaochen.finalproject.data;

import edu.neu.madcourse.tengjiaochen.finalproject.FinalProject_Activity;
import android.hardware.SensorEvent;
import android.os.Handler;
import android.os.Message;

public abstract class FinalProject_ShakeData {
	protected Handler handler;

	public FinalProject_ShakeData() {
		super();
	}

	public FinalProject_ShakeData(Handler handler) {
		super();
		this.handler = handler;
	}

	public abstract void saveAcc(SensorEvent event);
	
	public void ShakeDetected(){
		Message msg = new Message();
		msg.what = FinalProject_Activity.SHAKE;
		handler.sendMessage(msg);
	}
	
}

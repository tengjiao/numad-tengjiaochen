package edu.neu.madcourse.tengjiaochen.finalproject.data;
import android.hardware.SensorEvent;
import android.os.Handler;
import android.util.Log;

public class FinalProject_ShakeDataImpl extends FinalProject_ShakeData{

	private static final String TAG = "TrickestPart_ShakeDataImpl";
	private int times = 0;
	
	public FinalProject_ShakeDataImpl() {
		super();
	}

	public FinalProject_ShakeDataImpl(Handler handler) {
		super(handler);
	}



	@Override
	public void saveAcc(SensorEvent event) {
		// TODO Auto-generated method stub
		float x = event.values[0];
		Log.i(TAG,"accX: "+x);
		if(Math.abs(x) > 9){
			times++;
			if(times >= 15){
				Log.i(TAG,"ShakeDetected " );
				ShakeDetected();
				times=0;
			}
		}
	}

}

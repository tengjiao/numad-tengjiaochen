package edu.neu.madcourse.tengjiaochen.finalproject.util;



import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import edu.neu.madcourse.tengjiaochen.finalproject.model.FinalProject_Direction;


public class FinalProject_CreateArrows {

	private List<FinalProject_Direction> directionList;
	
	private static FinalProject_CreateArrows createArrows ;
	
	private FinalProject_CreateArrows(){};
	
	// 6
	private int[][] arrow3 = {{0,0,1},{0,1,0},{0,1,1},{1,0,0},{1,0,1},{1,1,0}};
	// 14
	private int[][] arrow4 = {{0,0,0,1},{0,0,1,0},{0,0,1,1},{0,1,0,0},
								{0,1,0,1},{0,1,1,0},{0,1,1,1},{1,0,0,0},
								{1,0,0,1},{1,0,1,0},{1,0,1,1},{1,1,0,0},
								{1,1,0,1},{1,1,1,0}};
	
	public static FinalProject_CreateArrows getInstance() {
		// TODO Auto-generated method stub
		if(createArrows == null){
			createArrows = new FinalProject_CreateArrows();
		}
		
		return createArrows;
	}

	public List<FinalProject_Direction> getNewArrows(int number) {
		
		if(directionList == null){
			directionList = new ArrayList<FinalProject_Direction>();
		} else{
			directionList.clear();
		}
		
		Random random = new Random();
		
		int left = 0;
		int right = 0;
		
		if(number == 3){
			int nextInt = random.nextInt(6);
			for(int i=0;i<3;i++){
				if(arrow3[nextInt][i] == 0){
					directionList.add(FinalProject_Direction.LEFT);
				} else {
					directionList.add(FinalProject_Direction.RIGHT);
				}
			}
			
			return directionList;
		}
		
		if(number == 4){
			int nextInt = random.nextInt(14);
			for(int i=0;i<4;i++){
				if(arrow4[nextInt][i] == 0){
					directionList.add(FinalProject_Direction.LEFT);
				} else {
					directionList.add(FinalProject_Direction.RIGHT);
				}
			}
			
			return directionList;
		}
		
		for(int i = 0 ; i < number ; i++){
			int nextInt = random.nextInt(2) + 1 ;
			
			switch (nextInt) {
			case 1:
				left++;
				if( left > (1+number)/2 ){
					i--;
					continue;
				}
				
				directionList.add(FinalProject_Direction.LEFT);
				break;

			case 2:
				right++;
				if( right > (1+number)/2 ){
					i--;
					continue;
				}
				directionList.add(FinalProject_Direction.RIGHT);
				break;
			
			case 3:
				directionList.add(FinalProject_Direction.AROUND);
				break;	
				
			}
		}
		
		return directionList;
	}

}

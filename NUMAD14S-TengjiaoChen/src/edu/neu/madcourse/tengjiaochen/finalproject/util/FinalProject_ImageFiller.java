package edu.neu.madcourse.tengjiaochen.finalproject.util;


import java.util.ArrayList;
import java.util.List;

import edu.neu.madcourse.tengjiaochen.R;
import edu.neu.madcourse.tengjiaochen.finalproject.model.FinalProject_Direction;
import edu.neu.madcourse.tengjiaochen.trickestpart.model.TrickestPart_Direction;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

public class FinalProject_ImageFiller {
	private Context context;
	private int currentStageArrowNum;
	
	public int getCurrentStageArrowNum() {
		return currentStageArrowNum;
	}

	public FinalProject_ImageFiller(Context context) {
		super();
		this.context = context;
	}

	public void fillTheImage(List<FinalProject_Direction> newArrows, LinearLayout arrowLayout, List<ImageView> arrowImageToDetect) {
		// TODO Auto-generated method stub
		currentStageArrowNum = newArrows.size();
		arrowLayout.removeAllViews();
		arrowLayout.invalidate();
		
		if(arrowImageToDetect == null){
			arrowImageToDetect = new ArrayList<ImageView>();
		}else {
			arrowImageToDetect.clear();
		}
		
		for(FinalProject_Direction direction : newArrows){
			ImageView imageView = new ImageView(context);
			int width = context.getResources().getDisplayMetrics().widthPixels / (newArrows.size()+1);
			int imageWidth = width;
			int imageHeight = width;
			imageView.setLayoutParams(new LayoutParams(imageWidth, imageHeight));
			
			int resId ;
			
			if(direction == FinalProject_Direction.LEFT){
				
				resId = context.getResources().getIdentifier(
						"trickestpart_jump_left", "drawable",
						context.getPackageName());;
				
			} else { // if(direction == FinalProject_Direction.RIGHT){
				
				resId = context.getResources().getIdentifier(
						"trickestpart_jump_right", "drawable",
						context.getPackageName());;
				
			}  
			
		    if(arrowImageToDetect.isEmpty()){
		    	imageView.setBackgroundColor(Color.parseColor("#2f88ee88"));
		    }
			
			imageView.setTag(resId);
			imageView.setImageResource(resId);
			arrowImageToDetect.add(imageView);
			arrowLayout.addView(imageView);
			
		}
		
	}
}

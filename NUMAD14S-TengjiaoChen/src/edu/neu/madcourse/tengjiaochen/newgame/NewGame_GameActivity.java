package edu.neu.madcourse.tengjiaochen.newgame;


import java.util.HashSet;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import edu.neu.madcourse.tengjiaochen.R;
import edu.neu.madcourse.tengjiaochen.newgame.service.WordWithBlanks;
import edu.neu.madcourse.tengjiaochen.newgame.util.GenerateRamdomLetters;
import edu.neu.madcourse.tengjiaochen.newgame.view.NewGame_FillTheWordBar;
import edu.neu.madcourse.tengjiaochen.newgame.view.NewGame_LetterGridAdapter;

public class NewGame_GameActivity extends Activity {
	
	// state for handler
	public static final int TIME_GOING_DOWN = 0;
	public static final int WORD_IS_RIGHT = 1;
	public static final int WORD_IS_WRONG = 2;
	public static final int WORD_TO_FILL = 3;
	public static final int WORD_TITLE_BACK = 4;
	
	// state for music effect
	private static final int RIGHT_WORD = 100;
	
	protected MediaPlayer backgroungMusic ;
	protected MediaPlayer rightWord ;
	protected ImageView backmusicButton;
	protected ImageView coverGameContent;
	protected TextView timeLeft;
	protected TextView headerText;
	protected TextView newgameScore;
	protected GridView gv_home;
	protected LinearLayout wordBar;
	protected LinearLayout gameWholeLayout;
	
	protected Button hintButton;
	protected Button nextButton;
	protected Button quitButton;
	protected OnClickListener nextClickListener;
	
	protected SharedPreferences sharedPreferences;
	protected Editor editor;
	
	protected int timeLeftNow = 120;
	protected int letterNumber = 35;
	protected int hintNumber = 3;
	//every time wordsSeen is multiple of 5
	// the background will change
	protected int wordsNumber = 0;
	
	protected int hintNow = -1;
	
	protected boolean isPlaying = true;
	protected boolean gamePause = false;
	protected boolean appOnPause = false;
	

	// this is the handler to handle different message
	public Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			if(msg.what == TIME_GOING_DOWN){
				if(!gamePause && !appOnPause){
					timeLeftNow = timeLeftNow - 1;
					changeTheTimeLeft();
				}
			} else if(msg.what == WORD_IS_RIGHT || msg.what == WORD_IS_WRONG || msg.what == WORD_TO_FILL|| msg.what == WORD_TITLE_BACK){
				Log.i("NewGame_GameActivity", "Handler: I get message" + msg.what);
				changeHeaderText(msg.what);
			}
		};
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.newgame_activity_game);
		
		// next job is to get these states back
		sharedPreferences = getApplicationContext().getSharedPreferences("GameState", MODE_PRIVATE);
		boolean hasGameEnded = sharedPreferences.getBoolean("hasGameEnded", true);
		
		if(!hasGameEnded){
			wordsNumber = sharedPreferences.getInt("wordsNumber",wordsNumber);
		}
		
		backgroungMusic = MediaPlayer.create(NewGame_GameActivity.this, R.raw.newgame_back_ground_music);
		rightWord = MediaPlayer.create(NewGame_GameActivity.this, R.raw.newgame_right_word);
		playBackgroundMusic(isPlaying);
		
		// this part is to delete the words history
		// found last time
		if(hasGameEnded){
			WordWithBlanks.getWordsFound().clear();
			WordWithBlanks.getWordsNotFound().clear();
		} else {
			int size = sharedPreferences.getInt("wordsFoundNum", WordWithBlanks.getWordsFound().size());
			HashSet<String> wordsFound = new HashSet<String>();
			
			for(int i=0 ; i<size ; i++){
				String string = sharedPreferences.getString("wordsFound" +i, "");
				wordsFound.add(string);
			}
			
			// words not found
			size = sharedPreferences.getInt("wordsNotFoundNum", WordWithBlanks.getWordsNotFound().size());
			HashSet<String> wordsNotFound = new HashSet<String>();
			
			for(int i=0 ; i<size ; i++){
				String string = sharedPreferences.getString("wordsNotFound" +i, "");
				wordsNotFound.add(string);
			}

			WordWithBlanks.getWordsFound().addAll(wordsFound);
			WordWithBlanks.getWordsNotFound().addAll(wordsNotFound);
		}
		
		// this part is for the letters below
		gv_home = (GridView) findViewById(R.id.newgame_gv_home);
		if(hasGameEnded){
			gv_home.setAdapter(new NewGame_LetterGridAdapter(this,GenerateRamdomLetters.generate(letterNumber)));
		} else {
			String lettersInGridStr = sharedPreferences.getString("lettersInGrid","");
			char[] lettersInGrid = lettersInGridStr.toCharArray();
			
			String inVisibleInGrid = sharedPreferences.getString("inVisibleInGrid","");
			String[] split = inVisibleInGrid.split("-");
			boolean[] isInVisible = new boolean[lettersInGrid.length];
			for(int i=0 ; i<split.length ; i++){
				if(!split[i].equals("")){
					int splitInt = Integer.parseInt(split[i]);
					isInVisible[splitInt] = true;
				}
			}
			
			gv_home.setAdapter( new NewGame_LetterGridAdapter(this,lettersInGrid, isInVisible) );
		}

		// this part is the word on the front
//		Log.i("我是oncreate", "我在执行，哈哈哈");
		headerText = (TextView) findViewById(R.id.newgame_HeaderText);
		wordBar = (LinearLayout) findViewById(R.id.newgame_wordBar);
		
		if(hasGameEnded){
			NewGame_FillTheWordBar.getSingleFillTheWordBar(this,handler).fillLinearLayout(wordBar);
		} else {
			String originalWord = sharedPreferences.getString("originalWord", WordWithBlanks.getOriginalWord());
			String wordWithBlank = sharedPreferences.getString("WordWithBlank", WordWithBlanks.getWordWithBlank());
			WordWithBlanks.setOriginalWord(originalWord);
			WordWithBlanks.setWordWithBlank(wordWithBlank);
			WordWithBlanks.disableGenerateNext();
			NewGame_FillTheWordBar.getSingleFillTheWordBar(this,handler).fillLinearLayout(wordBar);
			WordWithBlanks.enableGenerateNext();
		}
		
		// this part is for the timer
		timeLeft = (TextView) findViewById(R.id.newgame_timeLeft);
		timeGoingDown();
		if(!hasGameEnded){
			timeLeftNow = sharedPreferences.getInt("timeLeft", timeLeftNow);
			timeLeft.setText(timeLeftNow+"");
		}
		
		// this part is for the game score
		newgameScore = (TextView) findViewById(R.id.newgame_score);
		if(!hasGameEnded){
			String newgameScoreStr = sharedPreferences.getString("newgameScore","");
			newgameScore.setText(newgameScoreStr);
		}
		
		// this button is to pause the game
		
		// this is to get the whole view of the game below
		// and I will block this view
		gameWholeLayout = (LinearLayout) findViewById(R.id.newgame_game_wholeView);
		
		coverGameContent = (ImageView) findViewById(R.id.newgame_cover_game_content);
		ImageView pauseButton = (ImageView) findViewById(R.id.newgame_game_pause_button);
		
		OnClickListener pauseOnClickListener = new OnClickListener() {
			
			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub
				ImageView pauseButton = (ImageView) view;
				if(gamePause == false){
					gamePause = true;
					
					// make the game pause
					pauseButton.setImageResource(getID("newgame_game_play"));
					playBackgroundMusic(false);
					
					coverGameContent.setVisibility(View.VISIBLE);
					coverGameContent.bringToFront();
//					gameWholeLayout.setClickable(true);
						
//					for ( int i = 0; i < gameWholeLayout.getChildCount();  i++ ){
//					    View childView = gameWholeLayout.getChildAt(i);
//					    childView.setClickable(false); // Or whatever you want to do with the view.
//					 }
				} else if(gamePause == true){
					gamePause = false;

					// make the game play
					pauseButton.setImageResource(getID("newgame_game_pause"));
					if(isPlaying){
						playBackgroundMusic(true);
					}
					
					coverGameContent.setVisibility(View.INVISIBLE);
					
//					gameWholeLayout.setClickable(false);
				}
			}
			
		};
		
		pauseButton.setOnClickListener(pauseOnClickListener);
		
		if(!hasGameEnded){
			gamePause = sharedPreferences.getBoolean("gamePause", gamePause);
			if(gamePause){
				gamePause = false;
				pauseOnClickListener.onClick(pauseButton);
			}
		}
		
		// this button is to show hint
		Button hintButton = (Button) findViewById(R.id.newgame_HintButton);
		if(!hasGameEnded){
			hintNumber = sharedPreferences.getInt("hintNumber",hintNumber);
			if(hintNumber == 0){
				hintButton.setTextColor(Color.parseColor("#ffAAAAAA"));
			}
		}
		hintButton.setText("Hint ("+hintNumber+")");
		hintButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Button hintButton = (Button)v;
				if(hintNumber>0 && hintNow != wordsNumber){
					hintNow = wordsNumber;
					changeTheScore(-1);
					headerText.setTextColor(Color.parseColor("#ffff8800"));
					headerText.setText(WordWithBlanks.getOriginalWord());
					hintNumber--;
					hintButton.setText("Hint ("+hintNumber+")");
					if(hintNumber == 0){
						hintButton.setTextColor(Color.parseColor("#ffAAAAAA"));
					}
				} 
				else if(hintNumber<=0){
					headerText.setTextColor(Color.parseColor("#ffff8800"));
					headerText.setText("No More Hints ~");
					waitThenChangeTitle(WORD_TITLE_BACK);
				}
			}
		});
		
		// this button is to generate next word
		nextButton = (Button)findViewById(R.id.newgame_NextButton);
		
		nextClickListener = new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				wordsNumber++;
				if(wordsNumber % 5 == 0){
					gv_home.setAdapter(new NewGame_LetterGridAdapter(NewGame_GameActivity.this,GenerateRamdomLetters.generate(letterNumber)));
				}
				
				if(headerText.getText().equals("Right Answer")){
					Log.i("NewGame_GameActivity", "nextOnClick: I am in right answer block");
					waitThenChangeTitle(WORD_TO_FILL);
				} else{
					WordWithBlanks.getWordsNotFound().add( WordWithBlanks.getOriginalWord() );
					Message msg = Message.obtain();
					msg.what = WORD_TO_FILL;
					handler.sendMessage(msg);
				}
			}

		};
		
		nextButton.setOnClickListener(nextClickListener);
		
		// this part is the background music part
		backmusicButton = (ImageView) findViewById(R.id.newgame_playMusicButton);
		
		OnClickListener backmusicOnClickListener = new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (isPlaying) {
					
					isPlaying = false;
					playBackgroundMusic(isPlaying);
					NewGame_GameActivity.this.backmusicButton
							.setImageDrawable(getResources().getDrawable(
									R.drawable.newgame_play_mute));
				} else if(!isPlaying){
					
					isPlaying = true;
					playBackgroundMusic(isPlaying);
					NewGame_GameActivity.this.backmusicButton
					.setImageDrawable(getResources().getDrawable(
							R.drawable.newgame_play_music));
				}
			}
		};
		
		backmusicButton.setOnClickListener(backmusicOnClickListener);
		
		if(!hasGameEnded){
			isPlaying = sharedPreferences.getBoolean("isMusicPlaying", false);
			if(!isPlaying){
				isPlaying = true;
				backmusicOnClickListener.onClick(backmusicButton);
			}
		}

		// this is the quit button part
		quitButton = (Button)findViewById(R.id.newgame_quitButton);
		quitButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				editor = sharedPreferences.edit();
				editor.putBoolean("hasGameEnded", true);
				editor.commit();
//				timeLeftNow =0;
				Log.i("Now the hasGameEndedis",sharedPreferences.getBoolean("hasGameEnded", false)+"");
				backgroungMusic.stop();
				NewGame_GameActivity.this.onDestroy();
			}
		});
		
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		finish();
	};
	
	@Override
	protected void onResume() {
		super.onResume();
		appOnPause = false;
		if(isPlaying && !gamePause){
			playBackgroundMusic(true);
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		appOnPause = true;
		playBackgroundMusic(false);
		editor = sharedPreferences.edit();
		if(timeLeftNow>0){
			editor.putBoolean("hasGameEnded", false);
			
			// time left
			editor.putInt("timeLeft", timeLeftNow);
			
			// board word
			editor.putString("originalWord", WordWithBlanks.getOriginalWord());
			editor.putString("WordWithBlank", WordWithBlanks.getWordWithBlank());
			
			// hint number
			editor.putInt("hintNumber",hintNumber);
			
			// wordsNumber
			editor.putInt("wordsNumber",wordsNumber);
			
			// score
			editor.putString("newgameScore",newgameScore.getText().toString());
					
			// grid letter
			boolean[] isInVisible = NewGame_LetterGridAdapter.getLetterGridAdapter().getIsInVisible();
			HashSet<Integer> nowInBlank = NewGame_FillTheWordBar.getSingleFillTheWordBar().getNowInBlank();
			String inVisibleNum="";
			for(int i=0 ; i<isInVisible.length ; i++){
				if(isInVisible[i] == true && !nowInBlank.contains(i)){
					if(inVisibleNum.equals("")){
						inVisibleNum += i; 
					}else{
						inVisibleNum = inVisibleNum + "-" + i; 
					}
				}
			}
			
			editor.putString("inVisibleInGrid",inVisibleNum);
			
			char[] lettersInGrid = NewGame_LetterGridAdapter.getLetterGridAdapter().getLettersInGrid();
//			String valueOf = String.valueOf(lettersInGrid);
			editor.putString("lettersInGrid",String.valueOf(lettersInGrid));
			
			// pause or not
			editor.putBoolean("gamePause", gamePause);
			editor.putBoolean("isMusicPlaying", isPlaying);
			
			// word bar (fill?)
			
			// words found 
			editor.putInt("wordsFoundNum", WordWithBlanks.getWordsFound().size());
			HashSet<String> wordsFound = WordWithBlanks.getWordsFound();
			
			int i=0;
			for(String str : wordsFound){
				editor.putString("wordsFound" +i, str);
				i++;
			}
			
			// words not found
			editor.putInt("wordsNotFoundNum", WordWithBlanks.getWordsNotFound().size());
			HashSet<String> wordsNotFound = WordWithBlanks.getWordsNotFound();
			
			i=0;
			for(String str : wordsNotFound){
				editor.putString("wordsNotFound" +i, str);
				i++;
			}
			
			editor.commit();
		}
	};
	
	private void playBackgroundMusic(boolean play) {
		// make the volumn available to the users
		if (play) {
			NewGame_GameActivity.this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
			backgroungMusic.setAudioStreamType(AudioManager.STREAM_MUSIC);
			backgroungMusic.setVolume(2, 2);
			backgroungMusic.start();
			backgroungMusic.setLooping(true);
		} else {
			backgroungMusic.pause();
		}
	}
	
	private void timeGoingDown(){
		new Thread() {
			public void run() {
				while(true){
//					timeLeft.setText(timeLeftNow);
					try {
						sleep(1000);
					} catch (InterruptedException e) {
						throw new RuntimeException(e);
					}
					Message msg = Message.obtain();
					msg.what = TIME_GOING_DOWN;
					handler.sendMessage(msg);
				}
			};
		}.start();
	}
	
	private void changeTheTimeLeft(){
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if(timeLeftNow <=10){
					timeLeft.setTextColor(Color.parseColor("#ffff4444"));
				}
				if(timeLeftNow >=0){
					timeLeft.setText(timeLeftNow+"");
				}
				if(timeLeftNow ==0){
					if(!headerText.getText().equals("Right Answer")){
						WordWithBlanks.getWordsNotFound().add( WordWithBlanks.getOriginalWord() );
					}
					
					// no need to continue
					editor = sharedPreferences.edit();
					editor.putBoolean("hasGameEnded", true);
					editor.commit();
					Intent intent = new Intent(NewGame_GameActivity.this, NewGame_ResultActivity.class);
					startActivity(intent);
					finish();
				}
			}
		});
	}
	
	private void waitThenChangeTitle(final int msgWhat) {
		// TODO Auto-generated method stub
		Log.i("NewGame_GameActivity", "waitThenChangeTitle: I'm in the function block");
		new Thread() {
			public void run() {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} finally{

					Log.i("NewGame_GameActivity", "waitThenChangeTitle: I sent message   "+msgWhat);
					Message msg = Message.obtain();
					msg.what = msgWhat;
					handler.sendMessage(msg);
				}
			}
		}.start();
	}
	
	private void changeHeaderText(final int WORD_JUDGE){
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if(WORD_JUDGE == WORD_IS_RIGHT){
					headerText.setTextColor(Color.parseColor("#ff669900"));
					headerText.setText("Right Answer");
					headerText.invalidate();
					// play the right word music
					playMusicEffect(RIGHT_WORD);
					
					// calculate the score
					changeTheScore(WordWithBlanks.getWordBlankNum());
					
					wordBar.setBackgroundColor(Color.parseColor("#2299cc00"));
					nextClickListener.onClick(nextButton);
				}
				else if(WORD_JUDGE == WORD_IS_WRONG){
					headerText.setTextColor(Color.parseColor("#ffcc0000"));
					headerText.setText("Wrong Answer");
				}
				
				else if(WORD_JUDGE == WORD_TO_FILL){
					Log.i("NewGame_GameActivity", "changeHeaderText: I get message to fill word");
					wordBar.removeAllViews();
					wordBar.postInvalidate();
					NewGame_FillTheWordBar.getSingleFillTheWordBar(NewGame_GameActivity.this,handler).fillLinearLayout(wordBar);
					headerText.setTextColor(Color.parseColor("#ff000000"));
					headerText.setText("Fill the Word");
					wordBar.setBackgroundColor(Color.parseColor("#ffffffff"));
					gameWholeLayout.postInvalidate();
				}
				
				else if(WORD_JUDGE == WORD_TITLE_BACK){
					Log.i("NewGame_GameActivity", "changeHeaderText: I get message to set title back");
					headerText.setTextColor(Color.parseColor("#ff000000"));
					headerText.setText("Fill the Word");
					wordBar.setBackgroundColor(Color.parseColor("#ffffffff"));
					gameWholeLayout.postInvalidate();
				}
			}


		});
	}
	
	private void playMusicEffect(int SITUATION){
		
		switch (SITUATION) {
		case RIGHT_WORD:
			NewGame_GameActivity.this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
			rightWord.setAudioStreamType(AudioManager.STREAM_MUSIC);
			rightWord.setVolume(2, 2);
			rightWord.start();
			break;

//		case :
//			break;
		}
	}
	
	private void changeTheScore(int scoreChange) {
		// TODO Auto-generated method stub
		CharSequence scoreStr = newgameScore.getText();
		Integer score = Integer.parseInt((String) scoreStr);
		score = score + scoreChange;

		boolean positive = true;
		if(score < 0){
			positive = false;
		}
		
		score = Math.abs(score);
		
		String scoreStrNew = score.toString();
		
		if(positive){
			
			for(int i = scoreStrNew.length();i<3;i++){
				scoreStrNew = "0"+ scoreStrNew;
			}
			
		} else { //negative
			
			for(int i = scoreStrNew.length();i<2;i++){
				scoreStrNew = "0"+ scoreStrNew;
			}

			scoreStrNew = "-" + scoreStrNew;
		}
		
		WordWithBlanks.setTotalScore(scoreStrNew);
		newgameScore.setText(scoreStrNew);
	}
	
	private int getID(String name){
		int resId = this.getResources().getIdentifier(name, "drawable" , this.getPackageName());
		return resId;
	}
}

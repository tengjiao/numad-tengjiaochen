package edu.neu.madcourse.tengjiaochen.newgame;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import edu.neu.madcourse.tengjiaochen.R;
import edu.neu.madcourse.tengjiaochen.newgame.view.NewGame_MyViewGroup;
import edu.neu.madcourse.tengjiaochen.newgame.view.NewGame_MyViewGroup.MyScrollListener;

public class NewGame_MainActivity extends Activity {

	//  把所有的图片ID写下来
	private int[] ids = new int[]{R.drawable.newgame_tutorial_a,R.drawable.newgame_tutorial_b,R.drawable.newgame_tutorial_c,R.drawable.newgame_tutorial_d};
	private NewGame_MyViewGroup myViewGroup;
	
	private RadioGroup radioGroup;
	private LinearLayout bodyLayout;
	private SharedPreferences sharedPreferences;
	private Editor editor;
	private Button ready;
	private Button continueButton;
	
	private int viewGroupExtend;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.newgame_activity_main);
		setTitle("Tutorial");
		myViewGroup = new NewGame_MyViewGroup(this);

        radioGroup =(RadioGroup)findViewById(R.id.newgame_radioGroup);
        
        sharedPreferences = getApplicationContext().getSharedPreferences("GameState", MODE_PRIVATE);
        boolean hasGameEnded = sharedPreferences.getBoolean("hasGameEnded", true);
        
        Log.i("MainAct:Now the hasGameEndedis",sharedPreferences.getBoolean("hasGameEnded", false)+"");
        
        if(!hasGameEnded){
        	viewGroupExtend = 2; // one more continue page
        } else {
        	viewGroupExtend = 1;
        }
        
        // add the radio group
        for (int i = 0; i < ids.length + viewGroupExtend; i++) {
        	RadioButton radioButton = new RadioButton(this);
        	radioGroup.addView(radioButton);
        	
        	if(i==0){
        		radioButton.setChecked(true);
        	}
        	
        	// store the number
        	radioButton.setTag(i);
        	
        	radioButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					// TODO Auto-generated method stub
					if(isChecked){
						myViewGroup.moveToDest((Integer)buttonView.getTag());
//						showToastInMainThread("我是第" + (Integer)buttonView.getTag() +"个");
					}
				}
			});
		}
		
        // put in the "continue" layout
        if(!hasGameEnded){
        	View viewContinue = getLayoutInflater().inflate(R.layout.newgame_view_continue,null);
        	myViewGroup.addView(viewContinue);
        }
        
        // put in all the pictures
        for (int i = 0; i < ids.length; i++) {
        	ImageView image = new ImageView(this);
        	image.setBackgroundResource(ids[i]);
        	myViewGroup.addView(image);
		}
        
        // put in the "Are you Ready" layout
        View viewReady = getLayoutInflater().inflate(R.layout.newgame_view_ready,null);
        
        myViewGroup.addView(viewReady);
        bodyLayout = (LinearLayout) findViewById(R.id.newgame_bodyLayout);
        bodyLayout.addView(myViewGroup);
        
        ready = (Button) findViewById(R.id.newgame_readyButton);
        ready.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(NewGame_MainActivity.this, NewGame_GameActivity.class);
				startActivity(intent);
				editor = sharedPreferences.edit();
				editor.putBoolean("hasGameEnded", true);
				editor.commit();
				finish();
			}
		});
        
        if(!hasGameEnded){
	        continueButton = (Button) findViewById(R.id.newgame_continueButton);
	        
	        continueButton.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(NewGame_MainActivity.this, NewGame_GameActivity.class);
					startActivity(intent);
					finish();
				}
			});
        }
        
        myViewGroup.setMyScrollListener(new MyScrollListener() {
			
			@Override
			public void moveToDest(int destId) {
				// TODO Auto-generated method stub
				( (RadioButton)radioGroup.getChildAt(destId) ).setChecked(true);
			}
		});
	}
	
	/**
	 * 在主线程显示土司
	 * 
	 * @param text
	 */
	public void showToastInMainThread(final String text) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(getApplicationContext(), text, 1).show();

			}
		});
	}
	
 
}


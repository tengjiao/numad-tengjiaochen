package edu.neu.madcourse.tengjiaochen.newgame;

import java.util.HashSet;

import edu.neu.madcourse.tengjiaochen.R;
import edu.neu.madcourse.tengjiaochen.dictionary.Dictionary_Acknowdgments;
import edu.neu.madcourse.tengjiaochen.dictionary.Dictionary_MainActivity;
import edu.neu.madcourse.tengjiaochen.newgame.service.WordWithBlanks;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class NewGame_ResultActivity extends Activity{
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.newgame_activity_result);
		
		// this is for the score part
		TextView totalScore = (TextView) findViewById(R.id.newgame_total_score);
		totalScore.setText("Total Score: "+WordWithBlanks.getTotalScore());
		
		// this is the quit button part
		Button quitButton = (Button)findViewById(R.id.newgame_quitButton);
		quitButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
		Button playAgain = (Button) findViewById(R.id.newgame_playAgain);
		playAgain.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(NewGame_ResultActivity.this, NewGame_GameActivity.class);
				startActivity(intent);
				finish();
			}
		});
		
		Button acknowledgement = (Button) findViewById(R.id.newgame_Acknowledgement);
		acknowledgement.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(NewGame_ResultActivity.this,NewGame_AcknowdgmentsActivity.class));
			}
		});

		LinearLayout wordsFound = (LinearLayout) findViewById(R.id.Newgame_WordsFound);
		HashSet<String> wordsFoundResult = WordWithBlanks.getWordsFound();
		
		for(String str:wordsFoundResult){
			TextView tv = new TextView(this);
			tv.setLayoutParams(new ViewGroup.LayoutParams(
					ViewGroup.LayoutParams.WRAP_CONTENT,
					ViewGroup.LayoutParams.WRAP_CONTENT));
			tv.setText(str);
			tv.setTextSize(15);
			wordsFound.addView(tv);
		}
		
		LinearLayout wordsNotFound = (LinearLayout) findViewById(R.id.Newgame_WordsNotFound);
		HashSet<String> wordsNotFoundResult = WordWithBlanks.getWordsNotFound();
		
		for(String str:wordsNotFoundResult){
			TextView tv = new TextView(this);
			tv.setLayoutParams(new ViewGroup.LayoutParams(
					ViewGroup.LayoutParams.WRAP_CONTENT,
					ViewGroup.LayoutParams.WRAP_CONTENT));
			tv.setText(str);
			tv.setTextSize(15);
			wordsNotFound.addView(tv);
		}
		
	}
}

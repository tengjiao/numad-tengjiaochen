package edu.neu.madcourse.tengjiaochen.newgame.service;

import java.util.ArrayList;
import java.util.HashSet;

import android.test.AndroidTestCase;
import android.util.Log;
import edu.neu.madcourse.tengjiaochen.dictionary.db.dao.DictionaryRecordDao;
import edu.neu.madcourse.tengjiaochen.dictionary.db.dao.DictionaryRecordDaoFactory;
import edu.neu.madcourse.tengjiaochen.dictionary.db.domain.DictionaryRecord;
import edu.neu.madcourse.tengjiaochen.newgame.util.GenerateLeftLettersNeeded;
import edu.neu.madcourse.tengjiaochen.newgame.util.GenerateRandomWordLength;

public class WordWithBlanks extends AndroidTestCase {
	// 随机产生4,5,6,7长度单词
	// 随机拿到词库剩余字母
	// 从数据库查到单词
	private static String totalScore;

	private static String originalWord;
	private static String WordWithBlank;

	private static HashSet<String> wordsFound = new HashSet<String>();
	private static HashSet<String> wordsNotFound = new HashSet<String>();

	private static int wordBlankNum;

	private static DictionaryRecordDao dictionaryRecordDaoNow;
	
	private static boolean isGeneNextDisabled = false;
	
	public static void disableGenerateNext(){
		isGeneNextDisabled = true;
	}

	public static void enableGenerateNext(){
		isGeneNextDisabled = false;
	}

	public static void generateNext() {
		if(!isGeneNextDisabled){
			String wordWithSpecifiedLetters = null;
			String[] lettersNeeded = {};
	
			// This method is not perfect!!
			// when I cannot find the word
			// for 5 times, I should change
			// the girdview for a new one
			// cause I cannot find any word
			while (wordWithSpecifiedLetters == null) {
				int length = GenerateRandomWordLength.generatelenth();
				lettersNeeded = GenerateLeftLettersNeeded.generate();
				wordBlankNum = lettersNeeded.length;
				DictionaryRecordDao dictionaryRecordDao = DictionaryRecordDaoFactory
						.getDictionaryRecordDaoFactory().getDictionaryRecordDao(
								length);
				dictionaryRecordDaoNow = dictionaryRecordDao;
				wordWithSpecifiedLetters = dictionaryRecordDao
						.getWordWithSpecifiedLetters(lettersNeeded, length);
			}
	
			Log.i("The word I get is", wordWithSpecifiedLetters);
			originalWord = wordWithSpecifiedLetters;
			WordWithBlank = "" + originalWord;
			for (String str : lettersNeeded) {
				WordWithBlank = WordWithBlank.replaceFirst(str, "_");
				Log.i("WordWithBlank is ", WordWithBlank);
			}
			Log.i("WordWithBlank is ", WordWithBlank);
		}
	}

	public static boolean FindInDictionary(String word) {
		boolean find;
		
		if( dictionaryRecordDaoNow == null){
			dictionaryRecordDaoNow = DictionaryRecordDaoFactory
					.getDictionaryRecordDaoFactory().getDictionaryRecordDao(word.length());
		}
		
		find = dictionaryRecordDaoNow.find(new DictionaryRecord(word));
		return find;
	}

	public static String getOriginalWord() {
		return originalWord;
	}

	public static String getWordWithBlank() {
		return WordWithBlank;
	}

	public static int getWordBlankNum() {
		return wordBlankNum;
	}

	public static HashSet<String> getWordsFound() {
		return wordsFound;
	}

	public static HashSet<String> getWordsNotFound() {
		return wordsNotFound;
	}

	public static String getTotalScore() {
		return totalScore;
	}

	public static void setTotalScore(String totalScore) {
		WordWithBlanks.totalScore = totalScore;
	}

	public static void setOriginalWord(String originalWord) {
		WordWithBlanks.originalWord = originalWord;
	}

	public static void setWordWithBlank(String wordWithBlank) {
		WordWithBlank = wordWithBlank;
	}
}

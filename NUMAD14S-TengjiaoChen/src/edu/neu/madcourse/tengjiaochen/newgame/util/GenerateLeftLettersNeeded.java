package edu.neu.madcourse.tengjiaochen.newgame.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import android.util.Log;
import edu.neu.madcourse.tengjiaochen.newgame.view.NewGame_LetterGridAdapter;

public class GenerateLeftLettersNeeded {
	public static String[] generate(){
		Random rand = new Random();
		String[] charArrstr = null;
		NewGame_LetterGridAdapter letterGrid = NewGame_LetterGridAdapter.getLetterGridAdapter();
		ArrayList<Character> leftCharlist = new ArrayList<Character>();
		leftCharlist.addAll(letterGrid.getLeftCharlist());
		
		Log.i("Now the list I get is ",letterGrid.getLeftCharlist().toString());
		
		String charSequence = "";
		
		// maybe 2 or 3 blanks
		int blankLetterNumber = 2 + rand.nextInt(2);
		
		for (int i = 0; i<blankLetterNumber && !leftCharlist.isEmpty();i++) {
			int nextInt = rand.nextInt(10000);
			nextInt = nextInt % leftCharlist.size();
			charSequence = charSequence + leftCharlist.remove(nextInt);
		}
		
		char[] charArray = charSequence.toCharArray();
		charArrstr = new String[charArray.length];
		
		for(int i=0;i<charArray.length;i++){
			charArrstr[i] = "" + charArray[i];
		}
		
		return charArrstr;
	}
}

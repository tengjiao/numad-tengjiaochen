package edu.neu.madcourse.tengjiaochen.newgame.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

public class GenerateRamdomLetters {

	public static char[] generate(int num) {

		Random rand = new Random();
		// List<Character> list = new ArrayList<Character>();

		// add the char beyond 26
		Set<Character> set = new TreeSet<Character>();
		for (int i = 0; set.size() != num - 26;) {
			int n = rand.nextInt(26);
			char ch = (char) ('a' + n);
			set.add(ch);
		}

		List<Character> list = new ArrayList<Character>();
		list.addAll(set);

		for (int i = 0; i < 26; i++) {
			char ch = (char) ('a' + i);
			list.add(ch);
		}

		List<Character> listRandom = new ArrayList<Character>();

		String charSequence = "";
		while (!list.isEmpty()) {
			int nextInt = rand.nextInt(10000);
			nextInt = nextInt % list.size();
			charSequence = charSequence + list.remove(nextInt);
		}

		char[] charArray = charSequence.toCharArray();

		return charArray;
	}
}

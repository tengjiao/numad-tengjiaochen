package edu.neu.madcourse.tengjiaochen.newgame.view;


import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import edu.neu.madcourse.tengjiaochen.newgame.NewGame_GameActivity;
import edu.neu.madcourse.tengjiaochen.newgame.service.WordWithBlanks;
import edu.neu.madcourse.tengjiaochen.twoplayer.TwoPlayer_GameActivity;


public class NewGame_FillTheWordBar {
	
	private static NewGame_FillTheWordBar fillTheWordBar= new NewGame_FillTheWordBar();
	private static Context context;
	private static Handler handler;
	private String wordtoFind = "kite";
	private HashMap<ImageView, Integer> indexPosition = new HashMap<ImageView, Integer>();
	private HashSet<Integer> nowInBlank = new HashSet<Integer>();
	
	public HashSet<Integer> getNowInBlank() {
		return nowInBlank;
	}

	public static NewGame_FillTheWordBar getSingleFillTheWordBar(){
		return fillTheWordBar;
	}
	
	public static NewGame_FillTheWordBar getSingleFillTheWordBar(Context context,Handler handler){
		fillTheWordBar.context = context;
		fillTheWordBar.handler = handler;
		return fillTheWordBar;
	}
	
	// for combining word
	private String[] wordCombine = {"","","","","","","","","",""};
	

	// 假设我能从剩余的词库里面拿到字母，并且查到单词
	// 这需要三个技术，如下

	// 随机产生4,5,6,7长度单词
	// 随机拿到词库剩余字母
	// 从数据库查到单词

	// "kite"
	// "bloom"
	// "around"
	// "special"

	// 随机得到2-3个空格

	public void fillLinearLayout(LinearLayout wordlayout) {
		Log.i("fillLinearLayout", "我在执行，哈哈哈");
		nowInBlank.clear();
		
		wordlayout.removeAllViews();
		wordlayout.invalidate();
		// This is for getting a word
		WordWithBlanks.generateNext();

		wordtoFind = WordWithBlanks.getOriginalWord();
		String replace = WordWithBlanks.getWordWithBlank();
		// 随机得到2-3个空格 -- 这个应该在词库拿到
		// 2个空格，4，5长度；3个空格6,7长度
		
		initwordCombine();
//		for(int i=0;i<wordCombine.length;i++){
//			wordCombine[i] ="";
//			Log.i("这个str是","后便不能有字"+wordCombine[i]);
//		}

		char[] charArray = replace.toCharArray();

		Log.i("fillLinearLayout函数头，wordCombine里面是", Arrays.toString(NewGame_FillTheWordBar.this.wordCombine));
		
		for (int i=0; i< charArray.length;i++) {
			int resId;
			ImageView imageView = new ImageView(context);
			int imageWidth = context.getResources().getDisplayMetrics().widthPixels/8;
			int imageHeight = imageWidth;
			imageView.setLayoutParams(new LayoutParams(imageWidth, imageHeight));
			if (charArray[i] == '_') {
				resId = context.getResources().getIdentifier(
						"newgame_board_for_letter", "drawable",
						context.getPackageName());
				indexPosition.put(imageView, i);
			} else {
				wordCombine[i] = "" + charArray[i]; 
				String name = "newgame_unselected_letter_" + charArray[i];
				resId = context.getResources().getIdentifier(name, "drawable",
						context.getPackageName());
			}
			imageView.setImageResource(resId);
			
			if(charArray[i]=='_'){
				imageView.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						ImageView blankBoard = (ImageView) v;
						NewGame_LetterGridAdapter letterGridAdapter = NewGame_LetterGridAdapter.getLetterGridAdapter();
						Integer nowSelected = letterGridAdapter.getNowSelected();
						
						// when the image is filled with word
						// your tap means you wanna move it away
						if(blankBoard.getTag()!=null){ //blankBoard.getTag()!=null
							ImageView getLetterBack = (ImageView)blankBoard.getTag();
							
							// set the board without letter
							blankBoard.setImageResource(getID("newgame_board_for_letter"));
							
							// make the back image visible again 
							getLetterBack.setVisibility(View.VISIBLE);
							getLetterBack.setClickable(true);
				
							// keep the set know what letter is in the blank
							int letterPosiBack = letterGridAdapter.getPosition(getLetterBack);
							nowInBlank.remove(letterGridAdapter);
							
							//remove tag
							blankBoard.setTag(null);
							
							// clear the word
							wordCombine[indexPosition.get(blankBoard)] ="";
							
							// add to the existing list
							char charInGetLetterBack = letterGridAdapter.getLettersInGrid()[letterGridAdapter.getPosition(getLetterBack)];
							letterGridAdapter.getLeftCharlist().add(charInGetLetterBack);
						}
						
						// when some block selected and you tap
						// your tap means you wanna add this letter
						if(nowSelected!=null && blankBoard.getTag()==null){
							// get the selected letter
							ImageView selectedLetterInGrid = (ImageView) letterGridAdapter.getItem(nowSelected);
//							blankBoard.setImageResource((int) letterGridAdapter.getItemId(nowSelected));
							Character selectedChar = letterGridAdapter.getLettersInGrid()[nowSelected];
							
							// write to the wordCombine for checking
							wordCombine[indexPosition.get(blankBoard)] = ""+selectedChar;
							String name = "newgame_selected_letter_"+selectedChar;
							
							// draw letter on this blank board
							blankBoard.setImageResource(getID(name));
							
							// disable the selected letter
							selectedLetterInGrid.setVisibility(View.INVISIBLE);
							selectedLetterInGrid.setClickable(false);
							
							// keep the set know what letter is in the blank
							int letterPosiInBlank = letterGridAdapter.getPosition(selectedLetterInGrid);
							nowInBlank.add(letterPosiInBlank);
							
							// undo select the letter
							selectedLetterInGrid.setImageResource(getID("newgame_unselected_letter_"+selectedChar));
							letterGridAdapter.setNowSelected(null);
							
							// set tag with this info
							blankBoard.setTag(selectedLetterInGrid);
							Log.i("NewGame_FilltheWordBar","charlist: "+letterGridAdapter.getLeftCharlist().toString());
							Log.i("NewGame_FilltheWordBar","selectedLetterInGrid: "+selectedChar);
							
							// remove from the existing list
							letterGridAdapter.getLeftCharlist().remove(selectedChar);
							
							
							Log.i("NewGame_FilltheWordBar","charlist: "+letterGridAdapter.getLeftCharlist().toString());
						} 
						String compare ="";
						
						for(String str:NewGame_FillTheWordBar.this.wordCombine){
							compare = compare +str;
						}
						
						if(compare.length() == NewGame_FillTheWordBar.this.wordtoFind.length()){
							Message msg = Message.obtain();
							// if compare is equal to the word
							// or found in DB 
							// the answer is right
							if(compare.equals(NewGame_FillTheWordBar.this.wordtoFind)||WordWithBlanks.FindInDictionary(compare) ){
								msg.what = NewGame_GameActivity.WORD_IS_RIGHT;
								handler.sendMessage(msg);
								Log.i("我在发单词正确的消息", compare);
								WordWithBlanks.getWordsFound().add(compare);
							}else {
								msg.what = NewGame_GameActivity.WORD_IS_WRONG;
								handler.sendMessage(msg);
								Log.i("我在发单词错误的消息", compare);
								
							}
						}
						
						Log.i("现在的单词是", compare);
						Log.i("wordCombine里面是", Arrays.toString(NewGame_FillTheWordBar.this.wordCombine));
					}
				});
			}
			wordlayout.addView(imageView);
		}
		wordlayout.invalidate();
	}

	private void initwordCombine() {
		for(int i=0;i<wordCombine.length;i++){
			wordCombine[i] ="";
//			Log.i("这个str是","这时下面的，后面不能有字"+wordCombine[i]);
		}
	}

	private int getID(String name) {
		int resId = context.getResources().getIdentifier(name, "drawable",
				context.getPackageName());
		return resId;
	}
}


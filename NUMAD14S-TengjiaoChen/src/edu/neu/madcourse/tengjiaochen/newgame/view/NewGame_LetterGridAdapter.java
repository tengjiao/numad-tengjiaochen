package edu.neu.madcourse.tengjiaochen.newgame.view;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

import edu.neu.madcourse.tengjiaochen.R;
import edu.neu.madcourse.tengjiaochen.newgame.NewGame_GameActivity;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.provider.ContactsContract.CommonDataKinds.Im;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class NewGame_LetterGridAdapter extends BaseAdapter {

	private static NewGame_LetterGridAdapter letterGridAdapter;
	private ImageView[] imageView ;
	private Context context;
	private char[] lettersInGrid;
	private boolean[] isInVisible;

	private ArrayList<Character> charlist = new ArrayList<Character>() ;

	protected MediaPlayer letterClicked ;
	
	private Integer nowSelected =null;
//	private ArrayList<ImageView> imageViewList = new ArrayList<ImageView>();
//	private HashSet<ImageView> imageViewset = new HashSet<ImageView>();
	private HashMap<ImageView, Integer> image_posi = new HashMap<ImageView, Integer>();
	
	
	public NewGame_LetterGridAdapter(Context context,char[] lettersInGrid) {
		super();
		imageView = new ImageView[lettersInGrid.length];
		this.context = context;
		this.lettersInGrid = lettersInGrid;
		isInVisible = new boolean[lettersInGrid.length];
		for(char ch:lettersInGrid){
			charlist.add(ch);
		}
		letterGridAdapter = this;
		letterClicked = MediaPlayer.create(context, R.raw.newgame_letter_clicked);
	}

	public NewGame_LetterGridAdapter(Context context,char[] lettersInGrid,boolean[] isInVisible) {
		super();
		imageView = new ImageView[lettersInGrid.length];
		this.context = context;
		this.lettersInGrid = lettersInGrid;
		this.isInVisible = isInVisible;
		for(char ch:lettersInGrid){
			charlist.add(ch);
		}
		letterGridAdapter = this;
		letterClicked = MediaPlayer.create(context, R.raw.newgame_letter_clicked);
	}
	
	public static NewGame_LetterGridAdapter getLetterGridAdapter() {
		return letterGridAdapter;
	}

	public NewGame_LetterGridAdapter() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 35;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
//		Log.i("我是getItem", "哈哈哈");
		
		return imageView[position];
	}

	@Override
	public long getItemId(int position) {
		
		// TODO Auto-generated method stub
//		Log.i("我是getItemId", "哈哈哈");
		return getID("newgame_unselected_letter_"+lettersInGrid[position]);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
//		Log.i("我是getView", "哈哈哈"+ position);//启动的时候，只执行这句
		// TODO Auto-generated method stub
//		View view = View.inflate(context,R.drawable.unselected_letter_b,null);
		if (imageView[position] != null) {
			return imageView[position];
		} else {
			imageView[position] = new ImageView(context);
			int imageWidth = context.getResources().getDisplayMetrics().widthPixels/8;
			int imageHeight = imageWidth;
			imageView[position].setLayoutParams(new GridView.LayoutParams(imageWidth,
					imageHeight));
			// imageView.setImageResource(R.drawable.unselected_letter_b);
			imageView[position].setImageResource(getID("newgame_unselected_letter_"
					+ lettersInGrid[position]));
			imageView[position].setScaleType(ImageView.ScaleType.FIT_CENTER);
			imageView[position].setTag(position);
//			imageView[position].setTag(1,lettersInGrid[position]);
			imageView[position].setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
//					boolean containsValue = LetterGridAdapter.this.posi_image.containsValue(v);
					ImageView iv = (ImageView) v;
					int position = (Integer) v.getTag();
					if (nowSelected != null) {
						imageView[nowSelected].setImageResource(getID("newgame_unselected_letter_"
								+ lettersInGrid[nowSelected]));
					}
					
					imageView[position].setImageResource(getID("newgame_selected_letter_"
							+ lettersInGrid[position]));
					
					if(nowSelected == null || position!=nowSelected ){
						playMusicEffectLetterClicked();
					}
					
					nowSelected = position;
				}
			});
			
			image_posi.put(imageView[position], position);
//			charlist.add(lettersInGrid[position]);
			
			if(isInVisible[position]){
				imageView[position].setVisibility(View.INVISIBLE);
			}
			
			return imageView[position];
		}
		
	}
	
//	int resId = getResources().getIdentifier("name", "drawable" , context.getPackageName());
//	name // 文件名
//	drawable //文件类型
//	context.getPackageName() //包名
	
	private int getID(String name){
		int resId = context.getResources().getIdentifier(name, "drawable" , context.getPackageName());
		return resId;
	}

	public void setNowSelected(Integer nowSelected) {
		this.nowSelected = nowSelected;
	}

	public Integer getNowSelected() {
		return nowSelected;
	}

	public char[] getLettersInGrid() {
		return lettersInGrid;
	}
	
	/**
	 * 得到改imageview的具体位置
	 * @param imageview
	 * @return
	 */
	public int getPosition(ImageView imageview){
		return image_posi.get(imageview);
	}
	
	/**
	 * 得到剩余的字母
	 * @return
	 */
	public ArrayList<Character> getLeftCharlist() {
		return charlist;
	}
	
	private void playMusicEffectLetterClicked(){
//		context.setVolumeControlStream(AudioManager.STREAM_MUSIC);
		letterClicked.setAudioStreamType(AudioManager.STREAM_MUSIC);
		letterClicked.setVolume(2, 2);
		letterClicked.start();
	}
	
	public boolean[] getIsInVisible() {
		for(int i =0; i<imageView.length ; i++){
			
			if(imageView[i].getVisibility() == View.INVISIBLE){
				isInVisible[i] = true;
			}
			
		}
		return isInVisible;
	}
	

	public void setIsInVisible(boolean[] isInVisible) {
		this.isInVisible = isInVisible;
	}
}

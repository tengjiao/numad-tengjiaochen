package edu.neu.madcourse.tengjiaochen.newgame.view;

import android.content.Context;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Scroller;

public class NewGame_MyViewGroup extends ViewGroup {

	private GestureDetector gestureDetector;
	private Scroller scroller;
	private Context context;
	float x1=0; 
	float x2=0;
	float y1=0; 
	float y2=0;
	float time1=0;
	float time2=0;
	private float velocityX = 0;
	int idNow = 0;
	private MyScrollListener myScrollListener;

	// TODO Auto-generated constructor stub
	public NewGame_MyViewGroup(Context context) {
		super(context);
		this.context = context;
		initView();
	}

	private void initView() {
		scroller = new Scroller(context);
		// 第三个参数是handler，不写handler的方法已经过时了
		gestureDetector = new GestureDetector(context, myGestureListener ,null);
		
	}
	
	// ViewGroup 的方法
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		gestureDetector.onTouchEvent(event);
		int curId =getScrollX() / getWidth();	
		switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				time2 = System.nanoTime();
				
				velocityX = 0;
				x1 = event.getX();
				y1 = event.getY();
				idNow = (int) (getScrollX()/getWidth());
				break;
			case MotionEvent.ACTION_MOVE:
				
				break;
			case MotionEvent.ACTION_UP:
				x2 = event.getX();
				y2 = event.getY();
				moveToDest();
				time1 = time2;
				break;
			default:
				break;
			}
			
		 
		 return true;
	}

	private void moveToDest() {
		// TODO Auto-generated method stub
		// 如果x2-x1大于0 --> 左移
		// 如果x2-x1小于0 --> 右移
		int destId = idNow;
		
		if(x2-x1 >getWidth()/5 || ( (x2-x1) >getWidth()/16 && y2-y1<-30) || velocityX >450){
			destId = destId -1;
		} else if (x2-x1 < -getWidth()/5|| ( (x2-x1) <-getWidth()/16 && y2-y1>30) || velocityX < -450) {
			destId = destId +1;
		}

		if(destId >= getChildCount()){
			destId = getChildCount()-1;
		} else if(destId <= 0){
			destId = 0;
		}
		
		moveToDest(destId);
		
	}

	public void moveToDest(int destId) {
		// TODO Auto-generated method stub
		int distance = destId * getWidth() - getScrollX();
		
		if(myScrollListener!=null){
			myScrollListener.moveToDest(destId);
		}
		
//		scrollBy(distance, 0);
		scroller.startScroll(getScrollX(), getScrollY(), distance, 0,Math.abs(distance) );
		// 用来刷新,调用computeScroll
		invalidate();
	}
	
	// 这个方法加上以后才会，显示我那张带图片的view
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		// TODO Auto-generated method stub
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		
		for (int i = 0; i < getChildCount(); i++) {
			View view = getChildAt(i);
			view.measure(widthMeasureSpec, heightMeasureSpec);
		}
	}
	
	//这个方法用于时时刻刻刷新，改移动的距离
	@Override
	public void computeScroll() {
		if(scroller.computeScrollOffset()){
			// getCurrX是指，到现在按照startScroll，改移动到什么地方了
			scrollTo(scroller.getCurrX(), scroller.getCurrY());
			invalidate();
		}
	}

	@Override
	// TODO Auto-generated method stub
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		for (int i = 0; i < getChildCount(); i++) {
			View view = getChildAt(i);
			// 用这行代码，把若干个窗口设置好
			view.layout(l + i * getWidth(), t, r + i * getWidth(), b);
		}
	}
	
	OnGestureListener myGestureListener = new OnGestureListener() {

		@Override
		public boolean onDown(MotionEvent e) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public void onShowPress(MotionEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public boolean onSingleTapUp(MotionEvent e) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean onScroll(MotionEvent e1, MotionEvent e2,
				float distanceX, float distanceY) {
			// TODO Auto-generated method stub
			scrollBy((int)distanceX, 0);
			return false;
		}

		//长按事件
		@Override
		public void onLongPress(MotionEvent e) {
			// TODO Auto-generated method stub
			
		}

		// 快速滑动
		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
				float velocityY) {
			// TODO Auto-generated method stub
//			Log.i("现在速度", ""+(velocityX));
//			velocityX = velocityX;
			NewGame_MyViewGroup.this.velocityX = velocityX; 
			return false;
		}
		
		
	};

	public interface MyScrollListener{
		public void moveToDest(int destId);
	}
	
	public MyScrollListener getMyScrollListener() {
		return myScrollListener;
	}

	public void setMyScrollListener(MyScrollListener myScrollListener) {
		this.myScrollListener = myScrollListener;
	}
	

}


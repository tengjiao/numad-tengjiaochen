package edu.neu.madcourse.tengjiaochen.trickestpart;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import edu.neu.madcourse.tengjiaochen.R;
import edu.neu.madcourse.tengjiaochen.trickestpart.data.TrickestPart_JumpData;
import edu.neu.madcourse.tengjiaochen.trickestpart.data.TrickestPart_JumpDataImpl;
import edu.neu.madcourse.tengjiaochen.trickestpart.model.TrickestPart_Direction;
import edu.neu.madcourse.tengjiaochen.trickestpart.util.TrickestPart_CreateArrows;
import edu.neu.madcourse.tengjiaochen.trickestpart.util.TrickestPart_ImageFiller;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.ColorDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class TrickestPart_Activity extends Activity implements
		SensorEventListener {
	private final String TAG = "TrickestPart_Activity";

	public final static int LEFT_DETECTED = 0;
	public final static int RIGHT_DETECTED = 1;
	public final static int MAKE_IT_THROUGH = 2;
	public final static int SHAKE = 3;

	private Toast toast;

	// these 2 variables are for the sensing part
	private SensorManager mSensorManager;
	private Sensor mSensor_acc;
	private Sensor mSensor_gravity;
	private Sensor mSensor_angle;

	// this layout is for arrows
	private LinearLayout arrowLayout;
	// this is to save the new series of arrows
	private List<TrickestPart_Direction> newArrows;
	// left -90; right +90;around 180
	private Float direction_angle = null;

	// these are arrows to detect
	private List<ImageView> arrowImageToDetect = new ArrayList<ImageView>();

	// this is to save the jump data
	private TrickestPart_JumpData jumpdata;

	// handler to send message
	private static Handler handler;

	// util
	private TrickestPart_ImageFiller imageFiller;

	private TextView scoreTV;

	private final int PROGRESS_MAX = 10000;

	private boolean isSetProgressAllowed = true;

	// data for 4 stages
	private long[] stageWholeTimeArray = { 0, 20000, 20000, 35000, 35000,
			35000, 30000 };
	private int[] arrowNumArray = { 0, 2, 3, 4, 4, 4, 4 };
	private int[] stagePeriodTimeArray = { 0, 5000, 4000, 3000, 2500, 2300,
			2000 };

	private int currentStage = 1;
	private int arrowNum = 2;
	private int stagePeriodTime = 2;
	private long stageWholeTime = 2;
	boolean isStageThreadAllowed = true;

	private static final int SET_ICE1 = 1;
	private static final int SET_ICE2 = 2;
	private static final int SET_ICE3 = 3;
	private static final int SET_ICE4 = 4;
	
	private static final int ICE_BREAK = 0;
	
	private ImageView imageShake;
	private MediaPlayer iceBreak;
	protected SharedPreferences sharedPreferences;

	@SuppressLint("InlinedApi")
	@Override
	// TODO Auto-generated method stub
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.trickestpart_game_activity);

		currentStage = 1;
		iceBreak = MediaPlayer.create(TrickestPart_Activity.this, R.raw.trickestpart_icesound);
		
		// stage title
		tvStage = (TextView) findViewById(R.id.trickest_stage);

		// find the progress bar and initialize it
		progressBar = (ProgressBar) findViewById(R.id.trickestpart_timeleft);
		ClipDrawable greenDraw = new ClipDrawable(new ColorDrawable(
				Color.parseColor("#ff44bb44")), Gravity.LEFT,
				ClipDrawable.HORIZONTAL);
		progressBar.setProgressDrawable(greenDraw);
		progressBar.setBackgroundColor(Color.parseColor("#cccccccc"));
		progressBar.setMax(PROGRESS_MAX);
		progressBar.setProgress(PROGRESS_MAX);

		// initiate the sensor
		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		mSensor_acc = mSensorManager
				.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
		mSensor_gravity = mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
		mSensor_angle = mSensorManager
				.getDefaultSensor(Sensor.TYPE_ORIENTATION);

		arrowLayout = (LinearLayout) findViewById(R.id.trickest_arrow_layout);
		scoreTV = (TextView) findViewById(R.id.trickest_score);

		toast = Toast.makeText(this, "", Toast.LENGTH_SHORT);

		// next job is to get these states back
		sharedPreferences = getApplicationContext().getSharedPreferences(
				"Jump", MODE_PRIVATE);
		int highestScore = sharedPreferences.getInt("highest", 000);
		tvHighestScore = (TextView) findViewById(R.id.trickest_highest_score);
		String highestScoreStr = highestScore + "";
		for (int i = highestScoreStr.length(); i < 3; i++) {
			highestScoreStr = "0" + highestScoreStr;
		}

		tvHighestScore.setText(highestScoreStr);

		handler = new Handler() {
			public void handleMessage(android.os.Message msg) {
				if (msg.what == LEFT_DETECTED || msg.what == RIGHT_DETECTED
						|| msg.what == MAKE_IT_THROUGH) {
					TrickestPart_Direction direction;

					if (!newArrows.isEmpty()) {
						direction = newArrows.get(0);
					} else {
						return;
					}

					if ((direction == TrickestPart_Direction.LEFT && msg.what == LEFT_DETECTED)
							|| (direction == TrickestPart_Direction.RIGHT && msg.what == RIGHT_DETECTED)
							|| (msg.what == MAKE_IT_THROUGH)) {

						newArrows.remove(0);
						ImageView image = arrowImageToDetect.remove(0);
						changeDetectedImage(image);
						image.postInvalidate();

						if (arrowImageToDetect.isEmpty()) {

							new Thread(new Runnable() {
								public void run() {
									try {
										Thread.sleep(200);
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									changeNewArrows(arrowNum);
								}
							}).start();
						}
					}

					if (msg.what == SHAKE) {
						Log.i(TAG, "SHAKE DETECTED");
						int tagIceState = (Integer) imageShake.getTag();
						if (tagIceState >= SET_ICE1 && tagIceState <= SET_ICE3) {
							tagIceState++;
							imageShake.setTag(tagIceState);
							changeIce(tagIceState);
						}

					}

				}
			};
		};

		// for studing detection algorithm
		// jumpdata = new TrickestPart_JumpDataImpl2(handler);

		jumpdata = new TrickestPart_JumpDataImpl(handler);
		// jumpdata.setToast(toast);//testing purpose

		imageFiller = new TrickestPart_ImageFiller(getApplicationContext());
		// this is the initial stage period time
		stagePeriodTime =5000;
		changeNewArrows(2);

		// // buttons for testing
		Button buttonLEFT = (Button) findViewById(R.id.buttonLEFT);
		Button buttonRIGHT = (Button) findViewById(R.id.buttonRIGHT);

		buttonLEFT.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				jumpdata.JumpLeftDetected();
			}
		});

		buttonRIGHT.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				jumpdata.JumpRightDetected();
			}
		});

		// buttonAROUND.setOnClickListener(new OnClickListener() {
		//
		// @Override
		// public void onClick(View v) {
		// // TODO Auto-generated method stub
		// jumpdata.JumpAroundDetected();
		// }
		// });

		changeStage(currentStage);
		generateRamdonIce();
	}

	// TODO Auto-generated method stub
	private void generateRamdonIce() {
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				while(true){
					if(!isStageThreadAllowed){
						return;
					}
					
					Random random = new Random();
					int randomInt = random.nextInt(1000);
					randomInt = randomInt % 9;
					
					randomInt = randomInt + 12;
					
					try {
						Thread.sleep(randomInt * 1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					
				}
			}
		}).start();
	}

	private void changeIce(final int setIceState) {
		// TODO Auto-generated method stub
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				switch (setIceState) {

				case SET_ICE2:
					imageShake.setImageDrawable(getResources().getDrawable(
							R.drawable.trickestpart_ice2));
					playMusicEffect(ICE_BREAK);
					break;
				
				case SET_ICE3:
					imageShake.setImageDrawable(getResources().getDrawable(
							R.drawable.trickestpart_ice3));
					playMusicEffect(ICE_BREAK);
					break;

				case SET_ICE4:
					imageShake.setVisibility(ImageView.INVISIBLE);
					playMusicEffect(ICE_BREAK);
//					textShake.setVisibility(View.VISIBLE);
					break;
				}
			}
		});	
	}
	
	private void playMusicEffect(int SITUATION){
		
		switch (SITUATION) {
 
		case ICE_BREAK:
			TrickestPart_Activity.this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
			iceBreak.setAudioStreamType(AudioManager.STREAM_MUSIC);
			iceBreak.setVolume(4, 4);
			iceBreak.start();
			break;
//		case :
//			break;
		}
	}
	
	private void changeStage(final int currentStageNow) {
		// TODO Auto-generated method stub

		Log.i(TAG, "current stage: " + currentStageNow);

		arrowNum = arrowNumArray[currentStageNow];
		stagePeriodTime = stagePeriodTimeArray[currentStageNow];
		stageWholeTime = stageWholeTimeArray[currentStageNow];

		new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub

				playStageMusic(currentStageNow);

				try {
					Thread.sleep(stageWholeTimeArray[currentStageNow]);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				while (isThreadPaused) {
					try {
						Thread.sleep(stageWholeTimeArray[currentStageNow]);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				if (!isStageThreadAllowed || currentStageNow == 4) {
					return;
				} else {
					changeStage(currentStageNow + 1);
					// stage switch animation
					changeStageTitle(currentStageNow + 1);
				}
			}

		}).start();

	}

	private void changeStageTitle(final int currentStageNow) {
		// TODO Auto-generated method stub
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				tvStage.setText("Stage " + currentStageNow);
				// int textSize = (int)tvStage.getTextSize();
				// tvStage.setTextSize(textSize+currentStageNow);
				switch (currentStageNow) {
				case 2:
					// ClipDrawable greenDraw2 = new ClipDrawable(new
					// ColorDrawable(
					// Color.parseColor("#ff33bb44")), Gravity.LEFT,
					// ClipDrawable.HORIZONTAL);
					// progressBar.setProgressDrawable(greenDraw2);
					tvStage.setTextColor(Color.parseColor("#ff55cc55"));
					break;

				case 3:
					// ClipDrawable greenDraw3 = new ClipDrawable(new
					// ColorDrawable(
					// Color.parseColor("#ff33bb33")), Gravity.LEFT,
					// ClipDrawable.HORIZONTAL);
					// progressBar.setProgressDrawable(greenDraw3);
					tvStage.setTextColor(Color.parseColor("#ff55bb55"));

					break;

				case 4:
					// ClipDrawable greenDraw4 = new ClipDrawable(new
					// ColorDrawable(
					// Color.parseColor("#ff33aa33")), Gravity.LEFT,
					// ClipDrawable.HORIZONTAL);
					// progressBar.setProgressDrawable(greenDraw4);
					tvStage.setTextColor(Color.parseColor("#ff44aa44"));

					break;
				}
			}
		});
	}

	private void playStageMusic(final int currentStageNow) {

		switch (currentStageNow) {
		case 1:
			backgroungMusic = MediaPlayer.create(TrickestPart_Activity.this,
					R.raw.trickestpart_stage_1);
			break;

		case 2:
			backgroungMusic.stop();
			backgroungMusic = MediaPlayer.create(TrickestPart_Activity.this,
					R.raw.trickestpart_stage_2);
			break;

		case 3:
			backgroungMusic.stop();
			backgroungMusic = MediaPlayer.create(TrickestPart_Activity.this,
					R.raw.trickestpart_stage_3);
			break;

		case 4:
			backgroungMusic.stop();
			backgroungMusic = MediaPlayer.create(TrickestPart_Activity.this,
					R.raw.trickestpart_stage_4);
			break;

		// case 5:
		// backgroungMusic.stop();
		// backgroungMusic = MediaPlayer.create(TrickestPart_Activity.this,
		// R.raw.trickestpart_stage_5);
		// break;
		//
		// case 6:
		// backgroungMusic.stop();
		// backgroungMusic = MediaPlayer.create(TrickestPart_Activity.this,
		// R.raw.trickestpart_stage_6);
		// break;
		}

		TrickestPart_Activity.this
				.setVolumeControlStream(AudioManager.STREAM_MUSIC);
		backgroungMusic.setAudioStreamType(AudioManager.STREAM_MUSIC);
		backgroungMusic.setVolume(2, 2);
		backgroungMusic.start();
		backgroungMusic.setLooping(true);

		if (!isStageThreadAllowed) {
			backgroungMusic.stop();
		}
	}

	// TODO Auto-generated method stub
	// 5000 whole progress -- 10000
	//
	private void rollProgressBar(final int wholeProgress,
			final int FromProgress, final int time) {
		progressBar.setProgress(FromProgress);
		new Thread(new Runnable() {

			@Override
			public void run() {
				float unit = (float) 6000 / 200;
				progressBar.setProgress(FromProgress);
				try {
					Thread.sleep((long) unit);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				progressBar.setProgress(FromProgress);
				try {
					Thread.sleep((long) unit);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				progressBar.setProgress(FromProgress);
				isSetProgressAllowed = true;

				// TODO Auto-generated method stub
				int progress = FromProgress;
				unit = (float) time / 200;
				while (progress >= 0) {
					
					if (!isSetProgressAllowed) {
						return;
					}

					if (isThreadPaused) {
						continue;
					}

					try {
						Thread.sleep((long) unit);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					if (!isSetProgressAllowed) {
						return;
					}

					progress = progressBar.getProgress();

					if (progress >= 1) {
						progress = (int) (progress - (float) wholeProgress / 200);
					}

					if (!isSetProgressAllowed) {
						return;
					}
					changeProgressBar(progress);
				}
			}

		}).start();
	}

	private void changeProgressBar(final int progress) {
		// TODO Auto-generated method stub
		runOnUiThread(new Runnable() {

			@Override
			public void run() {

				if (progress <= 0 && isSetProgressAllowed) {
					isSetProgressAllowed = false;
					Intent intent = new Intent(TrickestPart_Activity.this,
							TrickestPart_GameOverActivity.class);
					startActivity(intent);
					finish();
				}

				// TODO Auto-generated method stub
				if (isSetProgressAllowed) {
					progressBar.setProgress(progress);
				}
				Log.i(TAG, "progress: " + progress);
				Log.i(TAG, "isSetProgressAllowed: " + isSetProgressAllowed);
			}
		});
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		// Activity is on the front, register the sensor
		mSensorManager.registerListener(this, mSensor_acc,
				SensorManager.SENSOR_DELAY_FASTEST);
		mSensorManager.registerListener(this, mSensor_gravity,
				SensorManager.SENSOR_DELAY_FASTEST);
		mSensorManager.registerListener(this, mSensor_angle,
				SensorManager.SENSOR_DELAY_FASTEST);
		isThreadPaused = false;
		if(backgroungMusic != null){
			backgroungMusic.start();
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		// Activity is on the back, unregister the sensor
		mSensorManager.unregisterListener(this);

		String currentHighestStr = "" + tvHighestScore.getText();
		int currentHighest = Integer.parseInt(currentHighestStr);

		String currentScoreStr = "" + scoreTV.getText();
		int currentScore = Integer.parseInt(currentScoreStr);
		if (currentScore > currentHighest) {
			editor = sharedPreferences.edit();
			editor.putInt("highest", currentScore);
			editor.commit();
		}

		editor = sharedPreferences.edit();
		editor.putString("current", currentScoreStr);
		editor.commit();

		backgroungMusic.pause();
		isThreadPaused = true;
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		isStageThreadAllowed = false;
		isSetProgressAllowed = false;
		isThreadPaused = false;
		backgroungMusic.stop();
	}

	// this part will be changed for the final project
	float[] rotMat = new float[9];
	float[] vals = new float[3];

	private TextView tvHighestScore;

	private Editor editor;

	private ProgressBar progressBar;

	private MediaPlayer backgroungMusic;

	private TextView tvStage;

	private boolean isThreadPaused = false;

	@SuppressLint("NewApi")
	@Override
	public void onSensorChanged(SensorEvent event) {
		Sensor sensor = event.sensor;

		if (sensor.equals(mSensor_acc)) {
			jumpdata.saveAcc(event);
		} else if (sensor.equals(mSensor_gravity)) {
			jumpdata.saveGravity(event);
		} else if (sensor.equals(mSensor_angle)) {
			// jumpdata.saveAngle(event);
		}

	}

	private void changeNewArrows(final int arrowNum) {
		// TODO Auto-generated method stub
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				newArrows = TrickestPart_CreateArrows.getInstance()
						.getNewArrows(arrowNum);
				imageFiller.fillTheImage(newArrows, arrowLayout,
						arrowImageToDetect);
				isSetProgressAllowed = false;
				progressBar.setProgress(PROGRESS_MAX);
				rollProgressBar(PROGRESS_MAX, PROGRESS_MAX, stagePeriodTime);
			}
		});
	}

	private void changeDetectedImage(final ImageView imageView) {
		// TODO Auto-generated method stub
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				// Everytime I change the background color
				// the score must add up by one.
				String scoreStr = "" + scoreTV.getText();
				int score = Integer.parseInt(scoreStr);
				score++;
				String newScoreStr = "" + score;
				while (newScoreStr.length() < 3) {
					newScoreStr = "0" + newScoreStr;
				}
				scoreTV.setText(newScoreStr);
				imageView.setBackgroundColor(Color.parseColor("#bb88ee88"));

			}
		});
	}

	// Temporally, we do not have to override this part
	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
	}
}

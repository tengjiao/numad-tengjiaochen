package edu.neu.madcourse.tengjiaochen.trickestpart;

import edu.neu.madcourse.tengjiaochen.R;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class TrickestPart_GameOverActivity extends Activity {
	private TextView tvCurrentScore;
	private TextView tvHighestScore;
	protected SharedPreferences sharedPreferences;
	private Button buttonPlayAgain;
	
	@Override
	// TODO Auto-generated method stub
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.trickestpart_gameover_activity);
		
		tvHighestScore = (TextView)findViewById(R.id.trickestpart_highest_score);
		tvCurrentScore = (TextView)findViewById(R.id.trickestpart_current_score);
		buttonPlayAgain = (Button)findViewById(R.id.trickestpart_play_again);
		
		sharedPreferences = getApplicationContext().getSharedPreferences("Jump", MODE_PRIVATE);
		int highestScore = sharedPreferences.getInt("highest", 000);
		String currentScore = sharedPreferences.getString("current", "unavaible");
		String highestScoreStr = highestScore+"";
		for(int i=highestScoreStr.length();i<3;i++){
			highestScoreStr = "0"+highestScoreStr;
		}
		
		tvHighestScore.setText("Highest Score: "+highestScoreStr);
		tvCurrentScore.setText("Current Score: "+currentScore);
		
		buttonPlayAgain.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(TrickestPart_GameOverActivity.this, TrickestPart_Activity.class);
				startActivity(intent);
				finish();
			}
		});
	}
}

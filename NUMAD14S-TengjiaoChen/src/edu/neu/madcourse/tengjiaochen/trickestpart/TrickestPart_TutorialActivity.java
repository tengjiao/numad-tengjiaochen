package edu.neu.madcourse.tengjiaochen.trickestpart;

import edu.neu.madcourse.tengjiaochen.R;
import edu.neu.madcourse.tengjiaochen.trickestpart.data.TrickestPart_JumpData;
import edu.neu.madcourse.tengjiaochen.trickestpart.data.TrickestPart_JumpDataImpl;
import edu.neu.madcourse.tengjiaochen.trickestpart.data.TrickestPart_ShakeData;
import edu.neu.madcourse.tengjiaochen.trickestpart.data.TrickestPart_ShakeDataImpl;
import edu.neu.madcourse.tengjiaochen.trickestpart.view.TrickestPart_MyViewGroup;
import edu.neu.madcourse.tengjiaochen.trickestpart.view.TrickestPart_MyViewGroup.MyScrollListener;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class TrickestPart_TutorialActivity extends Activity implements SensorEventListener {

	protected static final String TAG = "TrickestPart_TutorialActivity";
	//  把所有的图片ID写下来
	private TrickestPart_MyViewGroup myViewGroup;
	
	private RadioGroup radioGroup;
	private LinearLayout bodyLayout;
	private Button ready;
	private Button continueButton;
	
	private int viewGroupExtend;
	private ImageView imageLeft;
	private TextView textLeft;
	private ImageView imageRight;
	private TextView textRight;
	private ImageView imageShake;
	private TextView textShake;
	
	private SensorManager mSensorManager;
	private Sensor mSensor_acc;
	private Sensor mSensor_gravity;
	private Sensor mSensor_angle;
	
	private int currentViewNumber = 0;
	
	private final int LEFT_DETECTED = TrickestPart_Activity.LEFT_DETECTED;
	private final int RIGHT_DETECTED = TrickestPart_Activity.RIGHT_DETECTED;
	private final int MAKE_IT_THROUGH = TrickestPart_Activity.MAKE_IT_THROUGH;
	private final int SHAKE = TrickestPart_Activity.SHAKE;

	private Handler handler;

	private TrickestPart_JumpData jumpdata;
	private static final int SET_ICE1 = 1;
	private static final int SET_ICE2 = 2;
	private static final int SET_ICE3 = 3;
	private static final int SET_ICE4 = 4;

	private static final int ICE_BREAK = 0;

	
	private int iceState = SET_ICE1;

	private MediaPlayer iceBreak;

	private TrickestPart_ShakeData shakeData;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.trickestpart_activity_tutorial);
		setTitle("Tutorial");
		myViewGroup = new TrickestPart_MyViewGroup(this);

        radioGroup =(RadioGroup)findViewById(R.id.newgame_radioGroup);
        
		// initiate the sensor
		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		mSensor_acc = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
		mSensor_gravity = mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
		mSensor_angle = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
        
        // add the radio group
        for (int i = 0; i < 4; i++) {
        	RadioButton radioButton = new RadioButton(this);
        	radioGroup.addView(radioButton);
        	
        	if(i==0){
        		radioButton.setChecked(true);
        	}
        	
        	// store the number
        	radioButton.setTag(i);
        	
        	radioButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					// TODO Auto-generated method stub
					if(isChecked){
						myViewGroup.moveToDest((Integer)buttonView.getTag());
						currentViewNumber = (Integer)buttonView.getTag();
//						showToastInMainThread("我是第" + (Integer)buttonView.getTag() +"个");
					}
				}
			});
		}
        
        handler = new Handler(){
			public void handleMessage(android.os.Message msg) {
				if(msg.what == LEFT_DETECTED || msg.what == RIGHT_DETECTED
						|| msg.what == MAKE_IT_THROUGH){
					
					if( (currentViewNumber == 0 && msg.what == LEFT_DETECTED)||	(msg.what == MAKE_IT_THROUGH) ){
						imageLeft.setBackgroundColor(Color.parseColor("#bb88ee88"));
						textLeft.setVisibility(View.VISIBLE);
					}
					
					if( (currentViewNumber == 1 && msg.what == RIGHT_DETECTED) || (msg.what == MAKE_IT_THROUGH)){
						imageRight.setBackgroundColor(Color.parseColor("#bb88ee88"));
						textRight.setVisibility(View.VISIBLE);
					}
					
				} else if( msg.what == SHAKE ){
					Log.i(TAG,"SHAKE DETECTED");
					if(currentViewNumber == 2){
						int tagIceState = (Integer)imageShake.getTag();
						if(tagIceState>=SET_ICE1 && tagIceState <= SET_ICE3){
							tagIceState++;
							imageShake.setTag(tagIceState);
							changeIce(tagIceState);
						}
						
					}
					
				}
			};
		};
        
		iceBreak = MediaPlayer.create(TrickestPart_TutorialActivity.this, R.raw.trickestpart_icesound);
		
		jumpdata = new TrickestPart_JumpDataImpl(handler);
		shakeData = new TrickestPart_ShakeDataImpl(handler);
        // put the "jump left" layout
        View viewLeft = getLayoutInflater().inflate(R.layout.trickestpart_view_left,null);
        myViewGroup.addView(viewLeft);
		
        // put the "jump right" layout
        View viewRight = getLayoutInflater().inflate(R.layout.trickestpart_view_right,null);
        myViewGroup.addView(viewRight);
        
        // put the "shaking" layout
        View viewShake = getLayoutInflater().inflate(R.layout.trickestpart_view_shake,null);
        myViewGroup.addView(viewShake);
        
        // put in the "Are you Ready" layout
        View viewReady = getLayoutInflater().inflate(R.layout.trickestpart_view_ready,null);
        myViewGroup.addView(viewReady);
        bodyLayout = (LinearLayout) findViewById(R.id.newgame_bodyLayout);
        bodyLayout.addView(myViewGroup);

        // left part
        imageLeft = (ImageView)findViewById(R.id.trickestpart_image_left);
        textLeft = (TextView)findViewById(R.id.trickestpart_find_left);
        
		int width = this.getResources().getDisplayMetrics().widthPixels / 3;
		int imageWidth = width;
		int imageHeight = width;
		imageLeft.setLayoutParams(new LayoutParams(imageWidth, imageHeight));
		imageLeft.setVisibility(View.VISIBLE);
        
		// right part
        imageRight = (ImageView)findViewById(R.id.trickestpart_image_right);
        textRight = (TextView)findViewById(R.id.trickestpart_find_right);
        imageRight.setLayoutParams(new LayoutParams(imageWidth, imageHeight));
        imageRight.setVisibility(View.VISIBLE);
		
        // shake part
        imageShake = (ImageView)findViewById(R.id.trickestpart_image_shake);
        textShake = (TextView)findViewById(R.id.trickestpart_find_shake);
        imageShake.setLayoutParams(new LayoutParams((int) (width*3*((float)7/8)), (int) (width*3*((float)5/6))));
        imageShake.setVisibility(View.VISIBLE);
        iceState = SET_ICE1;
        imageShake.setTag(iceState); // this is the original state
        
        ready = (Button) findViewById(R.id.newgame_readyButton);
        ready.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(TrickestPart_TutorialActivity.this, TrickestPart_Activity.class);
				startActivity(intent);
				finish();
			}
		});
 
        myViewGroup.setMyScrollListener(new MyScrollListener() {
			
			@Override
			public void moveToDest(int destId) {
				// TODO Auto-generated method stub
				( (RadioButton)radioGroup.getChildAt(destId) ).setChecked(true);
			}
		});
	}
	
	/**
	 * 在主线程显示土司
	 * @param text
	 */
	public void showToastInMainThread(final String text) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(getApplicationContext(), text, 1).show();
			}
		});
	}

	private void changeIce(final int setIceState) {
		// TODO Auto-generated method stub
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				switch (setIceState) {

				case SET_ICE2:
					imageShake.setImageDrawable(getResources().getDrawable(
							R.drawable.trickestpart_ice2));
					playMusicEffect(ICE_BREAK);
					break;
				
				case SET_ICE3:
					imageShake.setImageDrawable(getResources().getDrawable(
							R.drawable.trickestpart_ice3));
					playMusicEffect(ICE_BREAK);
					break;

				case SET_ICE4:
					imageShake.setVisibility(ImageView.INVISIBLE);
					playMusicEffect(ICE_BREAK);
					textShake.setVisibility(View.VISIBLE);
					break;
				}
			}
		});	
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		// Activity is on the front, register the sensor	
		mSensorManager.registerListener(this, mSensor_acc, SensorManager.SENSOR_DELAY_FASTEST);
		mSensorManager.registerListener(this, mSensor_gravity, SensorManager.SENSOR_DELAY_FASTEST);
		mSensorManager.registerListener(this, mSensor_angle, SensorManager.SENSOR_DELAY_FASTEST);

	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		// Activity is on the back, unregister the sensor
		mSensorManager.unregisterListener(this);	
	}
	
	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		// TODO Auto-generated method stub
		Sensor sensor = event.sensor;

		if(sensor.equals(mSensor_acc)){
			shakeData.saveAcc(event);
			jumpdata.saveAcc(event);
		}else if(sensor.equals(mSensor_gravity)){
			jumpdata.saveGravity(event);
		}
	}
	
	private void playMusicEffect(int SITUATION){
		
		switch (SITUATION) {
 
		case ICE_BREAK:
			TrickestPart_TutorialActivity.this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
			iceBreak.setAudioStreamType(AudioManager.STREAM_MUSIC);
			iceBreak.setVolume(4, 4);
			iceBreak.start();
			break;
//		case :
//			break;
		}
	}
}

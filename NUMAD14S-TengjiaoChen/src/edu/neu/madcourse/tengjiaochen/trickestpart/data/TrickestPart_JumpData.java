package edu.neu.madcourse.tengjiaochen.trickestpart.data;

import edu.neu.madcourse.tengjiaochen.trickestpart.TrickestPart_Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

public abstract class TrickestPart_JumpData {
	
	protected Handler handler;
	Toast toast;
	
	public TrickestPart_JumpData() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public TrickestPart_JumpData(Handler handler) {
		this.handler = handler;
	}

	/**
	 * 
	 * @param angle : angle detected now
	 * 
	 * TODO 
	 * save this angle to map with key(time)
	 * and value(angle). Once you save a datum, 
	 * you can call a method like dataAnalysis()
	 * to judge whether there is any kind of
	 * jump detected now		
	 */
	public abstract void saveAngle(float angle);
	
	/**
	 * 
	 * @param angle : accelerometer detected now
	 * 
	 * TODO 
	 * save this accelerometer to map with key(time)
	 * and value(angle). Once you save a datum, 
	 * you can call a method like dataAnalysis()
	 * to judge whether there is any kind of
	 * jump detected now		
	 */
	public abstract void saveAcc(SensorEvent event);
	public abstract void saveGravity(SensorEvent event);
	
	// After you have detected LEFT jumping
	// you should call this.JumpLeftDetected
	// for only one time
	public void JumpLeftDetected(){
		Message msg = new Message();
		msg.what = TrickestPart_Activity.LEFT_DETECTED;
		handler.sendMessage(msg);
	}
	
	// After you have detected LEFT jumping
	// you should call this.JumpRIGHTDetected
	// for only one time
	public void JumpRightDetected(){
		Message msg = new Message();
		msg.what = TrickestPart_Activity.RIGHT_DETECTED;
		handler.sendMessage(msg);
	}
	
	// After you have detected LEFT jumping
	// you should call this.JumpAroundDetected
	// for only one time
	public void MakeItThrough(){
		Message msg = new Message();
		msg.what = TrickestPart_Activity.MAKE_IT_THROUGH;
		handler.sendMessage(msg);
	}
	
	public void setToast(Toast toast){
		this.toast = toast;
	}
}

package edu.neu.madcourse.tengjiaochen.trickestpart.data;

import android.hardware.SensorEvent;
import android.os.Handler;
import android.util.Log;

// when direction is 11, it is possibly left 
public class TrickestPart_JumpDataImpl2 extends TrickestPart_JumpData {

	private final String TAG ="TrickestPart_JumpDataImpl2";
	private static final float ACC_SENSITIVITY = 0.1f;
	
	public static final int UP = 1;
	public static final int DOWN = 0;
	public static final int UNKNOWN = -1;
	public static final int LEFT = 2;
	public static final int RIGHT = 3;
	
	public TrickestPart_JumpDataImpl2(Handler handler) {
		super(handler);
	}
	private void showText(String string){
		if(toast != null){
			try {
				toast.setText(string);
				toast.show();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void saveAngle(float angle) {
		Log.i(TAG+"angle", "ANGLE:"+Math.toDegrees(angle));
	}
	
	//high pass filter
	private long prevTime = 0;
	private float prevAcc = 0;
	private boolean firstTime = true;
	private float prevPass = 0;
	private double alpha = 0;
	private double RC = 0.0000318;
	
	@Override
	public void saveAcc(SensorEvent event) {
		float x = event.values[0];
		float y = event.values[1];
		float z = event.values[2];
		//this recording for direction
		if(canRecordDirection() && Math.abs(x) > 0.2){
			x = (float)(x*Math.cos(getXAngle()) + y*Math.cos(getYAngle()) - z*Math.cos(getZAngle()));
			recordDirection(x);
		}
		//this recording for jump
		float accelerometer = (float) (z*(Math.sin(getZAngle())) + y*(Math.sin(getYAngle())));
		if(Math.abs(accelerometer) < ACC_SENSITIVITY){
			return;
		}
		//high pass filter
		if(firstTime){
			firstTime = false;
			prevTime = System.currentTimeMillis();
			prevPass = accelerometer;
			prevAcc = accelerometer;
		}else{
			long now = System.currentTimeMillis(); 
			long dt = now - prevTime;
			prevTime = now;
			alpha = RC / (RC + dt/1000);
			prevPass = (float) (prevPass * alpha + alpha * (accelerometer - prevAcc));
			prevAcc = accelerometer;
			accelerometer = prevPass;
		}
		//analysis jump
		analysisAcc(accelerometer);
	}
	private int UP_LEVEL_SUM = 40;//stage 1 threshold
	private int DOWN_LEVEL_SUM1 = -25;//stage 2 threshold
	private int DOWN_LEVEL_SUM0 = -25;//stage 2 threshold
	private int DOWN_LEVEL_SUM = -25;//stage 2 threshold default
	private int DOWN_INIT_LEVEL_SUM = -9;//stage 0 threshold
	private int patternStage = 0;//current stage
	private float prevSum = -1;//previous sum of the acc
	private float lastSum = 0; //last sum of the acc (current one)
	private int failedStage = 0; //which stage the pattern failed
	private boolean prevJumpDetect = false;//if previous jump detected
	
	private void analysisAcc(float lastRecordAcc){
		lastSum += lastRecordAcc;
		//Log.i(TAG+"sum", ""+lastSum);
		switch (patternStage) {
		case 0:
			//see if it is the beginning of the pattern recognition
			if(prevSum == -1){
				if(lastSum < 0){//user's phone goes down
					prevSum = lastSum;
					beginDirectionRecord();
				}else{
					resetPatterDetection();
				}
			}else{
				if(lastSum > prevSum){
					//user bends his leg, ready to jump
					if(prevSum <= DOWN_INIT_LEVEL_SUM){
						//possibly stage 1 sensor not accurate
						//this could be stage 3
						if(failedStage == 1 && prevSum <= DOWN_LEVEL_SUM0){
							resetPatterDetection();
							int direction = getTurnDirection();
							if(direction == LEFT){
								JumpLeftDetected();
							}else if(direction == RIGHT){
								JumpRightDetected();
							}
							beginDirectionRecord();
						}
						goStage(1);
					}else{
						showText("Stage 1:"+ prevSum);
						resetPatterDetection();
						failedStage = 0;
					}
				}else{
					prevSum = lastSum;
				}
			}
			break;
		case 1:
			if(prevSum == -1){
				prevSum = lastSum;
			}
			//user jump to the highest point
			if(lastSum < prevSum){
				if(prevSum >= UP_LEVEL_SUM){
					//adjust the threshold for stage 2 according to the current
					//phone status and jump strengh in this stage
					if(prevSum > 70 && getYAngle() < getZAngle()){
						DOWN_LEVEL_SUM = DOWN_LEVEL_SUM1;
					}else{
						DOWN_LEVEL_SUM = DOWN_LEVEL_SUM0;
					}
					goStage(2);
				}else{
					showText("stage2:" + prevSum);
					resetPatterDetection();
					failedStage = 1;
					prevJumpDetect = true;
				}
			}else{
				prevSum = lastSum;
			}
			break;
		case 2:
			if(prevSum == -1){
				prevSum = lastSum;
			}
			//user lands
			if(lastSum > prevSum){
				if(prevSum <= DOWN_LEVEL_SUM){
					prevJumpDetect = true;
					resetPatterDetection();
					//get the direction
					int direction = getTurnDirection();
					if(direction == LEFT){
						JumpLeftDetected();
					}else if(direction == RIGHT){
						JumpRightDetected();
					}
					goStage(1);//user might continue jumping
					beginDirectionRecord();
					return;
				}else{
					//continue jumping
					if(prevJumpDetect){
						//might be in the first stage
						if(prevSum >= DOWN_INIT_LEVEL_SUM){
							resetPatterDetection();
							beginDirectionRecord();
							goStage(1);
							return;
						}
					}
					showText("stage3:" + prevSum);
					resetPatterDetection();
					failedStage = 3;
					prevJumpDetect = false;
					return;
				}
			}else{
				prevSum = lastSum;
			}
			break;
		}
	}
	private void goStage(int stage){
		patternStage = stage;
		prevSum = -1;
		lastSum = 0;
	}
	private void resetPatterDetection(){
		patternStage = 0;
		prevSum = -1;
		lastSum = 0;
		stopDirectionRecord();
	}
	//acce x
	private float prevXX = 0;// prev prev sum of accelerometer x
	private float prevX = 0;// prev sum of accelerometer x
	private float sumX = 0;// current sum of accelerometer x
	private final int POTENTIAL_LEFT = 10;//maybe jump to left
	private final int POTENTIAL_RIGHT = 11;//maybe jump to right
	private int direction = UNKNOWN;//dont know the direction
	private final float DOWN_LEVEL = -15;//left level threshold
	private final float UP_LEVEL = 15;//right level threshold
	private boolean canRecordingDirection = false;
	private void recordDirection(float x){
		sumX += x;
		Log.i(TAG+"sum", ""+sumX);
		//if left level
		if(sumX < DOWN_LEVEL){
			if(direction == UNKNOWN){
				direction = POTENTIAL_LEFT;
			}
			if(direction == POTENTIAL_RIGHT){
				resetDirectionRecord();
				direction = RIGHT;
			}
		}else if(sumX > UP_LEVEL){
			if(direction == UNKNOWN){
				direction = POTENTIAL_RIGHT;
			}
			if(direction == POTENTIAL_LEFT){
				resetDirectionRecord();
				direction = LEFT;
			}
		}
		if(sumX > prevX && prevX < prevXX){
			resetDirectionRecord();
		} else if(sumX < prevX && prevX > prevXX){
			resetDirectionRecord();
		}else{
			prevXX = prevX;
			prevX = sumX;
		}
	}
	private boolean canRecordDirection(){
		return canRecordingDirection;
	}
	private void beginDirectionRecord(){
		direction = UNKNOWN;
		resetDirectionRecord();
		canRecordingDirection = true;
	}
	private void stopDirectionRecord(){
		canRecordingDirection = false;
	}
	private void resetDirectionRecord(){
		sumX = 0;
		prevX = 0;
		prevXX = 0;
	}
	private int getTurnDirection(){
		switch (direction) {
		case LEFT:
			showText("Direction: LEFT" );
			break;

		case RIGHT:
			showText("Direction: RIGHT" );
			break;
			
		case POTENTIAL_LEFT:
			showText("Direction: POTENTIAL_LEFT" );
			break;
			
		case POTENTIAL_RIGHT:
			showText("Direction: POTENTIAL_RIGHT" );
			break;
		}
		if(direction == POTENTIAL_LEFT){
			direction = LEFT;
		}
		if(direction == POTENTIAL_RIGHT){
			direction = RIGHT;
		}
		return direction;
	}
	
	private float[] gravity = {0,0,0};
	private float GRAVITY = 9.81f;
	@Override
	public void saveGravity(SensorEvent event){
		final float alpha = 0.8f;
		// Isolate the force of gravity with the low-pass filter.
		gravity[0] = alpha * gravity[0] + (1 - alpha) * event.values[0];
		gravity[1] = alpha * gravity[1] + (1 - alpha) * event.values[1];
		gravity[2] = alpha * gravity[2] + (1 - alpha) * event.values[2];
	}
	private float getZAngle(){
		return (float) Math.asin(gravity[2] / GRAVITY);
	}
	private float getYAngle(){
		return (float) Math.asin(gravity[1] / GRAVITY);
	}
	private float getXAngle(){
		return (float) Math.asin(gravity[0] / GRAVITY);
	}
}

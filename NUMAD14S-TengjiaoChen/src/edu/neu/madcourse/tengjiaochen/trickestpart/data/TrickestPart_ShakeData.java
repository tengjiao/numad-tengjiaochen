package edu.neu.madcourse.tengjiaochen.trickestpart.data;

import edu.neu.madcourse.tengjiaochen.trickestpart.TrickestPart_Activity;
import android.hardware.SensorEvent;
import android.os.Handler;
import android.os.Message;

public abstract class TrickestPart_ShakeData {
	protected Handler handler;

	public TrickestPart_ShakeData() {
		super();
	}

	public TrickestPart_ShakeData(Handler handler) {
		super();
		this.handler = handler;
	}

	public abstract void saveAcc(SensorEvent event);
	
	public void ShakeDetected(){
		Message msg = new Message();
		msg.what = TrickestPart_Activity.SHAKE;
		handler.sendMessage(msg);
	}
	
}

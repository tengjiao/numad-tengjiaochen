package edu.neu.madcourse.tengjiaochen.trickestpart.data;

import android.hardware.SensorEvent;
import android.os.Handler;
import android.util.Log;

public class TrickestPart_ShakeDataImpl extends TrickestPart_ShakeData{

	private static final String TAG = "TrickestPart_ShakeDataImpl";
	private int times = 0;
	
	public TrickestPart_ShakeDataImpl() {
		super();
	}

	public TrickestPart_ShakeDataImpl(Handler handler) {
		super(handler);
	}



	@Override
	public void saveAcc(SensorEvent event) {
		// TODO Auto-generated method stub
		float x = event.values[0];
		Log.i(TAG,"accX: "+x);
		if(Math.abs(x) > 9){
			times++;
			if(times >= 15){
				Log.i(TAG,"ShakeDetected " );
				ShakeDetected();
				times=0;
			}
		}
	}

}

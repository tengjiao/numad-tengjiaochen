package edu.neu.madcourse.tengjiaochen.trickestpart.data;

import android.hardware.SensorEvent;

public class Trickestpart_JumpDataImpl3 extends TrickestPart_JumpData  {

	@Override
	public void saveAngle(float angle) {}

	@Override
	public void saveAcc(SensorEvent event) {
		// TODO Auto-generated method stub
 
	}

	private float[] gravity = {0,0,0};
	private float GRAVITY = 9.81f;
	
	@Override
	public void saveGravity(SensorEvent event){
		final float alpha = 0.8f;
		// Isolate the force of gravity with the low-pass filter.
		gravity[0] = alpha * gravity[0] + (1 - alpha) * event.values[0];
		gravity[1] = alpha * gravity[1] + (1 - alpha) * event.values[1];
		gravity[2] = alpha * gravity[2] + (1 - alpha) * event.values[2];
	}
	private float getZAngle(){
		return (float) Math.asin(gravity[2] / GRAVITY);
	}
	private float getYAngle(){
		return (float) Math.asin(gravity[1] / GRAVITY);
	}
	private float getXAngle(){
		return (float) Math.asin(gravity[0] / GRAVITY);
	}
	
}

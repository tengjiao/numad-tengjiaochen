package edu.neu.madcourse.tengjiaochen.trickestpart.util;

import java.util.ArrayList;
import java.util.List;

import android.util.Log;

public class CyclicQueue<T> {
	private int startIndex = 0;
	private int endIndex = 0;
	private int size = 0;
	private static final String TAG = "CyclicQueue";
	
	private List<T> queue;
	
	public CyclicQueue(int size){
		this.size = size;
		queue = new ArrayList<T>();
		for (int i = 0; i < size; i++) {
			queue.add(null);
		}
	}
	public void addObject(T object){
		if(queue.size() <= endIndex) return;
		queue.set(endIndex, object);
		endIndex++;
		if(endIndex == size) endIndex = 0;
		if(endIndex == startIndex) startIndex++;
		if(startIndex == size) startIndex = 0;
	}
	public void clear(){
		startIndex = 0;
		endIndex = 0;
		for (int i = 0; i < size; i++) {
			queue.set(i, null);
		}
	}
	public int getStartIndex(){
		return startIndex;
	}
	public int getSize(){
		return size;
	}
	public int getEndIndex(){
		return endIndex;
	}
	public int getNextIndex(int currentIndex){
		currentIndex++;
		return currentIndex%size;
	}
	public T get(int index){
		return queue.get(index);
	}
}

package edu.neu.madcourse.tengjiaochen.trickestpart.util;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import edu.neu.madcourse.tengjiaochen.trickestpart.model.TrickestPart_Direction;

public class TrickestPart_CreateArrows {

	private List<TrickestPart_Direction> directionList;
	
	private static TrickestPart_CreateArrows createArrows ;
	
	private TrickestPart_CreateArrows(){};
	
	public static TrickestPart_CreateArrows getInstance() {
		// TODO Auto-generated method stub
		if(createArrows == null){
			createArrows = new TrickestPart_CreateArrows();
		}
		
		return createArrows;
	}

	public List<TrickestPart_Direction> getNewArrows(int number) {
		
		if(directionList == null){
			directionList = new ArrayList<TrickestPart_Direction>();
		} else{
			directionList.clear();
		}
		
		Random random = new Random();
		
		int left = 0;
		int right = 0;
		
		for(int i = 0 ; i < number ; i++){
			int nextInt = random.nextInt(2) + 1 ;
			
			switch (nextInt) {
			case 1:
				left++;
				if( left > (1+number)/2 ){
					i--;
					continue;
				}
				
				directionList.add(TrickestPart_Direction.LEFT);
				break;

			case 2:
				right++;
				if( right > (1+number)/2 ){
					i--;
					continue;
				}
				directionList.add(TrickestPart_Direction.RIGHT);
				break;
			
			case 3:
				directionList.add(TrickestPart_Direction.AROUND);
				break;	
				
			}
		}
		
		return directionList;
	}

}

package edu.neu.madcourse.tengjiaochen.trickestpart.util;

import java.util.ArrayList;
import java.util.List;
import edu.neu.madcourse.tengjiaochen.trickestpart.model.TrickestPart_Direction;
import android.content.Context;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

public class TrickestPart_ImageFiller {
	private Context context;
	
	public TrickestPart_ImageFiller(Context context) {
		super();
		this.context = context;
	}

	public void fillTheImage(List<TrickestPart_Direction> newArrows, LinearLayout arrowLayout, List<ImageView> arrowImageToDetect) {
		// TODO Auto-generated method stub
		arrowLayout.removeAllViews();
		arrowLayout.invalidate();
		
		if(arrowImageToDetect == null){
			arrowImageToDetect = new ArrayList<ImageView>();
		}else {
			arrowImageToDetect.clear();
		}
		
		for(TrickestPart_Direction direction : newArrows){
			ImageView imageView = new ImageView(context);
			int width = context.getResources().getDisplayMetrics().widthPixels / (newArrows.size()+1);
			int imageWidth = width;
			int imageHeight = width;
			imageView.setLayoutParams(new LayoutParams(imageWidth, imageHeight));
			
			int resId ;
			
			if(direction == TrickestPart_Direction.LEFT){
				
				resId = context.getResources().getIdentifier(
						"trickestpart_jump_left", "drawable",
						context.getPackageName());;
				
			} else if(direction == TrickestPart_Direction.RIGHT){
				
				resId = context.getResources().getIdentifier(
						"trickestpart_jump_right", "drawable",
						context.getPackageName());;
				
			} else { //if(direction == TrickestPart_Direction.AROUND)
				
				resId = context.getResources().getIdentifier(
						"trickestpart_jump_around", "drawable",
						context.getPackageName());;
			}
			
			imageView.setTag(resId);
			imageView.setImageResource(resId);
			arrowImageToDetect.add(imageView);
			arrowLayout.addView(imageView);
			
		}
		
	}
}

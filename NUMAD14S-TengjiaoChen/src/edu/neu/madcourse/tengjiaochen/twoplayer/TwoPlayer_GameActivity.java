package edu.neu.madcourse.tengjiaochen.twoplayer;


import java.util.Arrays;
import java.util.HashSet;
import java.util.regex.Pattern;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.AlertDialog.Builder;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import edu.neu.madcourse.tengjiaochen.R;
import edu.neu.madcourse.tengjiaochen.communicate.bean.Communicate_MessageContainer;
import edu.neu.madcourse.tengjiaochen.communicate.bean.Communicate_ServiceState;
import edu.neu.madcourse.tengjiaochen.communicate.util.Communicate_NetUtils;
import edu.neu.madcourse.tengjiaochen.newgame.service.WordWithBlanks;
import edu.neu.madcourse.tengjiaochen.newgame.util.GenerateRamdomLetters;
import edu.neu.madcourse.tengjiaochen.twoplayer.view.NewGame_FillTheWordBar;
import edu.neu.madcourse.tengjiaochen.newgame.view.NewGame_LetterGridAdapter;
import edu.neu.madcourse.tengjiaochen.twoplayer.service.TwoPlayer_SnoopNewMessageService;
import edu.neu.madcourse.tengjiaochen.twoplayer.service.TwoPlayer_WatchDogService;

public class TwoPlayer_GameActivity extends Activity {
	
	// state for handler
	public static final int TIME_GOING_DOWN = 0;
	public static final int WORD_IS_RIGHT = 1;
	public static final int WORD_IS_WRONG = 2;
	public static final int WORD_TO_FILL = 3;
	public static final int WORD_TITLE_BACK = 4;
	public static final int RECEIVED_NEW_MESSAGE = 5;
	public static final int INTERNET_FAILS = 6;
	public static final int INTERNET_OK = 7;
	
	// state for music effect
	private static final int RIGHT_WORD = 100;
	private static final int ICE_BREAK = 200;
	protected static final String TAG = "TwoPlayer_GameActivity";
	
	private String TWOPLAYER_CLASS_NAME = "TwoPlayer_GameActivity";
	
	protected MediaPlayer backgroungMusic ;
	protected MediaPlayer rightWord ;
	protected ImageView backmusicButton;
	protected ImageView coverGameContent;
	protected TextView timeLeft;
	protected TextView headerText;
	protected TextView newgameScore;
	protected static TextView p2Score;
	protected static TextView p2State;
	protected GridView gv_home;
	protected LinearLayout wordBar;
	protected LinearLayout gameWholeLayout;
	protected MyReceiverNetFail receiverFail;
	protected MyReceiverNetOK receiverOK;
	
	protected SensorManager mSensorManager;
	protected Sensor mSensor_linear;
	
	protected Button hintButton;
	protected Button nextButton;
	protected Button quitButton;
	protected OnClickListener nextClickListener;
	
	protected SharedPreferences sharedPreferences;
	protected Editor editor;
	
	protected int timeLeftNow = 120;
	protected int timetoNotify = 120;
	
	protected int letterNumber = 35;
	protected int hintNumber = 3;
	//every time wordsSeen is multiple of 5
	// the background will change
	protected int wordsNumber = 0;
	
	protected int hintNow = -1;
	
	protected boolean isPlaying = true;
	protected boolean gamePause = false;
	protected boolean appOnPause = false;
	protected boolean isAllowedRunning = true;
	protected boolean isInternetOK = true;
	
	// this is the handler to handle different message
	public static Handler handler;
	private Intent snoopNewMessage;
	private ImageView iceImage; 
	
	private MyListener mSensor_listener;
	
	public static String getScoreAndAction(){
		return p2Score.getText()+","+p2State.getText();
	}
	
	@SuppressLint("InlinedApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// allow new thread running
		timetoNotify = timeLeftNow - 15;
		isAllowedRunning = true;
		isInternetOK = true;
		mSensor_listener = new MyListener();
		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE); 
		mSensor_linear = mSensorManager
				.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
		mSensorManager.registerListener(mSensor_listener, mSensor_linear,
				SensorManager.SENSOR_DELAY_GAME);
		
		setContentView(R.layout.twoplayer_activity_game);
		
		// register two receivers
		receiverFail = new MyReceiverNetFail();   
        IntentFilter filterFail = new IntentFilter("edu.neu.mascourse.tengjiaochen.COMMUNICATION_FAILS");   
        registerReceiver(receiverFail, filterFail);
		
        receiverOK = new MyReceiverNetOK();   
        IntentFilter filterOK = new IntentFilter("edu.neu.mascourse.tengjiaochen.COMMUNICATION_OK");   
        registerReceiver(receiverOK, filterOK);
		
		snoopNewMessage = new Intent(TwoPlayer_GameActivity.this, TwoPlayer_SnoopNewMessageService.class);
		startService( snoopNewMessage );
		TwoPlayer_SnoopNewMessageService.setIsAllowedRunning(true);
		
		// this is the p2 score and state
		p2Score = (TextView) findViewById(R.id.twoplayer_p2_score);
		p2State = (TextView) findViewById(R.id.twoplayer_p2_state);
		
		// next job is to get these states back
		sharedPreferences = getApplicationContext().getSharedPreferences("2PGameState", MODE_PRIVATE);
		boolean hasGameEnded = sharedPreferences.getBoolean("hasGameEnded", true);
		
		if(!hasGameEnded){
			wordsNumber = sharedPreferences.getInt("wordsNumber",wordsNumber);
		}
		
		backgroungMusic = MediaPlayer.create(TwoPlayer_GameActivity.this, R.raw.newgame_back_ground_music);
		rightWord = MediaPlayer.create(TwoPlayer_GameActivity.this, R.raw.newgame_right_word);
		iceBreak = MediaPlayer.create(TwoPlayer_GameActivity.this, R.raw.twoplayer_icesound);
		playBackgroundMusic(isPlaying);
		
		// this part is to delete the words history
		// found last time
		if(hasGameEnded){
			WordWithBlanks.getWordsFound().clear();
			WordWithBlanks.getWordsNotFound().clear();
		} else {
			int size = sharedPreferences.getInt("wordsFoundNum", WordWithBlanks.getWordsFound().size());
			HashSet<String> wordsFound = new HashSet<String>();
			
			for(int i=0 ; i<size ; i++){
				String string = sharedPreferences.getString("wordsFound" +i, "");
				wordsFound.add(string);
			}
			
			// words not found
			size = sharedPreferences.getInt("wordsNotFoundNum", WordWithBlanks.getWordsNotFound().size());
			HashSet<String> wordsNotFound = new HashSet<String>();
			
			for(int i=0 ; i<size ; i++){
				String string = sharedPreferences.getString("wordsNotFound" +i, "");
				wordsNotFound.add(string);
			}

			WordWithBlanks.getWordsFound().addAll(wordsFound);
			WordWithBlanks.getWordsNotFound().addAll(wordsNotFound);
		}
		
		// this part is for the letters below
		gv_home = (GridView) findViewById(R.id.twoplayer_gv_home);
		if(hasGameEnded){
			gv_home.setAdapter(new NewGame_LetterGridAdapter(this,GenerateRamdomLetters.generate(letterNumber)));
		} else {
			String lettersInGridStr = sharedPreferences.getString("lettersInGrid","");
			char[] lettersInGrid = lettersInGridStr.toCharArray();
			
			String inVisibleInGrid = sharedPreferences.getString("inVisibleInGrid","");
			String[] split = inVisibleInGrid.split("-");
			boolean[] isInVisible = new boolean[lettersInGrid.length];
			for(int i=0 ; i<split.length ; i++){
				if(!split[i].equals("")){
					int splitInt = Integer.parseInt(split[i]);
					isInVisible[splitInt] = true;
				}
			}
			
			gv_home.setAdapter( new NewGame_LetterGridAdapter(this,lettersInGrid, isInVisible) );
		}

		// this part is the word on the front
//		Log.i("我是oncreate", "我在执行，哈哈哈");
		headerText = (TextView) findViewById(R.id.twoplayer_HeaderText);
		wordBar = (LinearLayout) findViewById(R.id.twoplayer_wordBar);
		
		if(hasGameEnded){
			NewGame_FillTheWordBar.getSingleFillTheWordBar(this,handler).fillLinearLayout(wordBar);
		} else {
			String originalWord = sharedPreferences.getString("originalWord", WordWithBlanks.getOriginalWord());
			String wordWithBlank = sharedPreferences.getString("WordWithBlank", WordWithBlanks.getWordWithBlank());
			WordWithBlanks.setOriginalWord(originalWord);
			WordWithBlanks.setWordWithBlank(wordWithBlank);
			WordWithBlanks.disableGenerateNext();
			NewGame_FillTheWordBar.getSingleFillTheWordBar(this,handler).fillLinearLayout(wordBar);
			WordWithBlanks.enableGenerateNext();
		}
		
		// this part is for the timer
		timeLeft = (TextView) findViewById(R.id.twoplayer_timeLeft);
		timeGoingDown();
		if(!hasGameEnded){
			timeLeftNow = sharedPreferences.getInt("timeLeft", timeLeftNow);
			timeLeft.setText(timeLeftNow+"");
		}
		
		// this part is for the game score
		newgameScore = (TextView) findViewById(R.id.twoplayer_p1_score);
		if(!hasGameEnded){
			String newgameScoreStr = sharedPreferences.getString("newgameScore","");
			newgameScore.setText(newgameScoreStr);
		}
		
		iceImage = (ImageView) findViewById(R.id.twoplayer_ice);

		// this button is to pause the game
		
		// this is to get the whole view of the game below
		// and I will block this view
		gameWholeLayout = (LinearLayout) findViewById(R.id.twoplayer_game_wholeView);
		
		coverGameContent = (ImageView) findViewById(R.id.twoplayer_cover_game_content);
		
		OnClickListener pauseOnClickListener = new OnClickListener() {
			
			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub
				ImageView pauseButton = (ImageView) view;
				if(gamePause == false){
					gamePause = true;
					
					// make the game pause
					pauseButton.setImageResource(getID("newgame_game_play"));
					playBackgroundMusic(false);
					
					coverGameContent.setVisibility(View.VISIBLE);
					coverGameContent.bringToFront();
//					gameWholeLayout.setClickable(true);
						
//					for ( int i = 0; i < gameWholeLayout.getChildCount();  i++ ){
//					    View childView = gameWholeLayout.getChildAt(i);
//					    childView.setClickable(false); // Or whatever you want to do with the view.
//					 }
				} else if(gamePause == true){
					gamePause = false;

					// make the game play
					pauseButton.setImageResource(getID("newgame_game_pause"));
					if(isPlaying){
						playBackgroundMusic(true);
					}
					
					coverGameContent.setVisibility(View.INVISIBLE);
					
//					gameWholeLayout.setClickable(false);
				}
			}
			
		};
		
		// this button is to show hint
		Button hintButton = (Button) findViewById(R.id.twoplayer_HintButton);
		if(!hasGameEnded){
			hintNumber = sharedPreferences.getInt("hintNumber",hintNumber);
			if(hintNumber == 0){
				hintButton.setTextColor(Color.parseColor("#ffAAAAAA"));
			}
		}
		hintButton.setText("Hint ("+hintNumber+")");
		hintButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Button hintButton = (Button)v;
				if(hintNumber>0 && hintNow != wordsNumber){
					hintNow = wordsNumber;
					changeTheScore(-1);
					headerText.setTextColor(Color.parseColor("#ffff8800"));
					headerText.setText(WordWithBlanks.getOriginalWord());
					hintNumber--;
					hintButton.setText("Hint ("+hintNumber+")");
					if(hintNumber == 0){
						hintButton.setTextColor(Color.parseColor("#ffAAAAAA"));
					}
				} 
				else if(hintNumber<=0){
					headerText.setTextColor(Color.parseColor("#ffff8800"));
					headerText.setText("No More Hints ~");
					waitThenChangeTitle(WORD_TITLE_BACK);
				}
			}
		});
		
		// this button is to generate next word
		nextButton = (Button)findViewById(R.id.twoplayer_NextButton);
		
		nextClickListener = new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				wordsNumber++;
				if(wordsNumber % 5 == 0){
					gv_home.setAdapter(new NewGame_LetterGridAdapter(TwoPlayer_GameActivity.this,GenerateRamdomLetters.generate(letterNumber)));
				}
				
				if(headerText.getText().equals("Right Answer")){
					Log.i("NewGame_GameActivity", "nextOnClick: I am in right answer block");
					waitThenChangeTitle(WORD_TO_FILL);
				} else{
					WordWithBlanks.getWordsNotFound().add( WordWithBlanks.getOriginalWord() );
					Message msg = Message.obtain();
					msg.what = WORD_TO_FILL;
					handler.sendMessage(msg);
				}
			}

		};
		
		nextButton.setOnClickListener(nextClickListener);
		
		// this part is the background music part
		backmusicButton = (ImageView) findViewById(R.id.twoplayer_playMusicButton);
		
		OnClickListener backmusicOnClickListener = new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (isPlaying) {
					
					isPlaying = false;
					playBackgroundMusic(isPlaying);
					TwoPlayer_GameActivity.this.backmusicButton
							.setImageDrawable(getResources().getDrawable(
									R.drawable.newgame_play_mute));
				} else if(!isPlaying){
					
					isPlaying = true;
					playBackgroundMusic(isPlaying);
					TwoPlayer_GameActivity.this.backmusicButton
					.setImageDrawable(getResources().getDrawable(
							R.drawable.newgame_play_music));
				}
			}
		};
		
		backmusicButton.setOnClickListener(backmusicOnClickListener);
		
		if(!hasGameEnded){
			isPlaying = sharedPreferences.getBoolean("isMusicPlaying", false);
			if(!isPlaying){
				isPlaying = true;
				backmusicOnClickListener.onClick(backmusicButton);
			}
		}

		// this is the quit button part
		quitButton = (Button)findViewById(R.id.twoplayer_quitButton);
		quitButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				editor = sharedPreferences.edit();
				editor.putBoolean("hasGameEnded", true);
				editor.commit();
//				timeLeftNow =0;
				Log.i("Now the hasGameEndedis",sharedPreferences.getBoolean("hasGameEnded", false)+"");
				backgroungMusic.stop();
				
				dialog();
				
//				TwoPlayer_GameActivity.this.onDestroy();
			}


		});
		
		handler = new Handler() {
			public void handleMessage(android.os.Message msg) {
				if(msg.what == TIME_GOING_DOWN){
					if(!gamePause && !appOnPause){
						timeLeftNow = timeLeftNow - 1;
						changeTheTimeLeft();
					}
				} else if(msg.what == WORD_IS_RIGHT || msg.what == WORD_IS_WRONG || msg.what == WORD_TO_FILL|| msg.what == WORD_TITLE_BACK){
					Log.i("NewGame_GameActivity", "Handler: I get message" + msg.what);
					changeHeaderText(msg.what);
				} else if(msg.what == RECEIVED_NEW_MESSAGE){
					String scoreAndAction = (String)msg.obj;
					String[] split = scoreAndAction.split(",");
					Log.i("TwoPlayer_GameActivity", "scoreAndAction : " + scoreAndAction);
					if(split.length == 3){
						String score = split[0];
						String action = split[1];
						Log.i("TwoPlayer_GameActivity", "scoreAndAction : " + score+ " , "+action+ " , time: "+split[2]);
						changeScoreAndAction(score,action);
					} else if(split.length == 2) {
						String score = split[0];
						Log.i("TwoPlayer_GameActivity", "score : " + score);
						changeScoreAndAction(score,"action");
					}
				} else if(msg.what == INTERNET_FAILS || msg.what == INTERNET_OK){
					showToastOnUI(msg.what);
				}
			}




		};
		
		new Thread( new Runnable() {
			@Override
			public void run() {
				while(true){
					if(!isAllowedRunning){
						return;
					}
					
					String imei = Communicate_MessageContainer.getMessageContainer().getIMEI();
					String messageSent = newgameScore.getText()+",";
					
					if ("Right Answer".equals(headerText.getText()) ) {
						messageSent = messageSent + "right";
						timetoNotify = timeLeftNow -15;
					} else if ("Wrong Answer".equals(headerText.getText()) ) {
						messageSent = messageSent + "wrong";
					} else {
						messageSent = messageSent + "action";
					} 
					messageSent = messageSent +","+ timeLeftNow;
					Communicate_NetUtils.setKeyValue( imei + "gamestate", messageSent);
//					Log.i("TwoPlayer_GameActivity", imei + "gamestate");
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		
		}).start();
		
		// this is for the high score list
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				String highscoreList = "";
				int attempt = 0;
				while(true){
					attempt++;
					Communicate_NetUtils.getValue("twoplayerhighscore");
					// wait for 2 second
					// 2s is enough to get this list
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					highscoreList = Communicate_MessageContainer.getMessageContainer().getHighScoreList() ;
					
					if(!"ERROR: IOException".equals(highscoreList)){
						break;
					}
					
					if(attempt == 5){
						Communicate_MessageContainer.getMessageContainer().setHighScoretop5(null);
						return;
					}
				}
				
				Log.i("TwoPlayer_GameActivity","highscoreList: " + highscoreList);
				
				String[] split = highscoreList.split(",");
				int[] highScoretop5 = new int[5];
				for(int i=0 ; i<split.length ; i++){
					
					if("".equals(split[i])){
						highScoretop5[i] = 0;
					}
					
					else if(Pattern.compile("[-]*[0-9]+").matcher(split[i]).matches()){
						highScoretop5[i] = Integer.parseInt(split[i]);
					}
				}
				
				Arrays.sort(highScoretop5);
				
				Communicate_MessageContainer.getMessageContainer().setHighScoretop5(highScoretop5);
				
			}

		}).start();
	}
	
	private void dialog() {
		// TODO Auto-generated method stub
		AlertDialog.Builder builder = new Builder(TwoPlayer_GameActivity.this);
		builder.setMessage("Are you sure to leave?\nIf you leave, the game will disconnect.");
		builder.setTitle("Hint");
		builder.setPositiveButton("YES",
		new android.content.DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				//AccoutList.this.finish();
				//System.exit(1);
//				android.os.Process.killProcess(android.os.Process.myPid());
				TwoPlayer_GameActivity.this.finish();
			}
		});
		builder.setNegativeButton("NO",
		new android.content.DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		builder.create().show();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		// if snoopNewMessageSerive is alive
		// I have to kill it
		boolean isSnoopNewMessageServiceAlive = Communicate_ServiceState.getServiceState().getIsSnoopNewMessageServiceAlive();
		if(isSnoopNewMessageServiceAlive){
			// set the tag the disallows this service restart
			Communicate_ServiceState.getServiceState().setSnoopNewMessageServiceAllowedRestart(false);
			// stop the SnoopNewMessageService
			Intent intent = new Intent(TwoPlayer_GameActivity.this, TwoPlayer_SnoopNewMessageService.class);
			stopService(intent);
			
		}

		// if snoopNewMessageSerive is alive
		// I have to kill it
		boolean isWatchDogServiceAlive = Communicate_ServiceState.getServiceState().getIsWatchDogServiceAlive();
		if(isWatchDogServiceAlive){
			// set the tag the disallows this service restart
			Communicate_ServiceState.getServiceState().setWatchDogServiceAllowedRestart(false);
			// stop the WatchDogService
			Intent intent = new Intent(TwoPlayer_GameActivity.this, TwoPlayer_WatchDogService.class);
			stopService(intent);

		}
		
		WordWithBlanks.setTotalScore(newgameScore.getText()+"");
		
		unregisterReceiver(receiverFail);
		unregisterReceiver(receiverOK);
		
		isAllowedRunning = false;
		mSensorManager.unregisterListener(mSensor_listener);
		NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		manager.cancelAll();
		finish();
	};
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
			dialog();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
//		appOnPause = false;
		TwoPlayer_SnoopNewMessageService.setAppOnFront(true);
		if(isPlaying && !gamePause){
			playBackgroundMusic(true);
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
//		appOnPause = true;
		TwoPlayer_SnoopNewMessageService.setAppOnFront(false);
		playBackgroundMusic(false);
		editor = sharedPreferences.edit();
		if(timeLeftNow>0){
			editor.putBoolean("hasGameEnded", false);
			
			// time left
			editor.putInt("timeLeft", timeLeftNow);
			
			// board word
			editor.putString("originalWord", WordWithBlanks.getOriginalWord());
			editor.putString("WordWithBlank", WordWithBlanks.getWordWithBlank());
			
			// hint number
			editor.putInt("hintNumber",hintNumber);
			
			// wordsNumber
			editor.putInt("wordsNumber",wordsNumber);
			
			// score
			editor.putString("newgameScore",newgameScore.getText().toString());
					
			// grid letter
			boolean[] isInVisible = NewGame_LetterGridAdapter.getLetterGridAdapter().getIsInVisible();
			HashSet<Integer> nowInBlank = NewGame_FillTheWordBar.getSingleFillTheWordBar().getNowInBlank();
			String inVisibleNum="";
			for(int i=0 ; i<isInVisible.length ; i++){
				if(isInVisible[i] == true && !nowInBlank.contains(i)){
					if(inVisibleNum.equals("")){
						inVisibleNum += i; 
					}else{
						inVisibleNum = inVisibleNum + "-" + i; 
					}
				}
			}
			
			editor.putString("inVisibleInGrid",inVisibleNum);
			
			char[] lettersInGrid = NewGame_LetterGridAdapter.getLetterGridAdapter().getLettersInGrid();
//			String valueOf = String.valueOf(lettersInGrid);
			editor.putString("lettersInGrid",String.valueOf(lettersInGrid));
			
			// pause or not
			editor.putBoolean("gamePause", gamePause);
			editor.putBoolean("isMusicPlaying", isPlaying);
			
			// word bar (fill?)
			
			// words found 
			editor.putInt("wordsFoundNum", WordWithBlanks.getWordsFound().size());
			HashSet<String> wordsFound = WordWithBlanks.getWordsFound();
			
			int i=0;
			for(String str : wordsFound){
				editor.putString("wordsFound" +i, str);
				i++;
			}
			
			// words not found
			editor.putInt("wordsNotFoundNum", WordWithBlanks.getWordsNotFound().size());
			HashSet<String> wordsNotFound = WordWithBlanks.getWordsNotFound();
			
			i=0;
			for(String str : wordsNotFound){
				editor.putString("wordsNotFound" +i, str);
				i++;
			}
			
			editor.commit();
		}
	};
	
	private void playBackgroundMusic(boolean play) {
		// make the volumn available to the users
		if (play) {
			TwoPlayer_GameActivity.this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
			backgroungMusic.setAudioStreamType(AudioManager.STREAM_MUSIC);
			backgroungMusic.setVolume(2, 2);
			backgroungMusic.start();
			backgroungMusic.setLooping(true);
		} else {
			backgroungMusic.pause();
		}
	}
	
	private void timeGoingDown(){
		new Thread() {
			public void run() {
				while(true){
//					timeLeft.setText(timeLeftNow);
					
					if(!isAllowedRunning){
						return;
					}
					
					try {
						sleep(1000);
					} catch (InterruptedException e) {
						throw new RuntimeException(e);
					}
					Message msg = Message.obtain();
					msg.what = TIME_GOING_DOWN;
					handler.sendMessage(msg);
				}
			};
		}.start();
	}
	
	private void showToastOnUI(final int internetState) {
		// TODO Auto-generated method stub
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if(internetState == TwoPlayer_GameActivity.INTERNET_FAILS){
					Log.i(TAG,"INTERNET_FAILS");
					Log.i(TAG,"isInternetOK: " + isInternetOK);
				} else {
					Log.i(TAG,"INTERNET_OK");
				}
				if(TwoPlayer_SnoopNewMessageService.isAppOnFront){
					if (internetState == TwoPlayer_GameActivity.INTERNET_OK && isInternetOK == false) {
						isInternetOK = true;
						Toast.makeText(TwoPlayer_GameActivity.this, "connection is OK",  Toast.LENGTH_SHORT).show();
					} else if (internetState == TwoPlayer_GameActivity.INTERNET_FAILS && isInternetOK == true) {
						isInternetOK = false;
						Toast.makeText(TwoPlayer_GameActivity.this, "connection fails",  Toast.LENGTH_SHORT).show();
					}
				}
			}
		});
	}
	
	private void changeScoreAndAction(final String score, final String action) {
		// TODO Auto-generated method stub
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				String p2ScoreStr = p2Score.getText().toString();
				String p2StateStr = p2State.getText().toString();
				
				if(!p2ScoreStr.equals(score) && !"".equals(score)){
					int oldScore = Integer.parseInt(p2ScoreStr);
					int newScore = Integer.parseInt(score);
					p2Score.setText(score);
					// if the score is increasing 
					// the ice will appear
					if (timeLeftNow <115 && newScore>oldScore && newScore>0 
							&& iceImage.getVisibility() == ImageView.INVISIBLE) {
						Log.i("TwoPlayer_GameActivity", "new score: "+newScore+" , oldscore: "+oldScore);
						startIceBlock();
					} 
				}
				
				if(!p2StateStr.equals(action)){
					p2State.setText(action);
				}
			}
		});
	};
	
	private boolean iceState1to2 = false;
	private boolean iceState2to3 = false;
	private boolean iceState3to4 = false;
	private MediaPlayer iceBreak;
	private static final int SET_ICE1 = 300;
	private static final int SET_ICE2 = 400;
	private static final int SET_ICE3 = 500;
	private static final int SET_ICE4 = 600;
	
	// TODO Auto-generated method stub
	private void startIceBlock() {
		changeIce(SET_ICE1);
		
		iceState1to2 = false;
		iceState2to3 = false;
		iceState3to4 = false;
		new Thread( new Runnable() {

			@SuppressLint("InlinedApi")
			@Override
			public void run() {
				
				while(true){
					if(!isAllowedRunning){
						return;
					}
					
					Log.i("TwoPlayer_GameActivity", "waiting for ice2");
					
					try {
						Thread.sleep(300);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					if(iceState1to2){
						changeIce(SET_ICE2);
						iceState1to2 = true; 
						iceState2to3 = false; 
						iceState3to4 = false;
						playMusicEffect(ICE_BREAK);
						break;
					}
					
					// if sense the shake
					// set to twoplayer_ice2
					// play the ice sound
					
				}
				
				while(true){
					// if sense the shake
					// set to twoplayer_ice3
					// play the ice sound
					// set the head text : fill the word
					if(!isAllowedRunning){
						return;
					}
					
					try {
						Thread.sleep(300);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					if(iceState2to3){
						changeIce(SET_ICE3);
						iceState1to2 = true; 
						iceState2to3 = true; 
						iceState3to4 = false;
						playMusicEffect(ICE_BREAK);
						break;
					}
				}
				
				while(true){
					// if sense the shake
					// set to twoplayer_ice3
					// play the ice sound
					// set the head text : fill the word
					if(!isAllowedRunning){
						return;
					}

					try {
						Thread.sleep(300);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					if(iceState3to4){
						changeIce(SET_ICE4);
						iceState1to2 = true; 
						iceState2to3 = true; 
						iceState3to4 = true;
						playMusicEffect(ICE_BREAK);
						
						break;
					}
				}	
			}
		
		}).start();
	}
	
	private void changeIce(final int setIceState) {
		// TODO Auto-generated method stub
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				switch (setIceState) {
				case SET_ICE1:
					headerText.setTextColor(Color.parseColor("#ff8888ff"));
					headerText.setText("Shake the Phone");
					iceImage.setVisibility(ImageView.VISIBLE);
					iceImage.setImageDrawable(getResources().getDrawable(
							R.drawable.twoplayer_ice1));
					iceImage.bringToFront();
					break;

				case SET_ICE2:
					iceImage.setImageDrawable(getResources().getDrawable(
							R.drawable.twoplayer_ice2));
					break;
				
				case SET_ICE3:
					iceImage.setImageDrawable(getResources().getDrawable(
							R.drawable.twoplayer_ice3));
					break;

				case SET_ICE4:
					headerText.setTextColor(Color.parseColor("#ff000000"));
					headerText.setText("Fill the Word");
					iceImage.setVisibility(ImageView.INVISIBLE);
					break;
				}
			}
		});	
	}

	private class MyListener implements SensorEventListener {

		@Override
		public void onSensorChanged(SensorEvent event) {
			// TODO Auto-generated method stub
			Sensor sensor = event.sensor;
			float x = event.values[0];
			float y = event.values[1];
			float z = event.values[2];
//			Log.i("TwoPlayer_GameActivity","x: "+x+" y: "+y+" z: "+z);
			if( x>=10 || y>=10 || z>=10){
				if(iceState1to2 == false && iceState2to3 == false && iceState3to4 == false){
					iceState1to2 = true;
				} else if( iceState1to2 == true && iceState2to3 == false && iceState3to4 == false){
					iceState2to3 = true;
				} else if( iceState1to2 == true && iceState2to3 == true && iceState3to4 == false){
					iceState3to4 = true;
				}
			}
		}

		@Override
		public void onAccuracyChanged(Sensor arg0, int arg1) {
			// TODO Auto-generated method stub
			
		}
	}
	
	private void changeTheTimeLeft(){
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if(timeLeftNow <=10){
					timeLeft.setTextColor(Color.parseColor("#ffff4444"));
				}
				if(timeLeftNow >=0){
					timeLeft.setText(timeLeftNow+"");
				}
				if(timeLeftNow ==0){
					if(!headerText.getText().equals("Right Answer")){
						WordWithBlanks.getWordsNotFound().add( WordWithBlanks.getOriginalWord() );
					}
					
					// no need to continue
					editor = sharedPreferences.edit();
					editor.putBoolean("hasGameEnded", true);
					editor.commit();
					Intent intent = new Intent(TwoPlayer_GameActivity.this, TwoPlayer_ResultActivity.class);
					startActivity(intent);
					finish();
				}
				
				// when the 15s no right answer
				// time not reach the end
				// the activity is still alive
				if(timeLeftNow == timetoNotify && timeLeftNow >= 10 && isAllowedRunning){
					sendNotification();
					timetoNotify = timeLeftNow -15;
				}
			}


		});
	}
	
	private void waitThenChangeTitle(final int msgWhat) {
		// TODO Auto-generated method stub
		Log.i("NewGame_GameActivity", "waitThenChangeTitle: I'm in the function block");
		new Thread() {
			public void run() {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} finally{

					Log.i("NewGame_GameActivity", "waitThenChangeTitle: I sent message   "+msgWhat);
					Message msg = Message.obtain();
					msg.what = msgWhat;
					handler.sendMessage(msg);
				}
			}
		}.start();
	}
	
	private void changeHeaderText(final int WORD_JUDGE){
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if(WORD_JUDGE == WORD_IS_RIGHT){
					headerText.setTextColor(Color.parseColor("#ff669900"));
					headerText.setText("Right Answer");
					headerText.invalidate();
					// play the right word music
					playMusicEffect(RIGHT_WORD);
					
					// calculate the score
					changeTheScore(WordWithBlanks.getWordBlankNum());
					
					wordBar.setBackgroundColor(Color.parseColor("#2299cc00"));
					nextClickListener.onClick(nextButton);
				}
				else if(WORD_JUDGE == WORD_IS_WRONG){
					headerText.setTextColor(Color.parseColor("#ffcc0000"));
					headerText.setText("Wrong Answer");
				}
				
				else if(WORD_JUDGE == WORD_TO_FILL){
					Log.i("NewGame_GameActivity", "changeHeaderText: I get message to fill word");
					wordBar.removeAllViews();
					wordBar.postInvalidate();
					NewGame_FillTheWordBar.getSingleFillTheWordBar(TwoPlayer_GameActivity.this,handler).fillLinearLayout(wordBar);
					headerText.setTextColor(Color.parseColor("#ff000000"));
					headerText.setText("Fill the Word");
					wordBar.setBackgroundColor(Color.parseColor("#ffffffff"));
					gameWholeLayout.postInvalidate();
				}
				
				else if(WORD_JUDGE == WORD_TITLE_BACK){
					Log.i("NewGame_GameActivity", "changeHeaderText: I get message to set title back");
					headerText.setTextColor(Color.parseColor("#ff000000"));
					headerText.setText("Fill the Word");
					wordBar.setBackgroundColor(Color.parseColor("#ffffffff"));
					gameWholeLayout.postInvalidate();
				}
			}


		});
	}
	
	private void playMusicEffect(int SITUATION){
		
		switch (SITUATION) {
		case RIGHT_WORD:
			TwoPlayer_GameActivity.this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
			rightWord.setAudioStreamType(AudioManager.STREAM_MUSIC);
			rightWord.setVolume(2, 2);
			rightWord.start();
			break;
		case ICE_BREAK:
			TwoPlayer_GameActivity.this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
			iceBreak.setAudioStreamType(AudioManager.STREAM_MUSIC);
			iceBreak.setVolume(2, 2);
			iceBreak.start();
			break;
//		case :
//			break;
		}
	}
	
	private void changeTheScore(int scoreChange) {
		// TODO Auto-generated method stub
		CharSequence scoreStr = newgameScore.getText();
		Integer score = Integer.parseInt((String) scoreStr);
		score = score + scoreChange;

		boolean positive = true;
		if(score < 0){
			positive = false;
		}
		
		score = Math.abs(score);
		
		String scoreStrNew = score.toString();
		
		if(positive){
			
			for(int i = scoreStrNew.length();i<3;i++){
				scoreStrNew = "0"+ scoreStrNew;
			}
			
		} else { //negative
			
			for(int i = scoreStrNew.length();i<2;i++){
				scoreStrNew = "0"+ scoreStrNew;
			}

			scoreStrNew = "-" + scoreStrNew;
		}
		
		WordWithBlanks.setTotalScore(scoreStrNew);
		newgameScore.setText(scoreStrNew);
	}
	
	private int getID(String name){
		int resId = this.getResources().getIdentifier(name, "drawable" , this.getPackageName());
		return resId;
	}
	
	private class MyReceiverNetFail extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
//			Toast.makeText(Communicate_CommunicateActivity.this, "Connection Fails", Toast.LENGTH_SHORT).show();
//			isInternetOK = false;
			Message msgSent = new Message();
			msgSent.what = TwoPlayer_GameActivity.INTERNET_FAILS;
			TwoPlayer_GameActivity.handler.sendMessage(msgSent);
		}
	}
	private class MyReceiverNetOK extends BroadcastReceiver{
		
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			
			Message msgSent = new Message();
			msgSent.what = TwoPlayer_GameActivity.INTERNET_OK;
			TwoPlayer_GameActivity.handler.sendMessage(msgSent);
		}
	}
	
	@SuppressWarnings("deprecation")
	private void sendNotification() {
		// TODO Auto-generated method stub
		String packageName = "edu.neu.madcourse.tengjiaochen";
		// 1.get the notificationManager
    	NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
    	
    	// 2.create Notification
    	int icon = R.drawable.ic_launcher;				// 图标
    	CharSequence tickerText = "Haven't scored for 15 sec. You can do better.";	// 提示文本
    	long when = System.currentTimeMillis();	// 时间
    	@SuppressWarnings("deprecation")
		Notification notification = new Notification(icon, tickerText, when);
    	
    	// 3. set Notification
    	Context context = getApplicationContext();				// 上下文环境
    	String contentTitle = "Haven't scored for 15 sec. You can do better.";					// 消息标题
    	String contentText = "New Message";	// 消息内容
    	Intent intent = new Intent();				 			// 用来开启Activity的意图
    	Log.i("TwoPlayer_SnoopNewMessageService", "packageName: "+packageName+".twoplayer."+TWOPLAYER_CLASS_NAME);
    	intent.setClassName(packageName, packageName+".twoplayer."+TWOPLAYER_CLASS_NAME);	// 意图指定Activity
    	intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
    	PendingIntent pedningIntent = PendingIntent.getActivity(TwoPlayer_GameActivity.this, 100, intent, PendingIntent.FLAG_ONE_SHOT);	// 定义待定意图 
    	notification.setLatestEventInfo(context, contentTitle, contentText, pedningIntent);	// 设置通知的具体信息
    	notification.flags = Notification.FLAG_AUTO_CANCEL;		// 设置自动清除
//    	notification.sound = Uri.parse("file:///mnt/sdcard/jiaodizhu.mp3");		// 设置通知的声音
    	notification.defaults |= Notification.DEFAULT_VIBRATE;
    	// 4.send Notification
    	manager.notify(0, notification);
    	
    	if(TwoPlayer_SnoopNewMessageService.isAppOnFront){
    		manager.cancelAll();
    	}
	}
}

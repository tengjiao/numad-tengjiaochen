package edu.neu.madcourse.tengjiaochen.twoplayer;

import java.util.Arrays;
import java.util.HashSet;

import edu.neu.madcourse.tengjiaochen.R;
import edu.neu.madcourse.tengjiaochen.communicate.bean.Communicate_MessageContainer;
import edu.neu.madcourse.tengjiaochen.communicate.util.Communicate_NetUtils;
import edu.neu.madcourse.tengjiaochen.dictionary.Dictionary_Acknowdgments;
import edu.neu.madcourse.tengjiaochen.dictionary.Dictionary_MainActivity;
import edu.neu.madcourse.tengjiaochen.newgame.service.WordWithBlanks;
import android.app.Activity;
import android.app.NotificationManager;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class TwoPlayer_ResultActivity extends Activity{
	
	boolean isOppoFini = false;
	private TextView p2totalScore;
	private int p1Score;
	private int p2Score;
	private TextView gameResult;
	protected MediaPlayer backgroungMusic ;
	protected boolean isPlaying = true;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.twoplayer_activity_result);
		isOppoFini = false;
		
		// here we do not need these notification any more
    	NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
    	manager.cancelAll();
		
		backgroungMusic = MediaPlayer.create(TwoPlayer_ResultActivity.this, R.raw.twoplayer_endmusic);
		playBackgroundMusic(isPlaying);
		
		// this is to check whether the game is fini
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				int attempt = 0;
				String lastTime = "";
				while(true){
					String opponent = Communicate_MessageContainer.getMessageContainer().getOpponent();
					Communicate_NetUtils.getValue(opponent + "gamestate");
					String scoreAndAction = Communicate_MessageContainer.getMessageContainer().getOpponentGameState();
					String[] split = scoreAndAction.split(",");
					String score = "000";
					Log.i("TwoPlayer_ResultActivity", "scoreAndAction : " + scoreAndAction);
					if(split.length == 3){
						score = split[0];
						String time = split[2];
						if("0".equals(time)){
							isOppoFini = true;
							changeP2Score(score,false);
							return;
						}
					} 
					
					try {
						if(lastTime.equals(scoreAndAction)){
							attempt++;
						} else{
							attempt =0;
						}
						
						if(attempt == 40){
							changeP2Score(score,true);
							return;
						}
						
						Thread.sleep(50);
						lastTime = scoreAndAction;
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}

		}).start();
		

		
		// this is for the score part
		TextView p1totalScoretv = (TextView) findViewById(R.id.twoplayer_p1_total_score);
		String p1totalScore = WordWithBlanks.getTotalScore();
		if(p1totalScore == null){
			p1totalScore = "000";
		}
		p1totalScoretv.setText("P1 Score: "+p1totalScore);

		String ScoreAndAction = Communicate_MessageContainer.getMessageContainer().getOpponentGameState();
		String[] split = ScoreAndAction.split(",");
		
		p2totalScore = (TextView) findViewById(R.id.twoplayer_p2_total_score);
		if(isOppoFini){
			p2totalScore.setText("P2 Score: "+ split[0]);
		} else {
			p2totalScore.setText("P2 is not done.");
		}
		
		gameResult = (TextView) findViewById(R.id.twoplayer_game_result);
		p1Score = Integer.parseInt(p1totalScore);
		p2Score = Integer.parseInt(split[0]);
		if(isOppoFini){
			if(p1Score > p2Score){
				gameResult.setText("You win");
			} else if(p1Score == p2Score){
				gameResult.setText("Same Score");
			} else if(p1Score < p2Score){
				gameResult.setText("You lose");
			}
		} else {
			gameResult.setText("waiting for P2.");
		}
		
		// this is the quit button part
		Button quitButton = (Button)findViewById(R.id.twoplayer_quitButton);
		quitButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
		LinearLayout wordsFound = (LinearLayout) findViewById(R.id.Newgame_WordsFound);
		HashSet<String> wordsFoundResult = WordWithBlanks.getWordsFound();
		
		for(String str:wordsFoundResult){
			TextView tv = new TextView(this);
			tv.setLayoutParams(new ViewGroup.LayoutParams(
					ViewGroup.LayoutParams.WRAP_CONTENT,
					ViewGroup.LayoutParams.WRAP_CONTENT));
			tv.setText(str);
			tv.setTextSize(15);
			wordsFound.addView(tv);
		}
		
		LinearLayout wordsNotFound = (LinearLayout) findViewById(R.id.Newgame_WordsNotFound);
		HashSet<String> wordsNotFoundResult = WordWithBlanks.getWordsNotFound();
		
		for(String str:wordsNotFoundResult){
			TextView tv = new TextView(this);
			tv.setLayoutParams(new ViewGroup.LayoutParams(
					ViewGroup.LayoutParams.WRAP_CONTENT,
					ViewGroup.LayoutParams.WRAP_CONTENT));
			tv.setText(str);
			tv.setTextSize(15);
			wordsNotFound.addView(tv);
		}
		
		// this is for the high score list
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				int attempt = 0;
//				String lastTime = "";
				int[] highScoretop5 = Communicate_MessageContainer.getMessageContainer().getHighScoretop5();
				while(true){
					
					if("P2 is not done.".equals(p2totalScore.getText()) ){
						
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						continue;
					}
					
					if( highScoretop5 == null ){
						LinearLayout topScoreList = (LinearLayout) findViewById(R.id.twoplayer_topscorelist);
						TextView tv = new TextView(TwoPlayer_ResultActivity.this);
						tv.setLayoutParams(new ViewGroup.LayoutParams(
								ViewGroup.LayoutParams.WRAP_CONTENT,
								ViewGroup.LayoutParams.WRAP_CONTENT));
						tv.setText("Top Score List Unavailable Now");
						tv.setTextSize(15);
						topScoreList.addView(tv);

						return;
					}
					
					changeTop5Score();
					break;

				}
			}



		}).start();
		
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		playBackgroundMusic(false);
		
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		playBackgroundMusic(true);
		
	}
	
	private void playBackgroundMusic(boolean play) {
		// make the volumn available to the users
		if (play) {
			TwoPlayer_ResultActivity.this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
			backgroungMusic.setAudioStreamType(AudioManager.STREAM_MUSIC);
			backgroungMusic.setVolume(2, 2);
			backgroungMusic.start();
			backgroungMusic.setLooping(true);
		} else {
			backgroungMusic.pause();
		}
	}
	
	private void changeTop5Score() {
		// TODO Auto-generated method stub
		runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				int[] highScoretop5 = Communicate_MessageContainer.getMessageContainer().getHighScoretop5();
				String p1totalScore = WordWithBlanks.getTotalScore();
				if(p1totalScore == null){
					p1totalScore = "000";
				}
				int p1Score = Integer.parseInt(p1totalScore);
				
				String ScoreAndAction = Communicate_MessageContainer.getMessageContainer().getOpponentGameState();
				String[] split = ScoreAndAction.split(",");
				String p2ScoreStr = split[0];
				int p2Score = Integer.parseInt(p2ScoreStr);
				
				int[] newtop5score = new int[7];
				
				for(int i =0;i<highScoretop5.length;i++){
					newtop5score[i] = highScoretop5[i];
				}
				newtop5score[5] = p1Score ;
				newtop5score[6] = p2Score ;
				
				Arrays.sort(newtop5score);
				
				LinearLayout topScoreList = (LinearLayout) findViewById(R.id.twoplayer_topscorelist);
				
				final String newtop5scoreStr =newtop5score[6]+","
										+newtop5score[5]+","
										+newtop5score[4]+","
										+newtop5score[3]+","
										+newtop5score[2];
				for(int i=0;i<5;i++){
					TextView tv = new TextView(TwoPlayer_ResultActivity.this);
					tv.setLayoutParams(new ViewGroup.LayoutParams(
							ViewGroup.LayoutParams.WRAP_CONTENT,
							ViewGroup.LayoutParams.WRAP_CONTENT));
					tv.setText("NO."+(i+1)+" : "+newtop5score[6-i]);
					tv.setTextSize(15);
					topScoreList.addView(tv);
				}
				
				new Thread(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						Communicate_NetUtils.setKeyValue("twoplayerhighscore",newtop5scoreStr);
					}
				}).start();
			}
		});	
	}
	
	private void changeP2Score(final String score,final boolean isDisconnect) {
		// TODO Auto-generated method stub
		runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				if(!isDisconnect){
					p2totalScore.setText("P2 Score: "+ score);
					p2Score = Integer.parseInt(score);
					if(p1Score > p2Score){
						gameResult.setText("You win");
					} else if(p1Score == p2Score){
						gameResult.setText("Same Score");
					} else if(p1Score < p2Score){
						gameResult.setText("You lose");
					}
				} else {
					p2totalScore.setText("P2 last Score: "+ score);
					gameResult.setText("Game Disconnect");
				}
			}
		});
	}
}

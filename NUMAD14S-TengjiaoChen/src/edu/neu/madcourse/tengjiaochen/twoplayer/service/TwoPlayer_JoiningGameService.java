package edu.neu.madcourse.tengjiaochen.twoplayer.service;

import java.util.regex.Pattern;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;

import edu.neu.madcourse.tengjiaochen.communicate.bean.Communicate_MessageContainer;
import edu.neu.madcourse.tengjiaochen.communicate.util.Communicate_NetUtils;
import edu.neu.madcourse.tengjiaochen.twoplayer.TwoPlayer_CommunicationInitialActivity;

public class TwoPlayer_JoiningGameService extends Service {

	private Thread joiningGameThread;
	private boolean isThreadAllowedRunning;
	
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		isThreadAllowedRunning = true;
		Communicate_MessageContainer.getMessageContainer().setInitialEnsure("");
		joiningGameThread = new Thread(new Runnable() {
			@Override
			public void run() {
				Log.i("JoiningGameService", "initialEnsure: " + Communicate_MessageContainer.getMessageContainer().getInitialEnsure()); 
				
				int attempt = 0;
				
				// check if the joinGameoponent is cleared
				while(true){
					
					if(!isThreadAllowedRunning){
						return;
					}
					
					Communicate_NetUtils.getValue("JoinGameOponent");
					String joinopponent = Communicate_MessageContainer.getMessageContainer().getJoinopponent();
					
					if( joinopponent.equals("") ){
						String imei = Communicate_MessageContainer.getMessageContainer().getIMEI();
						Communicate_NetUtils.setKeyValue("JoinGameOponent", imei);
						break;
					}
					
					try {
						attempt++;
						Thread.sleep(50);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					if(attempt % 12 == 0){
						Communicate_NetUtils.getValue("JoinGameOponent");
					}
				}
				
				attempt = 0;
				
				// check if the joinGameoponent is set with my imei
				while(true){
					
					if(!isThreadAllowedRunning){
						return;
					}
					
					Communicate_NetUtils.getValue("JoinGameOponent");
					String joinopponent = Communicate_MessageContainer.getMessageContainer().getJoinopponent();
					
					if( joinopponent.equals(Communicate_MessageContainer.getMessageContainer().getIMEI()) ){
						break;
					}
					
					try {
						attempt++;
						Thread.sleep(50);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					if(attempt % 12 == 0){
						Communicate_NetUtils.getValue("JoinGameOponent");
					}
				}
				
				attempt = 0;
				int initial ;
				Communicate_MessageContainer.getMessageContainer().setInitialEnsure("");
				// TODO Auto-generated method stub
				while(true){
					
					if(!isThreadAllowedRunning){
						return;
					}
					
					Communicate_NetUtils.getValue("InitialEnsure");
					String initialEnsure = Communicate_MessageContainer.getMessageContainer().getInitialEnsure();
					if(Pattern.compile("[0-9]+").matcher(initialEnsure).matches()){
						initial = Integer.parseInt(initialEnsure);
						Log.i("JoiningGameService", "in joining"); 
						Log.i("JoiningGameService", "initialEnsure: " + initialEnsure); 
						Communicate_NetUtils.setKeyValue("InitialEnsure",""+(initial+1));
						break;
					}
					
					try {
						Thread.sleep(300);
						attempt++;
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

					if (attempt % 4 == 0) {
						Communicate_NetUtils.getValue("InitialEnsure");
					}
					
				}
				
				attempt = 0;
				
				while(true){
					if(!isThreadAllowedRunning){
						return;
					}
					
					Communicate_NetUtils.getValue("InitialEnsure");
					String initialEnsure = Communicate_MessageContainer.getMessageContainer().getInitialEnsure();
					
					if(initialEnsure.equals(""+(initial+1))){
						Log.i("JoiningGameService", "initialEnsure now: " + initialEnsure);
						Message msgSent = new Message();
						msgSent.what = TwoPlayer_CommunicationInitialActivity.JOINING_SUCCESS;
						TwoPlayer_CommunicationInitialActivity.handler.sendMessage(msgSent); 
						break;
					}
					
					try {
						Thread.sleep(300);
						attempt++;
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

					if (attempt % 4 == 0) {
						Communicate_NetUtils.setKeyValue("InitialEnsure",""+(initial+1));
						Communicate_NetUtils.getValue("InitialEnsure");
					}
				}
			}
		});
		
		joiningGameThread.start();
	}
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		isThreadAllowedRunning = false;
//		NetUtils.setKeyValue("InitialEnsure", "");
		Communicate_MessageContainer.getMessageContainer().setInitialEnsure("");
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

}

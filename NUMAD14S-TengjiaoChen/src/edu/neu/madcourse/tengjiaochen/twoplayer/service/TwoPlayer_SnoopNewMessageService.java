package edu.neu.madcourse.tengjiaochen.twoplayer.service;

import java.util.List;
import java.util.regex.Pattern;

import edu.neu.madcourse.tengjiaochen.R;
import edu.neu.madcourse.tengjiaochen.communicate.bean.Communicate_MessageContainer;
import edu.neu.madcourse.tengjiaochen.communicate.bean.Communicate_ServiceState;
import edu.neu.madcourse.tengjiaochen.communicate.util.Communicate_NetUtils;
import edu.neu.madcourse.tengjiaochen.twoplayer.TwoPlayer_GameActivity;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;

public class TwoPlayer_SnoopNewMessageService extends Service {
	
	private String commuPackageName  ;
//	private int id = 0;
	private String TWOPLAYER_CLASS_NAME = "TwoPlayer_GameActivity";

	private static boolean isAllowedRunning = true;
	private List<ActivityManager.RunningTaskInfo> taskInfo;
	
	public static void setIsAllowedRunning(boolean isAllowedRunning){
		TwoPlayer_SnoopNewMessageService.isAllowedRunning = isAllowedRunning;
	}
	
	// this is to judge whether the app is on front
	public static boolean isAppOnFront = true;
	private WatchDogReceiver watchDogReceiver;

	public static void setAppOnFront(boolean isAppOnFront) {
		TwoPlayer_SnoopNewMessageService.isAppOnFront = isAppOnFront;
	}

	@Override
	// TODO Auto-generated method stub
	public void onCreate() {
		super.onCreate();

		// set a tag that this service is alive
		Communicate_ServiceState.getServiceState().setSnoopNewMessageServiceAlive(true);
		
		// set a tag that allows this service restart
		Communicate_ServiceState.getServiceState().setSnoopNewMessageServiceAllowedRestart(true);
		
		watchDogReceiver = new WatchDogReceiver();   
        IntentFilter filter = new IntentFilter("edu.neu.mascourse.tengjiaochen.WATCHDOG_KILLED");   
        registerReceiver(watchDogReceiver, filter);
		
		ActivityManager am = (ActivityManager) getBaseContext().getSystemService(Context.ACTIVITY_SERVICE);
		taskInfo = am.getRunningTasks(1);
		commuPackageName = getPackageName();
		
		new Thread( new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
//				Log.i("SnoopNewMessageService","opponent: " + opponent);
//				String oldScore = "";
//				String newScore = "";
				int newScoreInt = 0;
				while(true){
					
					if(!isAllowedRunning){
						Log.i("SnoopNewMessageService","This service is not allowed");
						return;
					}
					
					String opponent = Communicate_MessageContainer.getMessageContainer().getOpponent();
					// if the app is already on the front 
					// we have to clear all the notifications
					if(isAppOnFront){
				    	// 1.get the notificationManager
						Log.i("SnoopNewMessageService","This service is on the front");
				    	NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
				    	manager.cancelAll();
					}
					
					// get the current class of the activity
					ComponentName componentInfo = taskInfo.get(0).topActivity; 
					String className = componentInfo.getClassName();// here includes the package info
					String packageName = componentInfo.getPackageName();
					
					Log.i("SnoopNewMessageService", "packageName.className: " + className);
					
					Communicate_NetUtils.getValue(opponent + "gamestate");
					String messageReceived = Communicate_MessageContainer.getMessageContainer().getOpponentGameState();
					String ScoreAndAction = TwoPlayer_GameActivity.getScoreAndAction();

					Log.i("SnoopNewMessageService","ScoreAndAction: " + ScoreAndAction);
					Log.i("SnoopNewMessageService","messageReceived: " + messageReceived);
					Log.i("SnoopNewMessageService","isAppOnFront: " + isAppOnFront);
					
					if(!messageReceived.equals(ScoreAndAction) && !messageReceived.equals("ERROR: IOException")){
						Message msgSent = new Message();
						msgSent.what = TwoPlayer_GameActivity.RECEIVED_NEW_MESSAGE;
						msgSent.obj = messageReceived;
						TwoPlayer_GameActivity.handler.sendMessage(msgSent);
						
						String[] splitOld = ScoreAndAction.split(",");
						String oldScore = splitOld[0];
						if("".equals(oldScore)){
							oldScore = "000";
						}
						int oldScoreInt = Integer.parseInt(oldScore);
						
						String[] splitNew = messageReceived.split(",");
						String newScore = splitNew[0];
						if("".equals(newScore)){
							newScore = "000";
						}
						
						if(Pattern.compile("[-]*[0-9]+").matcher(newScore).matches()){
							newScoreInt = Integer.parseInt(newScore);
						}
						
						Log.i("TwoPlayer_SnoopNewMessageService","new: "+newScoreInt+" , old:"+oldScoreInt);
						
						if( !isAppOnFront && newScoreInt > oldScoreInt ){
					    	// 1.get the notificationManager
					    	NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
					    	
					    	// 2.create Notification
					    	int icon = R.drawable.ic_launcher;				// 图标
					    	CharSequence tickerText = "Take efforts in your game. Your opponent scored.";	// 提示文本
					    	long when = System.currentTimeMillis();	// 时间
					    	@SuppressWarnings("deprecation")
							Notification notification = new Notification(icon, tickerText, when);
					    	
					    	// 3. set Notification
					    	Context context = getApplicationContext();				// 上下文环境
					    	String contentTitle = "Take efforts in your game. Your opponent scored.";					// 消息标题
					    	String contentText = "New Message";	// 消息内容
					    	Intent intent = new Intent();				 			// 用来开启Activity的意图
					    	Log.i("TwoPlayer_SnoopNewMessageService", "packageName: "+packageName+".twoplayer."+TWOPLAYER_CLASS_NAME);
					    	intent.setClassName(packageName, packageName+".twoplayer."+TWOPLAYER_CLASS_NAME);	// 意图指定Activity
					    	intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
					    	PendingIntent pedningIntent = PendingIntent.getActivity(TwoPlayer_SnoopNewMessageService.this, 100, intent, PendingIntent.FLAG_ONE_SHOT);	// 定义待定意图 
					    	notification.setLatestEventInfo(context, contentTitle, contentText, pedningIntent);	// 设置通知的具体信息
					    	notification.flags = Notification.FLAG_AUTO_CANCEL;		// 设置自动清除
//					    	notification.sound = Uri.parse("file:///mnt/sdcard/jiaodizhu.mp3");		// 设置通知的声音
					    	notification.defaults |= Notification.DEFAULT_VIBRATE;
					    	
					    	// 4.send Notification
					    	manager.notify(0, notification);
						} 
					} else if(messageReceived.equals("ERROR: IOException")){
						Intent intent = new Intent("edu.neu.mascourse.tengjiaochen.COMMUNICATION_FAILS");
						sendBroadcast(intent);
					}
					
//					oldScore = newScore;
					
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
			}
		}).start();
	}
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		isAllowedRunning = false;
		boolean isSnoopNewMessageServiceAllowedRestart = Communicate_ServiceState.getServiceState().getIsSnoopNewMessageServiceAllowedRestart();
		if(isSnoopNewMessageServiceAllowedRestart){
			Intent intent = new Intent("edu.neu.mascourse.tengjiaochen.SNOOP_NEW_MESSAGE_SERVICE_KILLED");
			sendBroadcast(intent);
		}
		unregisterReceiver(watchDogReceiver);

		// set a tag that this service is dead
		Communicate_ServiceState.getServiceState().setSnoopNewMessageServiceAlive(false);

	}
	
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	
	private class WatchDogReceiver extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			startService(new Intent(TwoPlayer_SnoopNewMessageService.this,TwoPlayer_WatchDogService.class));
		}
	}
	
}
